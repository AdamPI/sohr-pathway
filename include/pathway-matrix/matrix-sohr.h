#ifndef __MATRIX_SOHR_H_
#define __MATRIX_SOHR_H_

#include <vector>
#include "types.h"

namespace pathway {

// size_t_matrix type
typedef std::vector<std::vector<index_int_t>> S_matrix_t;
std::string toString(const S_matrix_t &mat);
S_matrix_t matrix_multiplication(S_matrix_t m_k, S_matrix_t k_n);
S_matrix_t matrix_power(S_matrix_t n_n, std::size_t k);

// matrix element, so each matrix element is actually a vector of vector, the
// inside vector is used to store a single path, means each matrix element is a
// bunch of path(s) with equal length
// Graph theoretic enumeration of chemical pathways
// IRPC paper, http://www.tandfonline.com/doi/abs/10.1080/0144235X.2016.1220774
// path_t represents of a sequence of reactions
typedef std::vector<index_int_t> path_t;
std::string toString(const path_t &p);
typedef std::vector<path_t> SR_matrix_element_t;
typedef std::vector<std::vector<SR_matrix_element_t>> SR_matrix_t;
std::string toString(const SR_matrix_t &pRm);

// notice that the matrix multiplication has property: Associativity, but
// not Commutativity
SR_matrix_t matrix_multiplication(SR_matrix_t m_k, SR_matrix_t k_n);
SR_matrix_t matrix_power(SR_matrix_t n_n, std::size_t k);

// calculate equilibrium concentration/ratios from transition matrix
bool cal_equilibrium_ratio_from_transition_matrix(
    std::vector<std::vector<double>> &transition_mat,
    double &first_positive_eigenvalue, std::vector<double> &equil_ratio);

// solve linear equation
bool gaussian_jordan(std::vector<std::vector<double>> &A,
                     std::vector<double> &b);

}  // namespace pathway

#endif  //__MATRIX_SOHR_H_

