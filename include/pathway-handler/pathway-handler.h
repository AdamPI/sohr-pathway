#ifndef __PATHWAYHANDLER_
#define __PATHWAYHANDLER_
#include <algorithm>  //for std:copy
#include <fstream>
#include <iostream>
#include <iterator>  //for std::vector<T>::iterator
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <utility>  //for std::pair
#include <vector>
#include "types.h"

// deal with pathway stuff
namespace pathway {
// read from file (ff) "pathway_sim.csv" to get a vector of topN pathways
void read_ff(std::string filename, std::vector<std::string>& pathway_vec,
             size_t topN);

void pathway_to_vector(const std::string&, std::vector<index_int_t>&,
                       std::vector<index_int_t>&);
// parse the pathway ends with spe
void ends_with(std::string spe, const std::vector<std::string>& str_vec_in,
               std::vector<std::string>& str_vec_out, size_t topN = 10);
void starts_with(std::string spe, const std::vector<std::string>& str_vec_in,
                 std::vector<std::string>& str_vec_out, size_t topN = 10);

// parse the topN pathways for every species from a multimap
// n_spe is the total number of species
std::vector<std::string> ends_with(
    const std::multimap<double, std::string, std::greater<double>>& p_map,
    std::size_t n_spe, std::size_t topN);
std::vector<std::string> ends_with(
    const std::multimap<double, std::string, std::less<double>>& p_map,
    std::size_t n_spe, std::size_t topN);

void merge(std::vector<std::string>& v1, std::vector<std::string> v2);

};  // namespace pathway

#endif
