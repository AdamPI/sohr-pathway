#ifndef _MY_CLOCK_H_
#define _MY_CLOCK_H_

#include <ctime>

class MyClock {
   private:
    std::time_t dBegin;
    std::time_t dEnd;
    double Time;
    bool fBegin;

   public:
    MyClock();
    void begin();
    void end();
    double GetHowLong();
};

#endif
