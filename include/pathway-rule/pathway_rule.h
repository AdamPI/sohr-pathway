#ifndef __PATHWAY_CONSTRAINT_H_
#define __PATHWAY_CONSTRAINT_H_
#include <boost/property_tree/ptree.hpp>  //for property_tree
#include <map>
#include <tuple>
#include <unordered_set>
#include <vector>
#include "types.h"

namespace pathway {

// Rule defines rules a pathway needs to satisfy to be considered as valid
class Rule {
   private:
    // species sink reaction, have to be one reaction from this vector (from
    // this list)
    std::map<index_int_t, std::unordered_set<index_int_t> >
        species_sink_reaction_map;
    // reaction out species, have to be one species from this vector (from this
    // list)
    std::map<index_int_t, std::unordered_set<index_int_t> >
        reaction_out_species_map;
    // a more general out species constraint, species in the set can not be out
    // species of any reactions
    std::unordered_set<index_int_t> not_allowed_out_species_set;

    // must reaction species set, set the survial probability to 0.0, react
    // probability to be 1.0
    std::unordered_set<index_int_t> must_react_species_set;

    // terminal species can not further react
    std::unordered_set<index_int_t> terminal_species_set;

   public:
    Rule() {}
    ~Rule() {}
    void init_from_json(const std::string &);
    void init_from_pt(const boost::property_tree::ptree &pt);

   public:
    // add r_idx as sink reaction of species s_idx
    void _add_species_sink_reaction(index_int_t r_idx, index_int_t s_idx);
    // add s_idx as out species of reaction r_idx
    void _add_reaction_out_species(index_int_t r_idx, index_int_t s_idx);
    // add s_idx to not allowed species set
    void _add_not_allowed_species(index_int_t);
    // add s_idx to must reaction species set
    void _add_must_react_species(index_int_t);

   public:
    // is r_idx a valid sink reaction of species s_idx
    bool is_species_sink_reaction(index_int_t r_idx, index_int_t s_idx);
    // is s_idx a valid out species out reaction r_idx
    bool is_reaction_out_species(index_int_t r_idx, index_int_t s_idx);
    bool is_species_allowed(index_int_t);
    bool is_species_must_react(index_int_t);
    bool is_terminal_species(index_int_t);
};

}  // namespace pathway

#endif
