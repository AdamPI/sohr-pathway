#ifndef __PATHWAP_TYPES_H_
#define __PATHWAP_TYPES_H_

#include <map>  //for std::map
#include <ostream>
#include <string>  //for std::string
#include <vector>  //for std::vector

namespace pathway {

// infinitesimal dt
#define INFINITESIMAL_DT 1.0E-14

// time type
typedef double my_time_t;
// index type or count number/integer
typedef int index_int_t;
// general value type
typedef double value_t;
typedef std::pair<my_time_t, index_int_t> when_where_t;
struct when_R_where_t {
    // when
    my_time_t t;
    // through which reaction
    index_int_t R;
    // where, which species
    index_int_t s;
    bool operator==(const when_R_where_t &rhs) const {
        return std::tie(t, R, s) == std::tie(rhs.t, rhs.R, rhs.s);
    }
    when_R_where_t(my_time_t _t, index_int_t _R, index_int_t _s) {
        t = _t;
        R = _R;
        s = _s;
    };
};

// reaction and species branching ratio, namely Gamma
struct r_s_Gamma_t {
    // reaction branching ratio
    value_t r;
    // species branching ratio
    value_t s;
    bool operator==(const r_s_Gamma_t &rhs) const {
        return std::tie(r, s) == std::tie(rhs.r, rhs.s);
    }
    r_s_Gamma_t(value_t _r, value_t _s) {
        r = _r;
        s = _s;
    };
};

// a constant, used as indicator
const index_int_t INDICATOR = 100000;

// stoichiometric coefficient type
typedef double stoichiometric_coef_t;

// typedef reaction name type
typedef std::string reaction_name_t;

// elements name
typedef std::string element_name_t;

// species name type
typedef std::string spe_name_t;
// species component type, like total number of atoms, number of a specific atom
typedef std::map<std::string, value_t> spe_component_t;

// species name and species stoichoimetric coefficient in reaction/transition.
typedef std::map<spe_name_t, stoichiometric_coef_t> spe_name_s_coef_t;
// sink or source reactions of a species and stoichoimetric coefficient
typedef std::pair<index_int_t, stoichiometric_coef_t> reaction_index_s_coef_t;
/*
 * All reactions a species involved in, either sink or source reactions
 * The first number, +1/-1 , to indicate whether it is a source or sink reaction
 * sk-->represents source or sink
 */
typedef std::pair<index_int_t, reaction_index_s_coef_t>
    reaction_sk_index_s_coef_t;

// species index, name and weight type
struct spe_index_name_weight_t {
    index_int_t index;
    spe_name_t name;
    stoichiometric_coef_t coef;
    bool operator==(const spe_index_name_weight_t &rhs) const {
        return std::tie(index, name, coef) ==
               std::tie(rhs.index, rhs.name, rhs.coef);
    }
    spe_index_name_weight_t(index_int_t _si, spe_name_t _sn,
                            stoichiometric_coef_t _coef) {
        index = _si;
        name = _sn;
        coef = _coef;
    };
    friend std::ostream &operator<<(std::ostream &os,
                                    const spe_index_name_weight_t &obj) {
        return os << "(" << obj.index << ", " << obj.name << ", " << obj.coef
                  << ")";
    }
};

// element name and element index type map
typedef std::map<element_name_t, index_int_t> ele_name_index_map_t;
// species name and species index type map
typedef std::map<spe_name_t, index_int_t> spe_name_index_map_t;

// element information struct
struct element {
    index_int_t index;
    element_name_t name;
};

// probability type
typedef double probability_t;

// species information base type
struct species {
    index_int_t index = 0;
    spe_name_t name = "";
    // species component
    spe_component_t component;
    // more reaction network related stuff, like survival probability etc
    probability_t survival_probability = 0.0;
    // P_min for every species, where P_min is defined so that when P<P_min in a
    // certain time range, no reaction will occur P_min in principle, tell us
    // about the time limitation that reaction has to occur in a certain time
    // range, otherwise, it is out of intertest
    probability_t prob_min = 0.0;
    // P_max= 1-P_min
    probability_t prob_max = 0.0;

    // fast transition group/ chattering group
    int chattering_group_id = -1;

    // reactions it is involved in, source and sink reactions and the
    // stoichiometric coefficient
    /*
     * out reaction and stoichoimetric coefficient of this species
     * like 2A+B->C+2D, stoichoimetric coefficient is 2 for A of this reaction
     * the reason to store this information is it is a fixed, unchangeable
     * relation, no need to search reaction network as soon as it is initialized
     * and need it in pathway generation scheme
     */
    std::vector<reaction_sk_index_s_coef_t> reaction_sk_index_s_coef_v;
    // net source term, cancel the same terms on both sides for auto-catalytic
    // reactions, left source term got to consider auto-catalytic reactions.
    /*
     * A+X->2X
     * this is not a out reaction for X, a source term instead of a sink term
     */
    std::vector<reaction_index_s_coef_t> reaction_s_index_s_coef_v;

    // net sink terms of this species, cancel the same terms on both sides for
    // auto-catalytic reactions, left sink term
    std::vector<reaction_index_s_coef_t> reaction_k_index_s_coef_v;
};

// forward reaction -- E.g A<=>B represents two forward reactions,
// A=>B and B=>A, with different "key"
struct forward_reaction {
    // id represents chemkin style index, which may contain duplicated
    // reactions; on the contrary, reaction_index merges duplicated reactions
    index_int_t id = 0;
    // index can represent forward and backward reaction.
    // E.g. A<=>B is one reaction
    index_int_t index = 0;
    // key represents unique index of forward reaction. E.g A=>B has key of "0",
    // while B=>A has key of "1". This key shall be used in reaction network
    // representation as unique identification of a reaction
    index_int_t key = 0;
    reaction_name_t name = "";

    // is reaction zero, used as a flag
    // zero->false, none zero->true
    bool is_rate_nonzero = false;
    value_t rate = 0.0;

    // net reactant
    std::vector<spe_index_name_weight_t> net_reactant;
    // net prodect
    std::vector<spe_index_name_weight_t> net_product;
    // out species and corresponding weight
    // E.g. 2A+B->H2O2+H100
    // two out species, weight wrt. H atom is: 2, 100
    // the reason to store this information is it is a fixed, unchangeable
    // relation, no need to search reaction network as soon as it is initialized
    // and we need it in pathway generation scheme. Consider auto-catalytic
    // reactions, e.g. A+X->2X, X is a out species for this reaction, and the
    // out weight is just 1 instead of 2
    std::map<std::string, std::vector<spe_index_name_weight_t> >
        out_spe_index_weight_v_map;
    // out species branching ratio, namely Gamma, after applying
    // pathway rules; useful in trajectory simulation
    std::vector<spe_index_name_weight_t> out_spe_Gamma_v;
    // out species branching ratio, namely Gamma, after applying pathway rules;
    // useful in pathway probability evaluation
    std::map<index_int_t, value_t> out_spe_Gamma_map;
};
// reaction information struct
struct reaction : forward_reaction {
    std::string reactant_string;
    std::string product_string;
    bool reversible = true;

    std::vector<spe_index_name_weight_t> reactant;
    std::vector<spe_index_name_weight_t> product;
};

}  // namespace pathway

#endif
