#ifndef GRAPH_BUNDLED_H_
#define GRAPH_BUNDLED_H_
// This is a boost graph template with bundled property
// http://stackoverflow.com/questions/671714/modifying-vertex-properties-in-a-boostgraph

#include <algorithm>  //for std::find_if
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/static_assert.hpp>
#include <boost/version.hpp>

namespace parallelEdgeGraph {
/* definition of basic boost::graph properties */
enum vertex_properties_t { vertex_properties };
enum edge_properties_t { edge_properties };
// enum graph_properties_t { graph_properties };

/* the graph base class template */
template <typename VERTEXPROPERTIES, typename EDGEPROPERTIES>
class GraphBase {
   public:
    // boost::adjacency_list<OutEdgeList, VertexList, Directed,
    //                       VertexProperties, EdgeProperties,
    //                       GraphProperties, EdgeList>

    // An adjacency_list doesn't have an edge index associated with it, only a
    // vertex index. To have an edge index, you need to manually add it to the
    // graph description, and then manually handle it.

    // BGL uses containers from the STL such as std::vector, std::list, and
    // std::set to represent the set of vertices and the adjacency structure
    // (out-edges and in-edges) of the graph.
    // * vecS selects std::vector.
    // * listS selects std::list.
    // * slistS selects std::slist.
    // * setS selects std::set.
    // * multisetS selects std::multiset.
    // * hash_setS selects boost::unordered_set.

    // In general, if you want your vertex and edge descriptors to be stable
    // (never invalidated) then use listS or setS for the VertexList and
    // OutEdgeList template parameters of adjacency_list. If you are not as
    // concerned about descriptor and iterator stability, and are more concerned
    // about memory consumption and graph traversal speed, use vecS for the
    // VertexList and/or OutEdgeList template parameters.
    typedef boost::adjacency_list<
        // "OutEdgeList" argument for boost::adjacency_list
        // The OutEdgeList parameter determines what kind of container will be
        // used to store the out-edges (and possibly in-edges) for each vertex
        // in the graph. The containers used for edge lists must either satisfy
        // the requirements for Sequence or for AssociativeContainer.
        //
        // One of the first things to consider when choosing the OutEdgeList is
        // whether you want adjacency_list to enforce the absence of parallel
        // edges in the graph (that is, enforce that the graph not become a
        // multi-graph). If you want this enforced then use the setS or
        // hash_setS selectors. If you want to represent a multi-graph, or know
        // that you will not be inserting parallel edges into the graph, then
        // choose one of the Sequence types: vecS, listS, or slistS. You will
        // also want to take into account the differences in time and space
        // complexity for the various graph operations. Below we use V for the
        // total number of vertices in the graph and E for the total number of
        // edges. Operations not discussed here are constant time.
        //
        // multisetS --  Specify boost::multisetS as the OutEdgeListS template
        // argument to adjacency_list, this will enable an extra function called
        // edge_range which returns a range of iterators for all the "parallel"
        // edges coming out of u and going into v
        boost::vecS,
        // "VertexList" argument for boost::adjacency_list
        boost::vecS,
        // "Directed" argument for boost::adjacency_list
        boost::bidirectionalS,  // directed graph

        // "VertexProperties" argument for boost::adjacency_list
        /*property<vertex_properties_t, VERTEXPROPERTIES>,*/
        boost::property<
            boost::vertex_index_t, std::size_t,
            boost::property<vertex_properties_t, VERTEXPROPERTIES> >,
        // "EdgeProperties" argument for boost::adjacency_list
        // property<edge_properties_t, EDGEPROPERTIES>
        boost::property<boost::edge_index_t, std::size_t,
                        boost::property<edge_properties_t, EDGEPROPERTIES> >
        // multiple properties
        // edge_index_t is not installed for default, if you want to use get()
        // method to get edge_index, you have to install it explicitly
        >
        GraphContainer;

    /* a bunch of graph-specific typedefs */
    // descriptor is index (integer) like 0,1,2... if selector is type of vecS,
    // don't try to access descriptor itself, e.g. printf(*er.first) because
    // that will increment the descriptor, i.e. er.first += 1
    typedef
        typename boost::graph_traits<GraphContainer>::vertex_descriptor Vertex;
    typedef std::pair<Vertex, Vertex> VertexPair;
    typedef typename boost::graph_traits<GraphContainer>::edge_descriptor Edge;
    typedef std::pair<Edge, Edge> EdgePair;

    typedef typename boost::graph_traits<GraphContainer>::vertex_iterator
        vertex_iter;
    typedef
        typename boost::graph_traits<GraphContainer>::edge_iterator edge_iter;
    typedef typename boost::graph_traits<GraphContainer>::adjacency_iterator
        adjacency_iter;
    typedef typename boost::graph_traits<GraphContainer>::out_edge_iterator
        out_edge_iter;

    typedef
        typename boost::graph_traits<GraphContainer>::degree_size_type degree_t;

    typedef std::pair<adjacency_iter, adjacency_iter> adjacency_vertex_range_t;
    typedef std::pair<out_edge_iter, out_edge_iter> out_edge_range_t;
    typedef std::pair<vertex_iter, vertex_iter> vertex_range_t;
    typedef std::pair<edge_iter, edge_iter> edge_range_t;

    // typedef typename boost::property_map<GraphContainer, edge_index_t>::type
    // edge_index_map_t;

    // typedef for reversed graph
    typedef boost::reverse_graph<GraphContainer> reverse_graph_t;
    typedef typename boost::graph_traits<reverse_graph_t>::vertex_descriptor
        reverse_vertex_t;
    typedef typename boost::graph_traits<reverse_graph_t>::edge_descriptor
        reverse_edge_t;

   public:
    // visitor for shortest path algotithm, both dijkstra and bellman_ford
    // here we just implement the "edge_relaxed" function so that it could
    // record the edge on the shortest path class path_visitor : public
    // default_bfs_visitor class path_visitor : public
    // dijkstra_visitor<boost::null_visitor>
    class path_visitor : public boost::base_visitor<boost::null_visitor> {
       protected:
        // predecessors vector
        std::vector<Vertex> &predecessors;
        // coresponding edges vector of predecessors, edge from current
        // vertex<--predessor coressponds a edge
        std::vector<Edge> &edge_v;

       public:
        template <typename Graph>
        path_visitor(Graph g, std::vector<Vertex> &p, std::vector<Edge> &e_v)
            : predecessors(p), edge_v(e_v) {
            predecessors.resize(num_vertices(g));
            edge_v.resize(num_vertices(g));
        }

       public:
        template <typename Vertex, typename Graph>
        void initialize_vertex(const Vertex &s, const Graph &g) const {}
        template <typename Vertex, typename Graph>
        void discover_vertex(const Vertex &s, const Graph &g) const {}
        template <typename Vertex, typename Graph>
        void examine_vertex(const Vertex &s, const Graph &g) const {}
        template <typename Edge, typename Graph>
        void examine_edge(const Edge &e, const Graph &g) const {}
        // no const statement for this function, gonna change the class itself
        template <typename Edge, typename Graph>
        void edge_relaxed(const Edge &e, const Graph &g) {
            auto s = source(e, g);
            auto t = target(e, g);
            // my own way to create predecessor map, or says predecessor vector
            predecessors[t] = s;
            edge_v[t] = e;
            // std::cout << s << "-->" << t << std::endl;
        }
        template <typename Edge, typename Graph>
        void edge_not_relaxed(const Edge &e, const Graph &g) const {}
        template <typename Vertex, typename Graph>
        void finish_vertex(const Vertex &s, const Graph &g) const {}
        // bellman_ford needs this two extra functions
        template <typename Edge, typename Graph>
        void edge_minimized(const Edge &e, const Graph &g) const {}
        template <typename Edge, typename Graph>
        void edge_not_minimized(const Edge &e, const Graph &g) const {}
    };

   public:
    // visitor for shortest path algotithm, both dijkstra and bellman_ford
    // here we just implement the "edge_relaxed" function so that it could
    // record the edge on the shortest path class path_visitor : public
    // default_bfs_visitor class path_visitor : public
    // dijkstra_visitor<boost::null_visitor> for reverse graph
    class path_visitor_r : public boost::base_visitor<boost::null_visitor> {
       protected:
        // predecessors vector
        std::vector<reverse_vertex_t> &predecessors;
        // coresponding reverse_edge_ts vector of predecessors, reverse_edge_t
        // from current reverse_vertex_t<--predessor coressponds a
        // reverse_edge_t
        std::vector<reverse_edge_t> &reverse_edge_v;

       public:
        template <typename reverse_graph_t>
        path_visitor_r(reverse_graph_t g, std::vector<reverse_vertex_t> &p,
                       std::vector<reverse_edge_t> &e_v)
            : predecessors(p), reverse_edge_v(e_v) {
            predecessors.resize(num_vertices(g));
            reverse_edge_v.resize(num_vertices(g));
        }

       public:
        template <typename reverse_vertex_t, typename reverse_graph_t>
        void initialize_vertex(const reverse_vertex_t &s,
                               const reverse_graph_t &g) const {}
        template <typename reverse_vertex_t, typename reverse_graph_t>
        void discover_vertex(const reverse_vertex_t &s,
                             const reverse_graph_t &g) const {}
        template <typename reverse_vertex_t, typename reverse_graph_t>
        void examine_vertex(const reverse_vertex_t &s,
                            const reverse_graph_t &g) const {}
        template <typename reverse_edge_t, typename reverse_graph_t>
        void examine_edge(const reverse_edge_t &e,
                          const reverse_graph_t &g) const {}
        // no const statement for this function, gonna change the class itself
        template <typename reverse_edge_t, typename reverse_graph_t>
        void edge_relaxed(const reverse_edge_t &e, const reverse_graph_t &g) {
            auto s = source(e, g);
            auto t = target(e, g);
            // my own way to create predecessor map, or says predecessor vector
            predecessors[t] = s;
            reverse_edge_v[t] = e;
        }
        template <typename reverse_edge_t, typename reverse_graph_t>
        void edge_not_relaxed(const reverse_edge_t &e,
                              const reverse_graph_t &g) const {}
        template <typename reverse_vertex_t, typename reverse_graph_t>
        void finish_vertex(const reverse_vertex_t &s,
                           const reverse_graph_t &g) const {}
        // bellman_ford needs this two extra functions
        template <typename reverse_edge_t, typename reverse_graph_t>
        void edge_minimized(const reverse_edge_t &e,
                            const reverse_graph_t &g) const {}
        template <typename reverse_edge_t, typename reverse_graph_t>
        void edge_not_minimized(const reverse_edge_t &e,
                                const reverse_graph_t &g) const {}
    };

    /* constructors etc. */
    GraphBase() {}

    GraphBase(const GraphBase &g) : graph(g.graph) {}

    virtual ~GraphBase() {}

    /* structure modification methods */
    void Clear() { graph.clear(); }

    Vertex AddVertex(const VERTEXPROPERTIES &prop) {
        Vertex v = add_vertex(graph);
        properties(v) = prop;
        return v;
    }
    // not stable with VertexList of vecS
    // The reason this is a problem is that we are invoking remove_vertex(),
    // which when used with an adjacency_list where VertexList=vecS, invalidates
    // all iterators and descriptors for the graph (such as vi and vi_end),
    // thereby causing trouble in subsequent iterations of the loop.
    void RemoveVertex(const Vertex &v) {
        clear_vertex(v, graph);
        remove_vertex(v, graph);
    }

    Edge AddEdge(const Vertex &v1, const Vertex &v2,
                 const EDGEPROPERTIES &prop_12) {
        /* TODO: maybe one wants to check if this edge could be inserted */
        Edge addedEdge = add_edge(v1, v2, graph).first;

        properties(addedEdge) = prop_12;

        return addedEdge;
    }

    EdgePair AddEdge(const Vertex &v1, const Vertex &v2,
                     const EDGEPROPERTIES &prop_12,
                     const EDGEPROPERTIES &prop_21) {
        /* TODO: maybe one wants to check if this edge could be inserted */
        Edge addedEdge1 = add_edge(v1, v2, graph).first;
        Edge addedEdge2 = add_edge(v2, v1, graph).first;

        properties(addedEdge1) = prop_12;
        properties(addedEdge2) = prop_21;

        return EdgePair(addedEdge1, addedEdge2);
    }

    /* property access */
    VERTEXPROPERTIES &properties(const Vertex &v) {
        typename boost::property_map<GraphContainer, vertex_properties_t>::type
            param = get(vertex_properties, graph);
        return param[v];
    }

    const VERTEXPROPERTIES &properties(const Vertex &v) const {
        typename boost::property_map<GraphContainer,
                                     vertex_properties_t>::const_type param =
            get(vertex_properties, graph);
        return param[v];
    }

    EDGEPROPERTIES &properties(const Edge &v) {
        typename boost::property_map<GraphContainer, edge_properties_t>::type
            param = get(edge_properties, graph);
        return param[v];
    }

    const EDGEPROPERTIES &properties(const Edge &v) const {
        typename boost::property_map<GraphContainer,
                                     edge_properties_t>::const_type param =
            get(edge_properties, graph);
        return param[v];
    }

    const EDGEPROPERTIES &properties(const reverse_edge_t &v) const {
        // reversed graph
        reverse_graph_t reversed_graph(this->graph);
        typename boost::property_map<reverse_graph_t,
                                     edge_properties_t>::const_type param =
            get(edge_properties, reversed_graph);
        return param[v];
    }

    /* selectors and properties */
    const GraphContainer &getGraph() const { return graph; }

    vertex_range_t getVertices() const { return vertices(graph); }

    std::size_t get_num_vertices() const {
        std::size_t count = 0;
        for (vertex_range_t vp = vertices(graph); vp.first != vp.second;
             ++vp.first)
            count++;
        return count;
    }

    edge_range_t getEdges() const { return edges(graph); }

    std::size_t getEdgeCount() const { return num_edges(graph); }

    std::size_t get_num_edges() const {
        std::size_t count = 0;
        for (edge_range_t ep = edges(graph); ep.first != ep.second; ++ep.first)
            count++;
        return count;
    }

    adjacency_vertex_range_t getAdjacentVertices(const Vertex &v) const {
        return adjacent_vertices(v, graph);
    }

    std::size_t getInDegree(const Vertex &v) const {
        return in_degree(v, graph);
    }

    std::size_t getOutDegree(const Vertex &v) const {
        return out_degree(v, graph);
    }

    out_edge_range_t getOutEdges(const Vertex &v) const {
        return out_edges(v, graph);
    }

    std::size_t getVertexCount() const { return num_vertices(graph); }

    std::size_t getVertexDegree(const Vertex &v) const {
        return in_degree(v, graph) + out_degree(v, graph);
    }

    /* operators */
    GraphBase &operator=(const GraphBase &rhs) {
        graph = rhs.graph;
        return *this;
    }

   protected:
    GraphContainer graph;
};

};  // namespace parallelEdgeGraph

namespace boost {
using namespace parallelEdgeGraph;
BOOST_INSTALL_PROPERTY(vertex, properties);
BOOST_INSTALL_PROPERTY(edge, properties);
// BOOST_INSTALL_PROPERTY(graph, properties);
}  // namespace boost

#endif
