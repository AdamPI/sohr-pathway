#ifndef __PATHWAP_NETWORK_IO_H_
#define __PATHWAP_NETWORK_IO_H_

#include "graph-base.h"
#include "types.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network.h"

namespace pathway {
// read species composition from json file
// notice we convert species name string and element string to lower case
void read_species_composition(const std::string &, std::vector<species> &,
                              spe_name_index_map_t &);
// update element registry based on species composition
void update_element(const std::vector<species> &, std::vector<element> &);
// read reaction composition from json file
// notice we convert species name string and element string to lower case
void read_reaction_composition(const std::string &, std::vector<reaction> &);
// update reaction -- updates species index in reactions
void update_reaction(const spe_name_index_map_t &, std::vector<reaction> &);
// reaction to forward_reaction, E.g A<=>B becomes two reactions,
// A=>B and B=>A
void to_forward_reaction(const std::vector<reaction> &,
                         std::vector<forward_reaction> &);

};  // namespace pathway

#endif
