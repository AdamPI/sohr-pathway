#ifndef __PATHWAP_NETWORK_H_
#define __PATHWAP_NETWORK_H_

#include "graph-base.h"
#include "matrix-sohr.h"
#include "network-io.h"
#include "path_statistics.h"
#include "pathway_rule.h"
#include "random.h"
#include "types.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "propagator.h"

namespace pathway {

struct VertexProperties_graph {
    // all the species are labeled with integer, here make them start from 0
    index_int_t species_index = 0;
    // vertex index can be the same as species index, but not always
    index_int_t vertex_index = 0;
};

// EdgeProperties_graph represents edge properties in network. An edge maps to
// exactly one reactions. However one reactions may be involved in multiple
// edges
struct EdgeProperties_graph {
    index_int_t src = 0;         // source vertex index
    index_int_t dest = 0;        // destination vertex index
    index_int_t edge_index = 0;  // its edge index in the graph
    // which reaction it is, notice this is "key" not "id" not "index"
    index_int_t reaction_key = 0;
    // just for search algorithm
    double edge_weight = 0.0;

    // stoichiometric coefficient, like A+A->B+B,
    // src_coef = dest_coef= 2
    stoichiometric_coef_t src_coef = 1;
    stoichiometric_coef_t dest_coef = 1;
};

// Network Summary
struct NetworkSummary {
    // number of species
    int n_species = -1;
    // number of reaction
    int n_reaction = -1;
    // number of vertex
    int n_vertex = -1;
    // number of edge
    int n_edge = -1;
};

// A Network class is a specialization of GraphBase template
class Network : public parallelEdgeGraph::GraphBase<VertexProperties_graph,
                                                    EdgeProperties_graph> {
   private:
    // species name to index
    spe_name_index_map_t spe_name_index_map;
    std::vector<element> element_v;
    std::vector<species> species_v;
    std::vector<forward_reaction> reaction_v;
    // system minimum time
    my_time_t sys_min_time = 1.0e-11;
    my_time_t absolute_end_t = 0.0;

   public:
    S_matrix_t S_matrix;
    SR_matrix_t SR_matrix;

   public:
    std::shared_ptr<Propagator> prop = NULL;

   public:
    std::shared_ptr<Rule> rule = NULL;

   public:
    std::shared_ptr<random_sohr::random> rand = NULL;

   private:
    std::vector<Vertex> vertex_index_to_descriptor;
    std::vector<Edge> edge_index_to_descriptor;

   public:
    void set_absolute_end_t(const my_time_t t);

   public:
    // update species and reaction information out of vectors/map which can be
    // shared across multiple cores
    void update_species_reaction(const std::vector<species> &,
                                 const spe_name_index_map_t &,
                                 std::vector<reaction>);
    void init_network_info(std::vector<VertexProperties_graph> &,
                           std::vector<EdgeProperties_graph> &) const;
    // initiate the graph according to the input Edge vector
    void init_graph(const std::vector<VertexProperties_graph> &,
                    const std::vector<EdgeProperties_graph> &);
    void set_spe_out_reaction();
    void set_reaction_out_spe(element_name_t);
    // set Prob_max(tau^{j}|t+tau^{j-1};S^{j-1}), with pathway_end_time fixed
    value_t set_spe_prob_max(const my_time_t init_t, const my_time_t end_t,
                             index_int_t s_idx);
    // set the prob_min of a species at a given time
    value_t set_spe_prob_min(const my_time_t init_t, const my_time_t end_t,
                             index_int_t s_idx);

   public:
    my_time_t next_reaction_time_from_importance_sampling(
        const my_time_t curr_t, const index_int_t curr_spe, const value_t Y);
    // update out reaction rate of species
    void update_out_reaction_rate(index_int_t curr_spe, my_time_t t);
    // set out species branching ratio, namely Gamma factor, for specified
    // element/followed atom
    void set_reaction_out_spe_Gamma(element_name_t);
    // given a species index, randomly pick a next reaction
    index_int_t spe_next_reaction(const index_int_t curr_spe);
    // randomly pick next species based on their weight
    index_int_t reaction_next_spe(const index_int_t r_idx);
    // pathway procceed one step forward
    when_R_where_t pathway_forward(const my_time_t t,
                                   const index_int_t curr_spe);
    // one-time pathway simulation in time range
    std::string pathway_sim(const my_time_t init_t, const my_time_t end_t,
                            const index_int_t init_s);
    // generate pathway by running Monte Carlo simulation
    void gen_pathway(const my_time_t init_t, const my_time_t end_t,
                     const index_int_t init_s, const int Ntraj, statistics &st);

   public:
    // for pathway probability evaluation
    // Gamma - branching_ratio
    value_t spe_sink_by_a_reaction_Gamma(index_int_t curr_spe,
                                         index_int_t next_reaction);
    value_t reaction_out_spe_Gamma(index_int_t curr_r, index_int_t out_s);
    // reaction and species branching ratio
    r_s_Gamma_t cal_r_s_Gamma(const my_time_t curr_t,
                              const index_int_t curr_spe,
                              const index_int_t next_reaction,
                              const index_int_t next_spe);
    // reaction and species branching ratio by simulating species hopping
    // through reaction network forward one step, notice the time variable
    // is randomly sampled and dynamically updated
    r_s_Gamma_t pathway_prob_forward(const index_int_t curr_spe,
                                     const index_int_t next_reaction,
                                     const index_int_t next_spe,
                                     my_time_t &curr_t);
    // probability species can react in a time range
    value_t prob_spe_react_in_a_time_range(const my_time_t init_t,
                                           const my_time_t end_t,
                                           const index_int_t curr_s);
    // pathway probability by simulating one chain of reaction events
    value_t pathway_prob_sim(const my_time_t init_t, const my_time_t end_t,
                             const std::vector<index_int_t> &s_vec,
                             const std::vector<index_int_t> r_vec);
    // [approx]imate the pathway probability using importance sampling technique
    value_t pathway_prob_approx(const my_time_t init_t, const my_time_t end_t,
                                const std::vector<index_int_t> &s_vec,
                                const std::vector<index_int_t> r_vec,
                                const int Ntraj);

    /* [approx]imate the pathway probability using importance sampling technique
    for a vector of time points, ts represents time points
    Arguments:
        t_vec: vector<my_time_t>
            time points vector
        s_vec: vector<index_int_t>
            species indices vector
        r_vec: vector<index_int_t>
            reaction(s) indices vector
        Ntraj: int
            number of trajectories
        pp_vec: vector<value_t>
            pathway probability vector
    Raises:
        Assertion(s)
    */
    void pathway_prob_approx_ts(const std::vector<my_time_t> &t_vec,
                                const std::vector<index_int_t> &s_vec,
                                const std::vector<index_int_t> r_vec,
                                const int Ntraj, std::vector<value_t> &pp_vec);

   public:
    void init_S_matrix(element_name_t);
    void init_SR_matrix(element_name_t);

   public:
    // Query writes a json of species info, reaction info to terminal/file
    void Query(const std::string &fn) const;
    NetworkSummary get_network_summary() const;

   public:
    // get vertex descriptor by vertex index
    Vertex getVertexByIndex(index_int_t) const;
    // get edge descriptor by edge index
    Edge getEdgeByIndex(index_int_t) const;

   public:
    // for internal test only
    const species &_get_species(index_int_t) const;
    const forward_reaction &_get_reaction(index_int_t) const;

   public:
    // Arguments:
    //    spe_fn : species composition filename
    //    rxn_fn: reaction composition filename
    //    pr_fn: pathway rules filename
    //    e: followed atom/element
    //    sequence_ID: sequence_ID for each core, used as a differentiator for
    //    random generator
    //    prop: Propagator initiated outside
    Network(const std::string &spe_fn, const std::string &rxn_fn,
            const std::string &pr_fn, element_name_t e, int sequence_ID,
            PropPtrType prop);
    Network();
    ~Network();
};

};  // namespace pathway

#endif
