#ifndef __FILEIO_H_
#define __FILEIO_H_

#include <boost/lexical_cast.hpp>
#include <fstream>
#include <vector>
#include "csv_reader.h"

namespace fileIO { /*namespace fileIO*/

// read top N line of csv matrix
template <typename T>
std::vector<std::vector<T>> read_topN_line_csv(std::string filename,
                                               std::size_t topN) {
    std::vector<std::vector<T>> mat;

    std::ifstream fin(filename.c_str());
    size_t icounter = 0;
    std::vector<T> vec;
    for (CSVIterator file_itr(fin);
         (icounter++ < topN) && (file_itr != CSVIterator()); ++file_itr) {
        vec.clear();
        for (size_t i = 0; i < (*file_itr).size(); ++i) {
            vec.push_back(boost::lexical_cast<T>((*file_itr)[i]));
        }
        mat.push_back(vec);
    }
    return mat;
}

} /*namespace fileIO*/

#endif
