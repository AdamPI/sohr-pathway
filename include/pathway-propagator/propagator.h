#ifndef __PROPAGATOR_H_
#define __PROPAGATOR_H_
#include <memory>

#include "types.h"
// cubic spline codes, e.g. interp_1d.h", has to be after all boost
// libraries
#include "interp_1d.h"
#include "interp_linear.h"

namespace pathway {

class Propagator {
   private:
    // number of species
    int nSpe = 0;
    // number of reactions
    int nRxn = 0;
    // number of points
    int nPts = 0;
    std::vector<my_time_t> time_v;
    // pseudo first order rate constant
    std::vector<std::vector<value_t>> pseudo1k_v;
    // [c]umulative pseudo first order rate constant
    std::vector<std::vector<value_t>> c_pseudo1k_v;

    // reaction rates
    std::vector<std::vector<value_t>> rr_v;

    std::vector<std::shared_ptr<Linear_interp>> pseudo1k_ptrs;
    std::vector<std::shared_ptr<Linear_interp>> c_pseudo1k_ptrs;
    // reverse pointer, ([c]umulative pseudo1k, time)
    std::vector<std::shared_ptr<Linear_interp>> reverse_c_pseudo1k_ptrs;
    std::vector<std::shared_ptr<Linear_interp>> rr_ptrs;

   public:
    Propagator();
    Propagator(const std::string &timeFn, const std::string &kFn,
               const std::string &rrFn, const int _nSpe, const int _nRxn,
               const int maxN);
    ~Propagator();

   public:
    // The reaction rate file should not double count the duplicate
    // reaction(s)
    void init_from_file(const std::string &timeFn, const std::string &kFn,
                        const std::string &rrFn, const int _nSpe,
                        const int _nRxn, const int maxN);
    /*
    Description:
        - Read time points from file
        - Initiate species pseudo-first-order rate const vector
        - Initiate reaction rate vector
    Arguments:
        - timeFn, time data point file
        - _nSpe, number of species
        - _nRxn, number of reactions
        - maxN, maximum number of time points to read
    */
    void init_time_from_file(const std::string &timeFn, const int _nSpe,
                             const int _nRxn, const int maxN);
    void init_pseudo1k_rr_ptr();
    void calculate_c_pseudo1k();
    void initiate_c_pseudo1k_ptr();
    value_t get_pseudo1k(const size_t speIdx, const my_time_t t) const;
    value_t get_c_pseudo1k(const size_t speIdx, const my_time_t t) const;
    my_time_t time_from_c_pseudo1k(const size_t speIdx, const value_t ck) const;
    value_t get_rr(const size_t rxnIdx, const my_time_t t) const;
    // get number of points
    int get_n_pts() const;
    // get time
    value_t get_time(const int i) const;
    // per time point
    void update_pseudo1k_per_t(const int t, const std::vector<value_t> &k);
    /*
    Description: at constant volume condition
    Arguments:
        - rr_fwd: Forward reaction rates presumably in duplicate-reaction space
        - rr_bwd: Backword reaction rates presumably in duplicate-reaction space
        - dup_2_non: reaction index mapping from duplicate-reaction space to
        non-duplicate-reaction space
        - dup_is_reversible: is reaction reversible in duplicate-reaction space
     */
    void update_rr_per_t(const int t, const std::vector<value_t> &rr_fwd,
                         const std::vector<value_t> &rr_bwd,
                         const std::map<int, int> &dup_2_non,
                         const std::map<int, bool> &dup_is_reversible);
    /*
    Description: save pseudo1k data to file
    Arguments:
        - kFile: location
     */
    void save_pseudo1k(const std::string &kFile) const;
    /*
    Description: save rr data to file
    Arguments:
        - rrFile: location
     */
    void save_rr(const std::string &rrFile) const;
};

// ---------- Utilities ----------
typedef std::shared_ptr<Propagator> PropPtrType;
PropPtrType New_prop_ptr();

/*
Description: Read reaction index mapping from a json file
Arguments:
    - dup_2_non: map from duplicate-reaction-space to non-duplicate-reaction
space
    - non_2_dup: map from non-duplicate-reaction-space to duplicate-reaction
space
    - dup_is_reversible: is reaction reversible in duplicate-reaction space
 */
void read_rxn_mapping(const std::string fn, std::map<int, int> &dup_2_non,
                      std::map<int, std::vector<int>> &non_2_dup,
                      std::map<int, bool> &dup_is_reversible);
}  // namespace pathway

#endif
