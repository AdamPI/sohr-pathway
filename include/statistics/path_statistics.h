#ifndef __STATISTICS_GET_CONCENTRATION_H_
#define __STATISTICS_GET_CONCENTRATION_H_
#include <algorithm>  //for std:copy
#include <fstream>
#include <iostream>
#include <iterator>  //for std::vector<T>::iterator
#include <map>
#include <string>
#include <utility>  //for std::pair
#include <vector>

// pathway statistics
namespace pathway {

typedef std::map<std::string, int> stat_map;
typedef std::pair<std::string, int> str_int_p;
struct Compare_pair {
    bool operator()(const str_int_p& p1, const str_int_p& p2) const {
        return p1.second > p2.second;
    }
};
typedef std::vector<str_int_p> str_int_v;

// simple statistics related the last species in the pathways
// can be used to calculated concentration of species, for example
// <species, count> <S5, 10001> for instance
class statistics {
   public:
    stat_map unordered_pathway;
    str_int_v ordered_pathway;

   public:
    const stat_map& get_unordered_pathway_map() const {
        return unordered_pathway;
    }

   public:
    statistics() {}
    // read a file, save result to another file
    statistics(std::string str_in, std::string str_out, int path_count = 1);
    ~statistics() {}

   public:
    // Read in file
    void read_csv_file(std::string str_in, int N = 10);
    // Insert pathway
    void insert_pathway(std::string in_pathway);
    // Insert unordered map
    void insert_unordered_map(stat_map& unordered_pathway_in) {
        unordered_pathway.clear();
        unordered_pathway = unordered_pathway_in;
    }
    // Sort by counting number and print to file, print only if pathway count >=
    // path_count
    void sort_print_to_file(std::string str_out = "./output/pathway_sim.csv",
                            int path_count = 1);
    const str_int_v& return_sorted_path_count();
};

};  // namespace pathway

#endif
