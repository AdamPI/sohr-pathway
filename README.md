# SOHR-pathway
## Overview
SOHR-pathway is a collection of utilities useful to enumerate pathways and evaluate pathway probabilities.
## Install
Prerequisites:

- [vcpkg](https://github.com/microsoft/vcpkg/blob/master/README.md)
- [cmake](https://cmake.org) with version > 3.12
- [boost](https://www.boost.org)
- [gflags](https://github.com/gflags/gflags)
- [gtest](https://github.com/google/googletest)

To get started, first install `vcpkg`, (assuming you have cmake>3.12 installed)

```sh
$ mkdir ~/Git
$ cd ~/Git
$ git clone https://github.com/Microsoft/vcpkg.git
$ cd vcpkg
$ ./bootstrap-vcpkg.sh
$ ./vcpkg integrate install
```
After that, install `boost`, `gflags` and `gtest`

```sh
$ cd ~/Git/vcpkg
$ ./vcpkg install boost
$ ./vcpkg install gflags
$ ./vcpkg install gtest
```
Finally, one can install SOHR-pathway with

```sh
$ cd ~/Git
$ git clone https://AdamPI@bitbucket.org/AdamPI/sohr-pathway.git
$ cd sohr-pathway
$ bash scripts/cmake.sh clean-build
$ sudo bash scripts/cmake.sh install "/opt/sohr/v1"
```
Where `opt/sohr/v1` represents the installation location prefix, which is `/usr/local` by default. Run the following to verify the binary is installed successfully.

```sh
$ /opt/sohr/v1/bin/path-prob -help
```

## Examples
Linear system: To a linear system with 3 species (X1, X2 and X3) and 2 reactions
```
X1 => X2
X2 => X3
```
### Species Composition File
One first needs to provide a `species composition` file,
```sh
$ cat /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json
{
    "X1": {
        "Q1": 1.0
    },
    "X2": {
        "Q1": 1.0
    },
    "X3": {
        "Q1": 1.0
    }
}
```
Where `Q1` represents the fictitious atom. `X1`, `X2` and `X3` each contains one `Q1` atom. 
### Reaction Composition File
Second one must provide a `reaction composition` file,
```sh
$ cat /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json
{
    "X1 => X2": {
        "id": "0001",
        "equation": "X1 => X2",
        "reactant_string": "X1",
        "reactants": {
            "X1": 1.0
        },
        "product_string": "X2",
        "products": {
            "X2": 1.0
        },
    },
    "X2 => X3": {
        "id": "0002",
        "equation": "X2 => X3",
        "reactant_string": "X2",
        "reactants": {
            "X2": 1.0
        },
        "product_string": "X3",
        "products": {
            "X3": 1.0
        },
    }
}
```
### Rules File
Third we need a `rules` file that defines rules how we walk through reaction network.
```
$ cat /opt/sohr/v1/share/sohr/examples/linear-system/rules.json
{
    "rules": {
        "terminal_species": [
            2
        ],
        "must_react_species": [],
        "not_allowed_out_species": [],
        "reaction_out_species_map": {},
        "species_sink_reaction_map": {}
    }
}
```
In which species `S2` (X3, the package's label system may differ from user's one) is a terminal species by definition, aka `S2`'s sink rate constant is zero.
### Verify the elements, species and reaction labelling scheme is sane
After that we need to verify the elements, species and reaction labelling scheme is correct,
```
$ /opt/sohr/v1/bin/path-prob \
-sComp /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json \
-rComp /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json \
-pRule /opt/sohr/v1/share/sohr/examples/linear-system/rules.json \
-atom "q1"\
-labelFile "label.json"
"rules.terminal_species" value: 2
Writing elements, species and reactions labelling scheme to file: label.json
$ cat label.json
{
    "element": {
        "q1": {
            "index": "0",
            "name": "q1"
        }
    },
    "species": {
        "0": {
            "index": "0",
            "name": "x1",
            "component": {
                "q1": "1"
            },
            "sink_reaction": {
                "0": "x1 => x2"
            }
        },
        "1": {
            "index": "1",
            "name": "x2",
            "component": {
                "q1": "1"
            },
            "sink_reaction": {
                "1": "x2 => x3"
            }
        },
        "2": {
            "index": "2",
            "name": "x3",
            "component": {
                "q1": "1"
            },
            "sink_reaction": ""
        }
    },
    "reaction": {
        "0": {
            "key": "0",
            "name": "x1 => x2",
            "out_species": {
                "1": "x2"
            }
        },
        "1": {
            "key": "1",
            "name": "x2 => x3",
            "out_species": {
                "2": "x3"
            }
        }
    }
}

```
Make sure the number of species and number of reactions are corrent. Also verify species composition and reaction formula are correct.
### S-matrix
As long as you have `species composition`, `reaction composition` and `rules` files in hand, you can show the `S-matrix` by,
```
$ /opt/sohr/v1/bin/path-prob \
-sComp /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json \
-rComp /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json \
-pRule /opt/sohr/v1/share/sohr/examples/linear-system/rules.json \
-atom "q1" -sMat
[] [S0#1S1] []
[] [] [S1#1S2]
[] [] []
```
The output shows `S-matrix`, e.g matrix element `S0#1S1` represents there exist `1` path originating from species `S0` to species `S1`. Noticeably, `-atom "q1"` constrain the followed atom to be lowercase `q1`. 
### SR-matrix
Similarly, one can view the `SR-matrix` by running
```
$ /opt/sohr/v1/bin/path-prob \
-sComp /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json \
-rComp /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json \
-pRule /opt/sohr/v1/share/sohr/examples/linear-system/rules.json \
-atom "q1" -srMat
[] [S0R0S1] []
[] [] [S1R1S2]
[] [] []
```
`SR-matrix` element `S0R0S1` further details that path from `S0` to `S1` goes through reaction `R0`.
### Enumerate Pathway
In order to enumerate pathway, the program runs Monte Carlo trajectory and ranks the pathways by frequency,
```
$ /opt/sohr/v1/bin/path-prob \
-sComp /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json \
-rComp /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json \
-pRule /opt/sohr/v1/share/sohr/examples/linear-system/rules.json \
-tFile /opt/sohr/v1/share/sohr/examples/linear-system/t2_time.csv \
-kFile /opt/sohr/v1/share/sohr/examples/linear-system/s3_t2_k.csv \
-rrFile /opt/sohr/v1/share/sohr/examples/linear-system/r2_t2_rr.csv \
-atom "q1" \
-mcPath \
-initS 0 \
-nTraj 10000 \
-startT 0 \
-endT 1
pathway: S0R0S1R1S2, count: 3897
pathway: S0, count: 3665
pathway: S0R0S1, count: 2438
```
User needs to provide flag `-mcPath`, initial species `-initS 0`, number of trajectory `nTraj 10000`, start time `-startT 0` and end time `endT 1` accordingly. What's more, one must provide a time file (`-tFile`) that enumerats time points (includes time zero) to which the program uses to interpolate time variable; one also must provide a pseudo rate constant file (`-kFile`) and a reaction rate (`-rrFile`). Please make sure the number of species and number of reactions are consistent with `label.json` file.
### Pathway Probability
To approximate pathway probability, one can run
```
$ /opt/sohr/v1/bin/path-prob \
-sComp /opt/sohr/v1/share/sohr/examples/linear-system/spe_composition.json \
-rComp /opt/sohr/v1/share/sohr/examples/linear-system/rxn_composition.json \
-pRule /opt/sohr/v1/share/sohr/examples/linear-system/rules.json \
-tFile /opt/sohr/v1/share/sohr/examples/linear-system/t2_time.csv \
-kFile /opt/sohr/v1/share/sohr/examples/linear-system/s3_t2_k.csv \
-rrFile /opt/sohr/v1/share/sohr/examples/linear-system/r2_t2_rr.csv \
-atom "q1" \
-nTraj 10000 \
-startT 0 \
-endT 1 \
-pathProb \
-pathway "S0R0S1"
pathway: S0R0S1
pathway probability: 2.317367e-01
```
Flags `-pathProb` and `-pathway "S0R0S1"` shall be provided. The output value shall be accurate obeying square-root-of-N rule.
