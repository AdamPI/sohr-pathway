#include <boost/graph/adjacency_list.hpp>
#include <cstdio>
#include <map>
#include <vector>
#include "boost/generator_iterator.hpp"
#include "boost/random.hpp"
#include "gtest/gtest.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network-io.h"
#include "network.h"

TEST(NetworkIOTest, SpeciesElement) {
    // test read species composition
    std::string filename = std::string(
        "../tests/examples/n-propane/species_composition.json");
    std::vector<pathway::species> s_v;
    pathway::spe_name_index_map_t spe_name_index_map;
    pathway::read_species_composition(filename, s_v, spe_name_index_map);
    EXPECT_EQ(112, s_v.size());
    bool found = false;
    pathway::species s1;
    s1.index = 108;
    s1.name = "vinoxylmethyl";
    s1.component["c"] = 3.0;
    s1.component["h"] = 5.0;
    s1.component["o"] = 1.0;
    for (auto s : s_v) {
        if (s.name != s1.name) continue;
        EXPECT_EQ(s1.index, s.index);
        EXPECT_EQ(s1.component, s.component);
        found = true;
    }
    EXPECT_EQ(true, found);

    found = false;
    pathway::species s2;
    s2.index = 6;
    s2.name = "hcfg";
    s2.component["h"] = 1.0;
    for (auto s : s_v) {
        if (s.name != s2.name) continue;
        EXPECT_EQ(s2.index, s.index);
        EXPECT_EQ(s2.component, s.component);
        found = true;
    }
    EXPECT_EQ(true, found);

    found = false;
    pathway::species s3;
    s3.index = 87;
    s3.name = "qooh_1";
    s3.component["c"] = 3.0;
    s3.component["h"] = 7.0;
    s3.component["o"] = 2.0;
    for (auto s : s_v) {
        if (s.name != s3.name) continue;
        EXPECT_EQ(s3.index, s.index);
        EXPECT_EQ(s3.component, s.component);
        found = true;
    }
    EXPECT_EQ(true, found);

    // Test update_element
    std::vector<std::string> expElement = {"c", "h", "o", "n", "ar", "he"};
    std::vector<pathway::element> e_v;

    pathway::update_element(s_v, e_v);
    for (auto e : e_v) {
        printf("Updated element: %s\n", e.name.c_str());
    }
    EXPECT_EQ(expElement.size(), e_v.size());

    for (auto e_name : expElement) {
        found = false;
        pathway::element e1;
        e1.name = e_name;
        printf("Checking element: %s\n", e1.name.c_str());
        for (auto e : e_v) {
            if (e.name != e1.name) continue;
            found = true;
        }
        EXPECT_EQ(true, found);
    }

    // call it again, should be idempotent
    pathway::update_element(s_v, e_v);
    for (auto e : e_v) {
        printf("Updated element: %s\n", e.name.c_str());
    }
    EXPECT_EQ(expElement.size(), e_v.size());

    for (auto e_name : expElement) {
        found = false;
        pathway::element e1;
        e1.name = e_name;
        printf("Checking element: %s\n", e1.name.c_str());
        for (auto e : e_v) {
            if (e.name != e1.name) continue;
            found = true;
        }
    }

    // Test update_spe_name_index_map_t
    EXPECT_EQ(spe_name_index_map["he"], 0);
    EXPECT_EQ(s_v[0].index, 0);
    EXPECT_EQ(spe_name_index_map["o"], 8);
    EXPECT_EQ(s_v[8].index, 8);
    EXPECT_EQ(spe_name_index_map["ipropanol"], 111);
    EXPECT_EQ(s_v[111].index, 111);
}

TEST(NetworkIOTest, Reaction) {
    // test read reaction composition
    std::string filename = std::string(
        "../tests/examples/n-propane/reaction_composition.json");
    std::vector<pathway::reaction> r_v;
    pathway::read_reaction_composition(filename, r_v);
    EXPECT_EQ(r_v.size(), 622);

    bool found = false;
    pathway::reaction r1;
    r1.id = 1;
    r1.index = 0;
    r1.name = "h + o2 <=> o + oh";
    r1.reactant_string = "h + o2";
    r1.product_string = "o + oh";
    r1.reactant.push_back(pathway::spe_index_name_weight_t(0, "h", 1.0));
    r1.reactant.push_back(pathway::spe_index_name_weight_t(0, "o2", 1.0));
    r1.product.push_back(pathway::spe_index_name_weight_t(0, "o", 1.0));
    r1.product.push_back(pathway::spe_index_name_weight_t(0, "oh", 1.0));
    for (auto r : r_v) {
        if (r.name != r1.name) continue;
        EXPECT_EQ(r.id, r1.id);
        EXPECT_EQ(r.index, r1.index);
        EXPECT_EQ(r.reactant, r1.reactant);
        EXPECT_EQ(r.reactant_string, r1.reactant_string);
        EXPECT_EQ(r.product, r1.product);
        EXPECT_EQ(r.product_string, r1.product_string);
        found = true;
    }
    EXPECT_EQ(true, found);

    found = false;
    pathway::reaction r2;
    r2.id = 638;
    r2.index = 621;
    r2.name = "2 npropyloo <=> o2 + npropanol + propanal";
    r2.reactant.push_back(
        pathway::spe_index_name_weight_t(0, "npropyloo", 2.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "o2", 1.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "npropanol", 1.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "propanal", 1.0));
    for (auto r : r_v) {
        if (r.name != r2.name) continue;
        EXPECT_EQ(r.id, r2.id);
        EXPECT_EQ(r.index, r2.index);
        EXPECT_EQ(r.reactant, r2.reactant);
        EXPECT_EQ(r.product, r2.product);
        found = true;
    }
    EXPECT_EQ(true, found);
}

TEST(NetworkIOTest, UpdateReaction) {
    // r1
    pathway::reaction r1;
    r1.name = "h + o2 <=> o + oh";
    r1.reactant.push_back(pathway::spe_index_name_weight_t(0, "h", 1.0));
    r1.reactant.push_back(pathway::spe_index_name_weight_t(0, "o2", 1.0));
    r1.product.push_back(pathway::spe_index_name_weight_t(0, "o", 1.0));
    r1.product.push_back(pathway::spe_index_name_weight_t(0, "oh", 1.0));
    // r2
    pathway::reaction r2;
    r2.name = "2 npropyloo <=> o2 + npropanol + propanal";
    r2.reactant.push_back(
        pathway::spe_index_name_weight_t(0, "npropyloo", 2.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "o2", 1.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "npropanol", 1.0));
    r2.product.push_back(pathway::spe_index_name_weight_t(0, "propanal", 1.0));
    // r3
    pathway::reaction r3;
    r3.name = "2 h + 4 o <=> 2 h + o2 + 2 o";
    r3.reactant.push_back(pathway::spe_index_name_weight_t(0, "h", 2.0));
    r3.reactant.push_back(pathway::spe_index_name_weight_t(0, "o", 4.0));
    r3.product.push_back(pathway::spe_index_name_weight_t(0, "h", 2.0));
    r3.product.push_back(pathway::spe_index_name_weight_t(0, "o2", 1.0));
    r3.product.push_back(pathway::spe_index_name_weight_t(0, "o", 2.0));
    // r_v
    std::vector<pathway::reaction> r_v = {r1, r2, r3};
    // spe_name_index_map
    pathway::spe_name_index_map_t spe_name_index_map;
    spe_name_index_map["h"] = 0;
    spe_name_index_map["o2"] = 1;
    spe_name_index_map["o"] = 2;
    spe_name_index_map["oh"] = 3;
    spe_name_index_map["npropyloo"] = 4;
    spe_name_index_map["npropanol"] = 5;
    spe_name_index_map["propanal"] = 6;
    pathway::update_reaction(spe_name_index_map, r_v);
    // r1, h
    EXPECT_EQ(r_v[0].reactant[0].index, 0);
    // o2
    EXPECT_EQ(r_v[0].reactant[1].index, 1);
    // o
    EXPECT_EQ(r_v[0].product[0].index, 2);
    // oh
    EXPECT_EQ(r_v[0].product[1].index, 3);
    // r2, npropyloo
    EXPECT_EQ(r_v[1].reactant[0].index, 4);
    // o2
    EXPECT_EQ(r_v[1].product[0].index, 1);
    // npropanol
    EXPECT_EQ(r_v[1].product[1].index, 5);
    // propanal
    EXPECT_EQ(r_v[1].product[2].index, 6);
    // r3, h
    EXPECT_EQ(r_v[2].reactant[0].index, 0);
    EXPECT_EQ(r_v[2].reactant[1].index, 2);
    EXPECT_EQ(r_v[2].product[0].index, 0);
    EXPECT_EQ(r_v[2].product[1].index, 1);
    EXPECT_EQ(r_v[2].product[2].index, 2);
    EXPECT_EQ(r_v[2].net_reactant[0].index, 2);
    EXPECT_EQ(r_v[2].net_reactant[0].coef, 2.0);
    EXPECT_EQ(r_v[2].net_product[0].index, 1);
    EXPECT_EQ(r_v[2].net_product[0].coef, 1.0);
}

TEST(NetworkIOTest, ToForwardReaction) {
    // r1
    pathway::reaction r1;
    r1.name = "h + o2 <=> o + oh";
    r1.reactant_string = "h + o2";
    r1.product_string = "o + oh";
    r1.net_reactant.push_back(pathway::spe_index_name_weight_t(0, "h", 1.0));
    r1.net_reactant.push_back(pathway::spe_index_name_weight_t(1, "o2", 1.0));
    r1.net_product.push_back(pathway::spe_index_name_weight_t(2, "o", 1.0));
    r1.net_product.push_back(pathway::spe_index_name_weight_t(3, "oh", 1.0));
    std::vector<pathway::reaction> r_v = {r1};

    std::vector<pathway::forward_reaction> f_r_v;
    pathway::to_forward_reaction(r_v, f_r_v);
    EXPECT_EQ(2, f_r_v.size());
    // forward reaction
    EXPECT_EQ(0, f_r_v[0].index);
    EXPECT_EQ(0, f_r_v[0].key);
    EXPECT_EQ("h + o2 => o + oh", f_r_v[0].name);
    EXPECT_EQ(2, f_r_v[0].net_product.size());
    EXPECT_EQ(pathway::spe_index_name_weight_t(0, "h", 1.0),
              f_r_v[0].net_reactant[0]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(1, "o2", 1.0),
              f_r_v[0].net_reactant[1]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(2, "o", 1.0),
              f_r_v[0].net_product[0]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(3, "oh", 1.0),
              f_r_v[0].net_product[1]);
    // backward reaction
    EXPECT_EQ(0, f_r_v[1].index);
    EXPECT_EQ(1, f_r_v[1].key);
    EXPECT_EQ("o + oh => h + o2", f_r_v[1].name);
    EXPECT_EQ(2, f_r_v[1].net_product.size());
    EXPECT_EQ(pathway::spe_index_name_weight_t(2, "o", 1.0),
              f_r_v[1].net_reactant[0]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(3, "oh", 1.0),
              f_r_v[1].net_reactant[1]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(0, "h", 1.0),
              f_r_v[1].net_product[0]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(1, "o2", 1.0),
              f_r_v[1].net_product[1]);
}
