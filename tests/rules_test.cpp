#include <cstdio>
#include "gtest/gtest.h"
#include "pathway_rule.h"
#include "string"

// Rule test base struct
struct RuleBase : testing::Test {
    pathway::Rule rule;
    RuleBase() {}
    virtual ~RuleBase() {}
    //
};

// test fixture
struct ruleSetting {
    std::string fn;
    ruleSetting() {
        fn = std::string("../tests/examples/n-propane/rules.json");
    }
    ruleSetting(std::string _f) : fn(_f) {}
    friend std::ostream &operator<<(std::ostream &os, const ruleSetting &obj) {
        os << "\nRule ruleSetting file name: \"" << obj.fn << "\"\n";
        return os;
    }
};

struct RuleTest : RuleBase, testing::WithParamInterface<ruleSetting> {};

TEST_P(RuleTest, APIs) {
    //
    auto ruleSettingObj = GetParam();
    printf("[----------] Rule ruleSetting file name: %s\n",
           ruleSettingObj.fn.c_str());
    ASSERT_TRUE(ruleSettingObj.fn != "");
    // init
    rule.init_from_json(ruleSettingObj.fn);
    // test
    EXPECT_EQ(false, rule.is_terminal_species(123134));
    EXPECT_EQ(true, rule.is_terminal_species(66));
    EXPECT_EQ(true, rule.is_terminal_species(1010));

    EXPECT_EQ(true, rule.is_species_allowed(123134));
    EXPECT_EQ(false, rule.is_species_allowed(5));

    EXPECT_EQ(false, rule.is_species_must_react(123134));
    EXPECT_EQ(true, rule.is_species_must_react(7));

    EXPECT_EQ(true, rule.is_species_sink_reaction(5, 348));
    // species doesn't exist in special list, default to be true
    EXPECT_EQ(true, rule.is_species_sink_reaction(5, 3489));
    EXPECT_EQ(false, rule.is_species_sink_reaction(55, 348));

    EXPECT_EQ(true, rule.is_reaction_out_species(101, 32));
    // reaction doesn't exist in special list, default to be true
    EXPECT_EQ(true, rule.is_reaction_out_species(101998, 2));
    EXPECT_EQ(false, rule.is_reaction_out_species(101, 2222));
}

INSTANTIATE_TEST_SUITE_P(Ingetration, RuleTest, testing::Values(ruleSetting()));
