#include <boost/format.hpp>
#include <stdexcept>

#include "gtest/gtest.h"
#include "memory"
#include "network-io.h"
#include "pathway-handler.h"
#include "string"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network.h"

// integration can be abbreviated as "i9", there is nine characters from the
// begining to the end of "integration"
struct NetworkI9Base : testing::Test {
    std::vector<pathway::VertexProperties_graph> vertexVec;
    std::vector<pathway::EdgeProperties_graph> edgePro;
    std::shared_ptr<pathway::Network> nPtr;
    pathway::element_name_t followed_atom = "o";
    NetworkI9Base() {}
    virtual ~NetworkI9Base() {}
    //
};

// test fixture
struct io_filename {
    // species file name
    std::string spe_fn;
    // reaction file name
    std::string rxn_fn;
    // pathway rules file name
    std::string pr_fn;
    // query output file
    std::string query_fn;
    // time filename
    std::string time_fn;
    // pseudo1k filenmae
    std::string k_fn;
    // reaction rate filename
    std::string rr_fn;
    io_filename() {
        spe_fn =
            std::string("../tests/examples/n-propane/species_composition.json");
        rxn_fn = std::string(
            "../tests/examples/n-propane/reaction_composition.json");
        pr_fn = std::string("../tests/examples/n-propane/rules.json");
        query_fn = std::string("../tests/examples/n-propane/query_test1.json");
        time_fn = std::string("../tests/examples/n-propane/t5_time.csv");
        k_fn = std::string("../tests/examples/n-propane/s112_t5_k.csv");
        rr_fn = std::string("../tests/examples/n-propane/r1244_t5_rr.csv");
    }
    io_filename(const string &option) {
        if (option == "linear-system") {
            spe_fn = std::string(
                "../tests/examples/linear-system/spe_composition.json");
            rxn_fn = std::string(
                "../tests/examples/linear-system/rxn_composition.json");
            pr_fn = std::string("../tests/examples/linear-system/rules.json");
            query_fn =
                std::string("../tests/examples/linear-system/query_test2.json");
            time_fn =
                std::string("../tests/examples/linear-system/t2_time.csv");
            k_fn = std::string("../tests/examples/linear-system/s3_t2_k.csv");
            rr_fn = std::string("../tests/examples/linear-system/r2_t2_rr.csv");
        }
    }
    friend std::ostream &operator<<(std::ostream &os, const io_filename &obj) {
        os << "\nspecies composition file name: \"" << obj.spe_fn << "\"\n";
        os << "reaction composition file name: \"" << obj.rxn_fn << "\"\n";
        os << "pathway rule file name: \"" << obj.pr_fn << "\"\n";
        os << "query output file name: \"" << obj.query_fn << "\"\n";
        os << "time points file name: \"" << obj.time_fn << "\"\n";
        os << "pseudo1k file name: \"" << obj.k_fn << "\"\n";
        os << "reaction rate file name: \"" << obj.rr_fn << "\"\n";
        return os;
    }
};

struct NetworkI9Test : NetworkI9Base,
                       testing::WithParamInterface<io_filename> {};

TEST_P(NetworkI9Test, Init) {
    //
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    EXPECT_EQ(112, nPtr->get_num_vertices());
    EXPECT_EQ(112, nPtr->getVertexCount());
    EXPECT_EQ(4190, nPtr->get_num_edges());
    EXPECT_EQ(4190, nPtr->getEdgeCount());

    auto ns = nPtr->get_network_summary();
    EXPECT_EQ(112, ns.n_species);
    EXPECT_EQ(1244, ns.n_reaction);
    EXPECT_EQ(112, ns.n_vertex);
    EXPECT_EQ(4190, ns.n_edge);
}

TEST_P(NetworkI9Test, SpeciesReaction) {
    //
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    // species and reaction properties
    auto s1 = nPtr->_get_species(15);
    EXPECT_EQ("co2", s1.name);
    EXPECT_EQ(14, s1.reaction_k_index_s_coef_v.size());
    EXPECT_EQ(std::make_pair(47, 1.0), s1.reaction_k_index_s_coef_v[0]);
    auto s1_our_r1 = nPtr->_get_reaction(s1.reaction_k_index_s_coef_v[0].first);
    EXPECT_EQ("co2 (+m) => co + o (+m)", s1_our_r1.name);
    EXPECT_EQ(std::make_pair(647, 1.0), s1.reaction_k_index_s_coef_v[13]);
    auto s1_k_r13 = nPtr->_get_reaction(s1.reaction_k_index_s_coef_v[13].first);
    EXPECT_EQ("c2h3 + co2 => ch2chco + o", s1_k_r13.name);

    auto r1 = nPtr->_get_reaction(66);
    EXPECT_EQ("hco + ho2 => co2 + h + oh", r1.name);
    EXPECT_EQ(1, int(r1.out_spe_index_weight_v_map.count(this->followed_atom)));
    EXPECT_EQ(2, r1.out_spe_index_weight_v_map[this->followed_atom].size());
    EXPECT_EQ(pathway::spe_index_name_weight_t(15, "co2", 2.0),
              r1.out_spe_index_weight_v_map[this->followed_atom][0]);
    EXPECT_EQ(pathway::spe_index_name_weight_t(10, "oh", 1.0),
              r1.out_spe_index_weight_v_map[this->followed_atom][1]);
    // reaction out species Gamma value
    for (int i = 0; i < r1.out_spe_Gamma_v.size(); ++i) {
        printf("%u %d %s\n", i, r1.out_spe_Gamma_v[i].index,
               r1.out_spe_Gamma_v[i].name.c_str());
    }
    EXPECT_NEAR(2.0 / 3, r1.out_spe_Gamma_v[0].coef, 1e-6);
    EXPECT_NEAR(1.0 / 3, r1.out_spe_Gamma_v[1].coef, 1e-6);
}

TEST_P(NetworkI9Test, VertexEdge) {
    //
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    // vertex properties
    EXPECT_EQ(0, nPtr->properties(0).species_index);
    EXPECT_EQ(0, nPtr->properties(0).vertex_index);
    if (nPtr->get_num_vertices() >= 100) {
        EXPECT_EQ(100, nPtr->properties(100).species_index);
        EXPECT_EQ(100, nPtr->properties(100).vertex_index);
    }
    // edge properties
    EXPECT_EQ(0, nPtr->properties(nPtr->getEdgeByIndex(0)).edge_index);
    EXPECT_EQ(0, nPtr->properties(nPtr->getEdgeByIndex(0)).reaction_key);
    EXPECT_EQ(3, nPtr->properties(nPtr->getEdgeByIndex(0)).src);
    EXPECT_EQ(8, nPtr->properties(nPtr->getEdgeByIndex(0)).dest);
    EXPECT_EQ(1, nPtr->properties(nPtr->getEdgeByIndex(0)).src_coef);
    EXPECT_EQ(1, nPtr->properties(nPtr->getEdgeByIndex(0)).dest_coef);
    if (nPtr->get_num_vertices() >= 100) {
        EXPECT_EQ(4189,
                  nPtr->properties(nPtr->getEdgeByIndex(4189)).edge_index);
        EXPECT_EQ(1243,
                  nPtr->properties(nPtr->getEdgeByIndex(4189)).reaction_key);
        EXPECT_EQ(74, nPtr->properties(nPtr->getEdgeByIndex(4189)).src);
        EXPECT_EQ(78, nPtr->properties(nPtr->getEdgeByIndex(4189)).dest);
        EXPECT_EQ(1, nPtr->properties(nPtr->getEdgeByIndex(4189)).src_coef);
        EXPECT_EQ(2, nPtr->properties(nPtr->getEdgeByIndex(4189)).dest_coef);
    }

    bool catched = false;
    try {
        nPtr->getVertexByIndex(100000);
    } catch (const std::invalid_argument &ex) {
        std::cerr << "Invalid argument: " << ex.what() << '\n';
        catched = true;
    }
    EXPECT_EQ(true, catched);
    catched = false;
    try {
        nPtr->getEdgeByIndex(100000);
    } catch (const std::invalid_argument &ex) {
        std::cerr << "Invalid argument: " << ex.what() << '\n';
        catched = true;
    }
    EXPECT_EQ(true, catched);
}

TEST_P(NetworkI9Test, Matrix) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    // S matrix
    EXPECT_EQ(112, nPtr->S_matrix.size());
    EXPECT_EQ(112, nPtr->S_matrix[0].size());
    EXPECT_EQ(0, nPtr->S_matrix[66][66]);
    EXPECT_EQ(35, nPtr->S_matrix[10][11]);

    // SR matrix
    EXPECT_EQ(112, nPtr->SR_matrix.size());
    EXPECT_EQ(112, nPtr->SR_matrix[0].size());
    // matrix element is vector of paths, e.g. {{1,2,3}, {1,100,3}}, represents
    // S1R2S3 and S1R100S3
    EXPECT_EQ(0, nPtr->SR_matrix[66][66].size());
    EXPECT_EQ(35, nPtr->SR_matrix[10][11].size());
    EXPECT_EQ("R4", pathway::toString(nPtr->SR_matrix[10][11][0]));
    EXPECT_EQ("R1018", pathway::toString(nPtr->SR_matrix[10][11][34]));
}

TEST_P(NetworkI9Test, Query) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    nPtr->Query(io_fn.query_fn);
}

TEST_P(NetworkI9Test, Propagator) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));

    // pseudo1k
    EXPECT_EQ(nPtr->prop->get_pseudo1k(1, 1.0), 1.0);
    // [c]umulative pseudo1k
    EXPECT_EQ(nPtr->prop->get_c_pseudo1k(0, 2.0), 2.0);
    // reverse time from [c]umulative pseudo1k
    EXPECT_EQ(nPtr->prop->time_from_c_pseudo1k(0, 1e10), 1e10);
    // rr
    EXPECT_EQ(nPtr->prop->get_rr(2, 1.0), 1.0);
}

TEST_P(NetworkI9Test, PathwayForward) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    nPtr->set_absolute_end_t(1e3);
    pathway::my_time_t t;
    pathway::index_int_t curr_spe = 10;
    // species object
    auto s_obj = nPtr->_get_species(curr_spe);
    auto when_R_where = nPtr->pathway_forward(t, curr_spe);
    if (when_R_where.R != -1) {
        // is reaction a sink reaction of curr species
        bool found = false;
        for (auto p : s_obj.reaction_k_index_s_coef_v) {
            if (when_R_where.R == p.first) {
                found = true;
                break;
            }
        }
        EXPECT_EQ(found, true);
        // is end species a out species of sink reaction
        found = false;
        auto r_obj = nPtr->_get_reaction(when_R_where.R);
        for (auto e : r_obj.out_spe_Gamma_v) {
            if (e.index == when_R_where.s) {
                found = true;
                break;
            }
        }
        // end species object
        auto end_obj = nPtr->_get_species(when_R_where.s);
        printf("source species: %s, sink reaction: %s, end species: %s\n",
               s_obj.name.c_str(), r_obj.name.c_str(), end_obj.name.c_str());
        EXPECT_EQ(found, true);
    }

    printf("when: %f, reaction: %d, where: %d\n", when_R_where.t,
           when_R_where.R, when_R_where.s);
}

TEST_P(NetworkI9Test, PathwaySimulate) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    pathway::index_int_t curr_spe = 10;
    pathway::my_time_t end_t = 10.0;
    auto path = nPtr->pathway_sim(0.0, end_t, curr_spe);
    printf("pathway: %s\n", path.c_str());
    std::vector<pathway::index_int_t> spe_v, rxn_v;
    pathway::pathway_to_vector(path, spe_v, rxn_v);
    ASSERT_TRUE(spe_v.size() == rxn_v.size() + 1);
    bool found = false;
    for (std::size_t i = 0; i < rxn_v.size(); ++i) {
        // is reaction a sink reaction of curr species
        found = false;
        auto s_obj = nPtr->_get_species(spe_v[i]);
        for (auto p : s_obj.reaction_k_index_s_coef_v) {
            if (rxn_v[i] == p.first) {
                found = true;
                break;
            }
        }
        EXPECT_EQ(found, true);
        // is end species a out species of sink reaction
        found = false;
        auto r_obj = nPtr->_get_reaction(rxn_v[i]);
        for (auto e : r_obj.out_spe_Gamma_v) {
            if (e.index == spe_v[i + 1]) {
                found = true;
                break;
            }
        }
        EXPECT_EQ(found, true);
    }
    // gen_pathway
    pathway::statistics st;
    int Ntraj = 1000;
    nPtr->gen_pathway(0.0, end_t, curr_spe, 1000, st);
    auto path_count = st.return_sorted_path_count();
    int Ntraj_real = 0;
    for (size_t i = 0; i < path_count.size(); ++i) {
        Ntraj_real += path_count[i].second;
        if (i < 10) {
            printf("path: %s, count: %d\n", path_count[i].first.c_str(),
                   path_count[i].second);
        };
    }
    EXPECT_EQ(Ntraj, Ntraj_real) << "gen pathway simulation";
}

TEST_P(NetworkI9Test, SpeciesSinkReactionGamma) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    pathway::index_int_t curr_spe = 15;
    pathway::my_time_t curr_t = 1.0;
    nPtr->update_out_reaction_rate(curr_spe, curr_t);
    // sink reactions of species 15, co2, 14 sink reactions in total
    auto ss_gamma = nPtr->spe_sink_by_a_reaction_Gamma(curr_spe, 47);
    EXPECT_NEAR(ss_gamma, 1.0 / 14, 1e-6);
    printf("spe sink reaction gamma: %f\n", ss_gamma);
    ss_gamma = nPtr->spe_sink_by_a_reaction_Gamma(curr_spe, 80);
    EXPECT_NEAR(ss_gamma, 1.0 / 14, 1e-6);
    printf("spe sink reaction gamma: %f\n", ss_gamma);
    ss_gamma = nPtr->spe_sink_by_a_reaction_Gamma(curr_spe, 647);
    EXPECT_NEAR(ss_gamma, 1.0 / 14, 1e-6);
    printf("spe sink reaction gamma: %f\n", ss_gamma);
    // not sink reactions of species 15, co2
    ss_gamma = nPtr->spe_sink_by_a_reaction_Gamma(curr_spe, 0);
    EXPECT_NEAR(ss_gamma, 0.0, 1e-6);
    printf("spe sink reaction gamma: %f\n", ss_gamma);
}

TEST_P(NetworkI9Test, ReactionOutSpeciesGamma) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    // reaction 47
    // out species are 8:o, 14:co
    auto os_gamma = nPtr->reaction_out_spe_Gamma(47, 8);
    EXPECT_NEAR(os_gamma, 1.0 / 2, 1e-6);
    printf("species gamma: %f\n", os_gamma);
    os_gamma = nPtr->reaction_out_spe_Gamma(47, 14);
    EXPECT_NEAR(os_gamma, 1.0 / 2, 1e-6);
    printf("species gamma: %f\n", os_gamma);
    // invalid out species
    os_gamma = nPtr->reaction_out_spe_Gamma(47, 0);
    EXPECT_NEAR(os_gamma, 0.0, 1e-6);
    printf("species gamma: %f\n", os_gamma);
}

TEST_P(NetworkI9Test, ReactionSpeciesGamma) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    pathway::index_int_t curr_spe = 15;
    pathway::my_time_t curr_t = 1.0;
    nPtr->update_out_reaction_rate(curr_spe, curr_t);
    // sink reactions of species 15, co2, 14 sink reactions in total
    // out species are 8:o, 14:co
    auto rs_gamma = nPtr->cal_r_s_Gamma(curr_t, curr_spe, 47, 8);
    EXPECT_NEAR(rs_gamma.r, 1.0 / 14, 1e-6);
    EXPECT_NEAR(rs_gamma.s, 1.0 / 2, 1e-6);
    printf("reaction gamma: %f, species gamma: %f\n", rs_gamma.r, rs_gamma.s);
    rs_gamma = nPtr->cal_r_s_Gamma(curr_t, curr_spe, 47, 14);
    EXPECT_NEAR(rs_gamma.r, 1.0 / 14, 1e-6);
    EXPECT_NEAR(rs_gamma.s, 1.0 / 2, 1e-6);
    printf("reaction gamma: %f, species gamma: %f\n", rs_gamma.r, rs_gamma.s);
    // invalid out species
    rs_gamma = nPtr->cal_r_s_Gamma(curr_t, curr_spe, 47, 0);
    EXPECT_NEAR(rs_gamma.r, 1.0 / 14, 1e-6);
    EXPECT_NEAR(rs_gamma.s, 0.0, 1e-6);
    printf("reaction gamma: %f, species gamma: %f\n", rs_gamma.r, rs_gamma.s);
    // invalid out reaction
    rs_gamma = nPtr->cal_r_s_Gamma(curr_t, curr_spe, 0, 0);
    EXPECT_NEAR(rs_gamma.r, 0.0, 1e-6);
    EXPECT_NEAR(rs_gamma.s, 0.0, 1e-6);
    printf("reaction gamma: %f, species gamma: %f\n", rs_gamma.r, rs_gamma.s);
}

TEST_P(NetworkI9Test, PathwayProbSim) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    // valid pathway, S10R526S43R527S10R89S8
    std::vector<pathway::index_int_t> s_vec = {10, 43, 10, 8};
    std::vector<pathway::index_int_t> r_vec = {526, 527, 89};
    auto prob = nPtr->pathway_prob_sim(0.0, 10, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - long pathway";
    s_vec = {15, 8};
    r_vec = {47};
    prob = nPtr->pathway_prob_sim(0.0, 10, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - short pathway";
    // valid pathway - single species
    s_vec = {15};
    r_vec = {};
    prob = nPtr->pathway_prob_sim(0.0, 10, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - single species\n";
    // invalid pathway - invalid sink reaction
    s_vec = {15, 8};
    r_vec = {0};
    prob = nPtr->pathway_prob_sim(0.0, 10, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_EQ(prob, 0.0) << "invalid pathway - invalid sink reaction\n";
    // invalid pathway - invalid out species
    s_vec = {15, 0};
    r_vec = {47};
    prob = nPtr->pathway_prob_sim(0.0, 10, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_EQ(prob, 0.0) << "invalid pathway - invalid out species\n";
}

INSTANTIATE_TEST_SUITE_P(Ingetration, NetworkI9Test,
                         testing::Values(io_filename()));

struct NetworkI9TestLinearSystem : NetworkI9Base,
                                   testing::WithParamInterface<io_filename> {
    NetworkI9TestLinearSystem() { followed_atom = "q1"; }
};

TEST_P(NetworkI9TestLinearSystem, Query) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());

    nPtr->Query(io_fn.query_fn);
}

TEST_P(NetworkI9TestLinearSystem, PathwaySimulate) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    pathway::index_int_t curr_spe = 0;
    pathway::my_time_t end_t = 1e6;
    auto path = nPtr->pathway_sim(0.0, end_t, curr_spe);
    // if end time is large enough, path shall converge
    EXPECT_EQ(path, "S0R0S1R1S2");
    printf("pathway: %s\n", path.c_str());
    // gen_pathway
    pathway::statistics st;
    int Ntraj = 1000;
    nPtr->gen_pathway(0.0, 1.0, 0, 1000, st);
    auto path_count = st.return_sorted_path_count();
    int Ntraj_real = 0;
    for (size_t i = 0; i < path_count.size(); ++i) {
        Ntraj_real += path_count[i].second;
        if (i < 10) {
            printf("path: %s, count: %d\n", path_count[i].first.c_str(),
                   path_count[i].second);
        };
    }
    EXPECT_EQ(Ntraj, Ntraj_real) << "gen pathway simulation - Ntraj";
    EXPECT_EQ(path_count.size(), 3) << "gen pathway simulation - N";
}

TEST_P(NetworkI9TestLinearSystem, PathwayProbSim) {
    auto io_fn = GetParam();
    ASSERT_TRUE(io_fn.spe_fn != "");
    ASSERT_TRUE(io_fn.rxn_fn != "");
    ASSERT_TRUE(io_fn.pr_fn != "");
    ASSERT_TRUE(io_fn.time_fn != "");
    ASSERT_TRUE(io_fn.k_fn != "");
    ASSERT_TRUE(io_fn.rr_fn != "");
    nPtr = std::make_shared<pathway::Network>(io_fn.spe_fn, io_fn.rxn_fn,
                                              io_fn.pr_fn, this->followed_atom,
                                              0, pathway::New_prop_ptr());
    auto ns = nPtr->get_network_summary();
    nPtr->prop->init_from_file(io_fn.time_fn, io_fn.k_fn, io_fn.rr_fn,
                               ns.n_species, ns.n_reaction, int(1e9));
    // test
    // valid pathway, S0R0S1R1S2
    std::vector<pathway::index_int_t> s_vec = {0, 1, 2};
    std::vector<pathway::index_int_t> r_vec = {0, 1};
    pathway::my_time_t end_t = 3.0;
    auto prob = nPtr->pathway_prob_sim(0.0, end_t, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - long pathway";
    // pathway probability approximation
    int Ntraj = 10000;
    auto prob_approx =
        nPtr->pathway_prob_approx(0.0, end_t, s_vec, r_vec, Ntraj);
    auto exact_pp = 1.0 + 1.0 * exp(-2.0 * end_t) - 2.0 * exp(-1.0 * end_t);
    printf("Real pp approximation: %e, expected pp: %e\n", prob_approx,
           exact_pp);
    // k0 = 1.0, k1 = 2.0, k2 = 3.0, S2 is a stable species
    EXPECT_NEAR(prob_approx, exact_pp, 1e-2)
        << "valid pathway - long pathway - pathway prob approx\n";
    // pathway probability approximation for time points vector
    vector<pathway::my_time_t> t_vec = {0.0, 1.0, 2.0, 3.0};
    vector<pathway::value_t> pp_vec;
    nPtr->pathway_prob_approx_ts(t_vec, s_vec, r_vec, Ntraj, pp_vec);
    vector<pathway::value_t> exact_pp_vec(t_vec.size(), 0);
    for (int i = 1; i < exact_pp_vec.size(); ++i) {
        exact_pp_vec[i] =
            1.0 + 1.0 * exp(-2.0 * t_vec[i]) - 2.0 * exp(-1.0 * t_vec[i]);
        std::cout << boost::format(
                         "Real pp approximation: %e, expected pp: %e for time "
                         "point %f\n") %
                         pp_vec[i] % exact_pp_vec[i] % t_vec[i];
        EXPECT_NEAR(pp_vec[i], exact_pp_vec[i], 1e-2)
            << boost::format(
                   "valid pathway - long pathway - pathway prob approx for "
                   "time point %f") %
                   t_vec[i];
    }

    s_vec = {0, 1};
    r_vec = {0};
    end_t = 2.0;
    prob = nPtr->pathway_prob_sim(0.0, end_t, s_vec, r_vec);
    printf("\npp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - medium length pathway";
    // pathway probability approximation
    Ntraj = 10000;
    prob_approx = nPtr->pathway_prob_approx(0.0, end_t, s_vec, r_vec, Ntraj);
    exact_pp = 1.0 * exp(-1.0 * end_t) - 1.0 * exp(-2.0 * end_t);
    printf("Real pp approximation: %e, expected pp: %e\n", prob_approx,
           exact_pp);
    // k0 = 1.0, k1 = 2.0, k2 = 3.0
    EXPECT_NEAR(prob_approx, exact_pp, 1e-2)
        << "valid pathway - medium pathway - pathway prob approx";

    s_vec = {0};
    r_vec = {};
    end_t = 1.0;
    prob = nPtr->pathway_prob_sim(0.0, end_t, s_vec, r_vec);
    printf("pp simulation: %e\n", prob);
    EXPECT_GT(prob, 0.0) << "valid pathway - short pathway";
    // 0-step path, analytic formula for pathway probability
    EXPECT_NEAR(prob, exp(-1.0 * end_t), 1e-6)
        << "valid pathway - short pathway";
}

INSTANTIATE_TEST_SUITE_P(Ingetration, NetworkI9TestLinearSystem,
                         testing::Values(io_filename("linear-system")));

