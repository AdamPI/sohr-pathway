#include <cstdio>
#include "file_io.h"
#include "gtest/gtest.h"

TEST(CSVTest, intCSV) {
    int N = 3;
    auto mat = fileIO::read_topN_line_csv<int>(
        std::string("../tests/examples/csv_int_test1.txt").c_str(), N);
    EXPECT_EQ(N, mat.size());
    for (int i = 0; i < N; ++i) {
        EXPECT_EQ(N, mat[i].size());
    }

    EXPECT_EQ(11, mat[0][0]);
    EXPECT_EQ(12, mat[0][1]);
    EXPECT_EQ(13, mat[0][2]);
    EXPECT_EQ(21, mat[1][0]);
    EXPECT_EQ(22, mat[1][1]);
    EXPECT_EQ(23, mat[1][2]);
    EXPECT_EQ(31, mat[2][0]);
    EXPECT_EQ(32, mat[2][1]);
    EXPECT_EQ(33, mat[2][2]);
}

TEST(CSVTest, doubleCSV) {
    int N = 3;
    auto mat = fileIO::read_topN_line_csv<double>(
        std::string("../tests/examples/csv_double_test1.txt").c_str(), N);
    EXPECT_EQ(N, mat.size());
    for (int i = 0; i < N; ++i) {
        EXPECT_EQ(N, mat[i].size());
    }

    EXPECT_EQ(11.1, mat[0][0]);
    EXPECT_EQ(12, mat[0][1]);
    EXPECT_EQ(13, mat[0][2]);
    EXPECT_EQ(21, mat[1][0]);
    EXPECT_EQ(22, mat[1][1]);
    EXPECT_EQ(23, mat[1][2]);
    EXPECT_EQ(31, mat[2][0]);
    EXPECT_EQ(32, mat[2][1]);
    double expV = 33.3333333333333;
    printf("Expect: %.15f, got: %.15f, diff: %.15f\n", expV, mat[2][2],
           expV - mat[2][2]);
    EXPECT_EQ(33.3333333333333, mat[2][2]);
}
