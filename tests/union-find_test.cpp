#include "union-find.h"
#include "gtest/gtest.h"

TEST(UnionFindTest, rootUniteFind) {
    UnionFind uf(10);
    EXPECT_EQ(0, uf.root(0));
    EXPECT_EQ(false, uf.find(1, 3));
    uf.unite(1, 3);
    EXPECT_EQ(true, uf.find(1, 3));
    EXPECT_EQ(1, uf.root(3));
}
