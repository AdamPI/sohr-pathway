#include "random.h"
#include <vector>
#include "gtest/gtest.h"

TEST(RandomTest, zeroOrOne) {
    random_sohr::random randomObj(1);
    auto n = randomObj.return_0_or_1_evenly_randomly();
    std::cout << "Random 0 or 1: " << n << std::endl;
    EXPECT_TRUE(n == 0 || n == 1);
}

TEST(RandomTest, indexGivenProb) {
    random_sohr::random randomObj(1);
    std::vector<double> prob = {1001, 1000, 0.5, 0.6};
    auto n = randomObj.return_index_randomly_given_probability_vector(prob);
    std::cout << "Random index: " << n << std::endl;
    EXPECT_TRUE((n == 0) || (n == 1) || (n == 2) | (n == 3));
}

TEST(RandomTest, minMax) {
    random_sohr::random randomObj(1);
    auto n = randomObj.random_min_max(0.5, 1.8);
    std::cout << "Random double: " << n << std::endl;
    EXPECT_TRUE(n >= 0.5 && n <= 1.8);
}
