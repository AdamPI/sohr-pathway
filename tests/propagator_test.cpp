#include <cstdio>
#include <memory>

#include "gtest/gtest.h"
#include "string"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "propagator.h"

#define PROPTEST() (std::cout << "[Propagator TEST] ")

// Propagator test base struct
struct PropagatorBase : testing::Test {
    pathway::Propagator prop;
    PropagatorBase() {}
    virtual ~PropagatorBase() {}
    //
};

// test fixture
struct propSetting {
    // time filename
    std::string timeFn;
    // pseudo1k filenmae
    std::string kFn;
    // reaction rate filename
    std::string rrFn;
    // reaction index mapping file
    std::string rIdxMapFile;
    propSetting(const int code = 0) {
        switch (code) {
            case 0: {
                timeFn = std::string("../tests/examples/prop_time_test1.csv");
                kFn = std::string("../tests/examples/prop_k_test1.csv");
                rrFn = std::string("../tests/examples/prop_rr_test1.csv");
                break;
            }
            case 1: {
                timeFn = std::string("../tests/examples/n-propane/t5_time.csv");
                rIdxMapFile = std::string(
                    "../tests/examples/n-propane/mechanism_EXACT/"
                    "rxn_index_mapping.json");
                break;
            }
            default:
                break;
        }
    }
    propSetting(std::string _tf, std::string _kf) : timeFn(_tf), kFn(_kf) {}
    friend std::ostream &operator<<(std::ostream &os, const propSetting &obj) {
        os << "\nPropagator time file name: \"" << obj.timeFn << "\"\n";
        os << "Propagator pseudo1k file name: \"" << obj.kFn << "\"\n";
        os << "Propagator reaction rate file name: \"" << obj.rrFn << "\"\n";
        return os;
    }
};

struct PropagatorTest : PropagatorBase,
                        testing::WithParamInterface<propSetting> {};

TEST_P(PropagatorTest, APIs) {
    //
    auto propSettingObj = GetParam();
    ASSERT_TRUE(propSettingObj.timeFn != "");
    ASSERT_TRUE(propSettingObj.kFn != "");
    // init
    prop.init_from_file(propSettingObj.timeFn, propSettingObj.kFn,
                        propSettingObj.rrFn, 2, 3, int(1e6));
    // test
    PROPTEST() << "Testing PropagatorTest:APIs" << std::endl;
    // pseudo1k
    EXPECT_EQ(prop.get_pseudo1k(1, 0), 2.0);
    EXPECT_EQ(prop.get_pseudo1k(1, 1), 3.0);
    EXPECT_EQ(prop.get_pseudo1k(1, 2), 4.1);
    EXPECT_EQ(prop.get_pseudo1k(1, 3), 5.0e-3);
    EXPECT_EQ(prop.get_pseudo1k(1, 4), 6.0e10);
    EXPECT_EQ(prop.get_pseudo1k(1, 1e4), 6.0e10);
    // [c]umulative pseudo1k
    EXPECT_EQ(prop.get_c_pseudo1k(0, 0), 0.0);
    EXPECT_EQ(prop.get_c_pseudo1k(0, 1), 1.0);
    EXPECT_EQ(prop.get_c_pseudo1k(0, 2), 2.0);
    EXPECT_EQ(prop.get_c_pseudo1k(0, 3), 3.0);
    EXPECT_EQ(prop.get_c_pseudo1k(0, 4), 4.0);
    EXPECT_EQ(prop.get_c_pseudo1k(0, 1e4), 1e4);
    // reverse time from [c]umulative pseudo1k
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 0), 0.0);
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 1), 1.0);
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 2), 2.0);
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 3), 3.0);
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 4), 4.0);
    EXPECT_EQ(prop.time_from_c_pseudo1k(0, 1e4), 1e4);
    // rr
    EXPECT_EQ(prop.get_rr(2, 0), 3.0);
    EXPECT_EQ(prop.get_rr(2, 1), 4.0);
    EXPECT_EQ(prop.get_rr(2, 2), 5.0);
    EXPECT_EQ(prop.get_rr(2, 3), 6.0);
    EXPECT_EQ(prop.get_rr(2, 4), 7.0e13);
    EXPECT_EQ(prop.get_rr(2, 1e5), 7.0e13);
}

INSTANTIATE_TEST_SUITE_P(Ingetration, PropagatorTest,
                         testing::Values(propSetting()));

struct PropagatorUtil : PropagatorBase,
                        testing::WithParamInterface<propSetting> {};

TEST_P(PropagatorUtil, init_time_from_file) {
    // use n-propane system as an example
    auto propSettingObj = GetParam();
    ASSERT_TRUE(propSettingObj.timeFn != "");
    ASSERT_TRUE(propSettingObj.rIdxMapFile != "");

    // mapping
    std::map<int, int> dup_2_non;
    std::map<int, bool> dup_is_reversible;
    std::map<int, std::vector<int>> non_2_dup;
    pathway::read_rxn_mapping(propSettingObj.rIdxMapFile, dup_2_non, non_2_dup,
                              dup_is_reversible);
    // init
    int nRxn = 638, nSpe = 112;
    prop.init_time_from_file(propSettingObj.timeFn, nSpe, non_2_dup.size(),
                             int(1e6));
    std::vector<double> rr_fwd(nRxn), rr_bwd(nRxn);
    rr_fwd[101] = 3.0;
    rr_bwd[37] = 1e7;

    int time_idx = 2;
    prop.update_rr_per_t(time_idx, rr_fwd, rr_bwd, dup_2_non,
                         dup_is_reversible);
    // for test purpose only
    prop.init_pseudo1k_rr_ptr();
    int idx = dup_2_non.at(101);
    EXPECT_EQ(prop.get_rr(idx, prop.get_time(time_idx)), 3.0);
    idx = dup_2_non.at(37);
    if (dup_is_reversible.at(37)) {
        ++idx;
        EXPECT_EQ(prop.get_rr(idx, prop.get_time(time_idx)), 1e7);
    }
}

INSTANTIATE_TEST_SUITE_P(Ingetration, PropagatorUtil,
                         testing::Values(propSetting(1)));

TEST(Util, rxn_idx_mapping) {
    std::string fn = std::string(
        "../tests/examples/n-propane/mechanism_EXACT/rxn_index_mapping.json");
    std::map<int, int> dup_2_non;
    std::map<int, std::vector<int>> non_2_dup;
    std::map<int, bool> dup_is_reversible;
    pathway::read_rxn_mapping(fn, dup_2_non, non_2_dup, dup_is_reversible);

    int N1 = 1244;
    int N2 = 638;
    EXPECT_EQ(non_2_dup.size(), N1);
    EXPECT_EQ(dup_2_non.size(), N2);
    for (auto p : non_2_dup) {
        EXPECT_GE(p.second.size(), 1);
    }
    for (int i = 0; i < N1; ++i) {
        EXPECT_EQ(non_2_dup.count(i), 1);
    }
    for (int i = 0; i < N2; ++i) {
        EXPECT_EQ(dup_2_non.count(i), 1);
    }
}
