#include <cstdio>
#include "gtest/gtest.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "interp_1d.h"
#include "interp_linear.h"

TEST(CubicSplineTest, linear_interp) {
    std::vector<double> x_v = {1, 2, 3, 4};
    std::vector<double> y_v = {1, 2, 3, 4};
    auto ptr1 = std::make_shared<Linear_interp>(x_v, y_v);
    EXPECT_DOUBLE_EQ(1, ptr1->interp(1));
    EXPECT_DOUBLE_EQ(0.5, ptr1->interp(0.5));
    EXPECT_EQ(1000.5, ptr1->interp(1000.5));
}

TEST(CubicSplineTest, Ploy_interp) {
    std::vector<double> x_v = {1, 2, 3, 4};
    std::vector<double> y_v = {1, 2, 3, 4};
    // four points means 3rd order of polynomial
    auto ptr1 = std::make_shared<Poly_interp>(x_v, y_v, 3);
    EXPECT_EQ(1, ptr1->interp(1));
    double expV = 0.5;
    double actualV = ptr1->interp(0.5);
    printf("Expect: %.15f, got: %.15f, diff: %.15f\n", expV, actualV,
           expV - actualV);
    EXPECT_DOUBLE_EQ(0.5, ptr1->interp(0.5));
    EXPECT_DOUBLE_EQ(1000.5, ptr1->interp(1000.5));
}

TEST(CubicSplineTest, Spline_interp) {
    std::vector<double> x_v = {1, 2, 3, 4};
    std::vector<double> y_v = {1, 2, 3, 4};
    auto ptr1 = std::make_shared<Spline_interp>(x_v, y_v, 3);
    EXPECT_EQ(1, ptr1->interp(1));
    double expV = -1.548076923076923;
    double actualV = ptr1->interp(0.5);
    printf("Expect: %.15f, got: %.15f, diff: %.15f\n", expV, actualV,
           expV - actualV);
    EXPECT_NEAR(-1.548076923076923, ptr1->interp(0.5), 1e-5);
    expV = 76119132.08653844;
    actualV = ptr1->interp(1000.5);
    printf("Expect: %.15f, got: %.15f, diff: %.15f\n", expV, actualV,
           expV - actualV);
    EXPECT_NEAR(76119132.08653844, ptr1->interp(1000.5), 1e-5);
}
