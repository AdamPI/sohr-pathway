#include <boost/graph/adjacency_list.hpp>
#include <cstdio>
#include <map>
#include <vector>
#include "boost/generator_iterator.hpp"
#include "boost/random.hpp"
#include "gtest/gtest.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network.h"

struct GraphTestBase : testing::Test {
    pathway::Network n;
    GraphTestBase() {}
    virtual ~GraphTestBase() {}
};

TEST_F(GraphTestBase, GraphEmpty) {
    EXPECT_EQ(0, n.get_num_vertices());
    EXPECT_EQ(0, n.getVertexCount());
    EXPECT_EQ(0, n.get_num_edges());
    EXPECT_EQ(0, n.getEdgeCount());
}

// test fixture
struct graph_state {
    // vertex information
    std::vector<pathway::VertexProperties_graph> vertex_info;
    // edge property
    std::vector<pathway::EdgeProperties_graph> edgePro;
    // two vertices, one edge for example
    graph_state() {
        // vertex
        pathway::VertexProperties_graph v0;
        v0.species_index = 0;
        v0.vertex_index = 0;
        vertex_info.push_back(v0);
        pathway::VertexProperties_graph v1;
        v1.species_index = 1;
        v1.vertex_index = 1;
        vertex_info.push_back(v1);
        // edge
        pathway::EdgeProperties_graph e1;
        e1.src = 0;
        e1.dest = 1;
        e1.edge_index = 0;
        e1.reaction_key = 0;
        e1.edge_weight = 1000;
        edgePro.push_back(e1);
        pathway::EdgeProperties_graph e2;
        e2.src = 1;
        e2.dest = 0;
        e2.edge_index = 1;
        edgePro.push_back(e2);
    }
    // random graph
    graph_state(int n_vertex) {
        typedef boost::mt19937 RNGType;
        RNGType rng;
        boost::uniform_int<> one_to_n(1, n_vertex);
        boost::variate_generator<RNGType, boost::uniform_int<> > dice(rng,
                                                                      one_to_n);
        vertex_info.resize(n_vertex);
        pathway::index_int_t s_idx = 0;
        pathway::index_int_t v_idx = 0;
        for (auto &v : vertex_info) {
            s_idx += dice();
            v.species_index = s_idx;
            v.vertex_index = v_idx++;
        }
        int count = 0;
        int r_idx = 0;
        for (size_t i = 0; i < vertex_info.size(); ++i) {
            for (size_t j = 0; j < vertex_info.size(); ++j) {
                if (i == j) continue;
                if (dice() > 0.5 * (1 + n_vertex)) {
                    pathway::EdgeProperties_graph e;
                    e.src = i;
                    e.dest = j;
                    e.edge_index = count++;
                    e.reaction_key = (r_idx += dice());
                    e.edge_weight = dice() * 10.0;
                    edgePro.push_back(e);
                }
            }
        }
    }
    friend std::ostream &operator<<(std::ostream &os, const graph_state &obj) {
        os << "\n#vertex: " << obj.vertex_info.size()
           << " #edge: " << obj.edgePro.size() << "\n";
        return os;
    };
};

struct GraphTest : GraphTestBase, testing::WithParamInterface<graph_state> {
    GraphTest() {}
};

TEST_P(GraphTest, VertexProperties) {
    auto gs = GetParam();
    n.init_graph(gs.vertex_info, gs.edgePro);
    EXPECT_EQ(gs.vertex_info.size(), n.get_num_vertices());
    EXPECT_EQ(gs.vertex_info.size(), n.getVertexCount());

    std::map<std::size_t, size_t> expInDegreeMap;
    std::map<std::size_t, size_t> expOutDegreeMap;
    for (auto e : gs.edgePro) {
        expOutDegreeMap[e.src] += 1;
        expInDegreeMap[e.dest] += 1;
    }
    for (auto p : expOutDegreeMap) {
        printf("Vertex: %lu, outDegree: %lu\n", p.first, p.second);
    }

    int idx = 0;
    for (auto vs = n.getVertices(); vs.first != vs.second; ++vs.first) {
        EXPECT_EQ(gs.vertex_info[idx].vertex_index,
                  n.properties(*vs.first).vertex_index);
        printf("Vertex InDegree: %lu\n", n.getInDegree(*vs.first));
        printf("Vertex outDegree: %lu\n", n.getOutDegree(*vs.first));
        printf("Vertex Degree: %lu\n", n.getVertexDegree(*vs.first));
        size_t inD =
            expInDegreeMap.count(idx) == 0 ? 0 : expInDegreeMap.at(idx);
        EXPECT_EQ(inD, n.getInDegree(*vs.first));
        size_t outD =
            expOutDegreeMap.count(idx) == 0 ? 0 : expOutDegreeMap.at(idx);
        EXPECT_EQ(outD, n.getOutDegree(*vs.first));
        EXPECT_EQ(inD + outD, n.getVertexDegree(*vs.first));

        ++idx;
    }
}

TEST_P(GraphTest, EdgeProperties) {
    auto gs = GetParam();
    n.init_graph(gs.vertex_info, gs.edgePro);
    EXPECT_EQ(gs.edgePro.size(), n.getEdgeCount());
    EXPECT_EQ(gs.edgePro.size(), n.get_num_edges());

    int idx = 0;
    for (auto er = n.getEdges(); er.first != er.second; ++er.first) {
        // edge descriptor is index like 0,1,2... if OutEdgeList selector is
        // type of vecS, don't try to access descriptor itself, e.g.
        // printf(*er.first) because that will increment the descriptor, i.e.
        // er.first += 1
        printf("Edge src: %d\n", n.properties(*er.first).src);
        printf("Edge dest: %d\n", n.properties(*er.first).dest);
        printf("Edge index: %d\n", n.properties(*er.first).edge_index);
        printf("Edge weight: %f\n", n.properties(*er.first).edge_weight);
        printf("Edge reaction index: %d\n",
               n.properties(*er.first).reaction_key);
        EXPECT_EQ(gs.edgePro[idx].edge_index,
                  n.properties(*er.first).edge_index);
        EXPECT_EQ(gs.edgePro[idx].edge_weight,
                  n.properties(*er.first).edge_weight);
        ++idx;
    }
}

INSTANTIATE_TEST_SUITE_P(GraphBasic, GraphTest,
                         testing::Values(graph_state(), graph_state(1),
                                         graph_state(5)));

