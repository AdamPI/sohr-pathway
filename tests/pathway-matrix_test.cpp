#include <iostream>
#include "gtest/gtest.h"
#include "matrix-sohr.h"

TEST(PathwayMatrixTest, toString) {
    // path_t
    pathway::path_t p0;
    EXPECT_EQ("", pathway::toString(p0));
    pathway::path_t p1 = {};
    EXPECT_EQ("", pathway::toString(p1));
    pathway::path_t p2 = {0, 1, 2};
    EXPECT_EQ("R0R1R2", pathway::toString(p2));

    // S matrix
    pathway::S_matrix_t sm0;
    EXPECT_EQ("", pathway::toString(sm0));
    sm0.resize(2);
    sm0[0] = {0, 1};
    EXPECT_EQ("[] [S0#1S1]\n", pathway::toString(sm0));
    sm0[1] = {50, 100};
    EXPECT_EQ("[] [S0#1S1]\n[S1#50S0] [S1#100S1]\n", pathway::toString(sm0));

    // SR matrix
    pathway::SR_matrix_t rm0;
    EXPECT_EQ("", pathway::toString(rm0));
    pathway::SR_matrix_t rm1;
    rm1.resize(2);
    // each matrix element is a matrix
    rm1[0] = {{}, {{1}}};
    rm1[1] = {{{5}}, {{6}}};
    EXPECT_EQ("[] [S0R1S1]\n[S1R5S0] [S1R6S1]\n", pathway::toString(rm1));
    rm1[1] = {{{5}}, {{6, 7}}};
    EXPECT_EQ("[] [S0R1S1]\n[S1R5S0] [S1R6R7S1]\n", pathway::toString(rm1));
}

TEST(PathwayMatrixTest, S_matrix) {
    pathway::S_matrix_t m1;
    const int N = 5;
    m1.resize(N);
    m1[0] = {0, 1, 1, 1, 1};
    m1[1] = {0, 0, 1, 1, 1};
    m1[2] = {0, 0, 0, 1, 1};
    m1[3] = {0, 0, 0, 0, 1};
    m1[4] = {0, 0, 0, 0, 0};

    pathway::S_matrix_t expMat1 = {
        {0, 0, 1, 2, 3}, {0, 0, 0, 1, 2}, {0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0},
    };
    pathway::S_matrix_t expMat2 = {
        {0, 0, 0, 1, 3}, {0, 0, 0, 0, 1}, {0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0},
    };

    // test matrix multiplication
    pathway::S_matrix_t m2 = pathway::matrix_multiplication(m1, m1);
    EXPECT_EQ(N, m2.size());
    for (auto x : m2) EXPECT_EQ(N, x.size());
    for (size_t i = 0; i < expMat1.size(); ++i) {
        for (size_t j = 0; j < expMat1[i].size(); ++j) {
            EXPECT_EQ(expMat1[i][j], m2[i][j]);
        }
    }

    // test matrix power
    auto m3 = pathway::matrix_power(m1, 3);
    EXPECT_EQ(N, m3.size());
    for (auto x : m3) EXPECT_EQ(N, x.size());
    for (size_t i = 0; i < expMat2.size(); ++i) {
        for (size_t j = 0; j < expMat2[i].size(); ++j) {
            EXPECT_EQ(expMat2[i][j], m3[i][j]);
        }
    }
}

TEST(PathwayMatrixTest, SR_matrix) {
    pathway::SR_matrix_t m1;
    m1.resize(5);
    // each matrix element is a matrix
    m1[0] = {{}, {{1}}, {{2}}, {{3}}, {{4}}};
    m1[1] = {{}, {}, {{5}}, {{6}}, {{7}}};
    m1[2] = {{}, {}, {}, {{8}}, {{9}}};
    m1[3] = {{}, {}, {}, {}, {{10}, {11}}};
    m1[4] = {{}, {}, {}, {}, {}};

    std::cout << pathway::toString(m1) << std::endl;
    // [] [S0R1S1] [S0R2S2] [S0R3S3] [S0R4S4]
    // [] [] [S1R5S2] [S1R6S3] [S1R7S4]
    // [] [] [] [S2R8S3] [S2R9S4]
    // [] [] [] [] [S3R10S4,S3R11S4]
    // [] [] [] [] []
    EXPECT_EQ(
        "[] [S0R1S1] [S0R2S2] [S0R3S3] [S0R4S4]\n[] [] [S1R5S2] [S1R6S3] "
        "[S1R7S4]\n[] [] [] [S2R8S3] [S2R9S4]\n[] [] [] [] "
        "[S3R10S4,S3R11S4]\n[] [] [] [] []\n",
        pathway::toString(m1));
    auto m2 = pathway::matrix_multiplication(m1, m1);
    std::cout << pathway::toString(m2) << std::endl;
    // [] [] [S0R1R5S2] [S0R1R6S3,S0R2R8S3]
    // [S0R1R7S4,S0R2R9S4,S0R3R10S4,S0R3R11S4]
    // [] [] [] [S1R5R8S3] [S1R5R9S4,S1R6R10S4,S1R6R11S4]
    // [] [] [] [] [S2R8R10S4,S2R8R11S4]
    // [] [] [] [] []
    // [] [] [] [] []
    EXPECT_EQ(
        "[] [] [S0R1R5S2] [S0R1R6S3,S0R2R8S3] "
        "[S0R1R7S4,S0R2R9S4,S0R3R10S4,S0R3R11S4]\n[] [] [] [S1R5R8S3] "
        "[S1R5R9S4,S1R6R10S4,S1R6R11S4]\n[] [] [] [] [S2R8R10S4,S2R8R11S4]\n[] "
        "[] [] [] []\n[] [] [] [] []\n",
        pathway::toString(m2));
    auto m3 = pathway::matrix_power(m1, 3);
    EXPECT_EQ(
        "[] [] [] [S0R1R5R8S3] "
        "[S0R1R5R9S4,S0R1R6R10S4,S0R1R6R11S4,S0R2R8R10S4,S0R2R8R11S4]\n[] [] "
        "[] [] [S1R5R8R10S4,S1R5R8R11S4]\n[] [] [] [] []\n[] [] [] [] []\n[] "
        "[] [] [] []\n",
        pathway::toString(m3));
}
