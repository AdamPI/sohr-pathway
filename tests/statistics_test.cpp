#include <cstdio>
#include <map>
#include "gtest/gtest.h"
#include "path_statistics.h"

TEST(PathStatisticsTest, readFile) {
    pathway::statistics st;
    st.read_csv_file(
        std::string("../tests/examples/pathway_stat_test2.csv").c_str());
    auto str_int_v = st.return_sorted_path_count();
    for (auto pn : str_int_v) {
        printf("species: %s, count: %d\n", pn.first.c_str(), pn.second);
    }
    std::map<std::string, int> expMap;
    expMap.emplace("S3", 10000);
    expMap.emplace("S1", 1000);
    expMap.emplace("S2", 1000);
    expMap.emplace("S5", 1000);
    for (auto pn : str_int_v) {
        EXPECT_EQ(pn.second, expMap.at(pn.first));
    }
}
