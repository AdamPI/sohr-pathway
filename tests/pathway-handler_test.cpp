#include "pathway-handler.h"
#include <cstdio>
#include <string>
#include <vector>
#include "gtest/gtest.h"

TEST(PathwayHandlerTest, readPath) {
    std::vector<std::string> path_v;
    int N = 4;
    pathway::read_ff(
        std::string("../tests/examples/pathway_stat_test1.csv").c_str(), path_v,
        N);
    for (std::size_t i = 0; i < path_v.size(); ++i) {
        printf("path %lu: %s\n", i, path_v[i].c_str());
    }
    EXPECT_EQ(N, path_v.size());
    EXPECT_EQ("S1", path_v[0]);
    EXPECT_EQ("S1R1S2", path_v[1]);
    EXPECT_EQ("S2R2S3", path_v[2]);
    EXPECT_EQ("S1R1S2R2S3R3S4R4S5", path_v[3]);
}

TEST(PathwayHandlerTest, pathwayToVector) {
    std::string p = "S1R1S2";
    std::vector<pathway::index_int_t> spe_v;
    std::vector<pathway::index_int_t> rxn_v;
    pathway::pathway_to_vector(p, spe_v, rxn_v);
    EXPECT_EQ(2, spe_v.size());
    EXPECT_EQ(1, spe_v[0]);
    EXPECT_EQ(2, spe_v[1]);
    EXPECT_EQ(1, rxn_v.size());
    EXPECT_EQ(1, rxn_v[0]);
}

TEST(PathwayHandlerTest, vectorEndsWith) {
    std::vector<std::string> v_in = {"S1", "S2R2S2", "S1R2S1"};
    std::vector<std::string> v_out;
    pathway::ends_with("S1", v_in, v_out);
    for (std::size_t i = 0; i < v_out.size(); ++i) {
        printf("path %lu: %s\n", i, v_out[i].c_str());
    }
    EXPECT_EQ("S1", v_out[0]);
    EXPECT_EQ("S1R2S1", v_out[1]);
}

TEST(PathwayHandlerTest, vectorStartsWith) {
    std::vector<std::string> v_in = {"S1", "S2R2S2", "S1R2S1"};
    std::vector<std::string> v_out;
    pathway::starts_with("S1", v_in, v_out);
    for (std::size_t i = 0; i < v_out.size(); ++i) {
        printf("path %lu: %s\n", i, v_out[i].c_str());
    }
    EXPECT_EQ("S1", v_out[0]);
    EXPECT_EQ("S1R2S1", v_out[1]);
}

TEST(PathwayHandlerTest, vectorMerge) {
    std::vector<std::string> v1 = {"S1", "S2R2S2", "S1R2S1"};
    std::vector<std::string> v2 = {"S1", "S1R2S3"};
    pathway::merge(v1, v2);
    for (std::size_t i = 0; i < v1.size(); ++i) {
        printf("path %lu: %s\n", i, v1[i].c_str());
    }
    EXPECT_EQ("S1", v1[0]);
    EXPECT_EQ("S1R2S1", v1[1]);
    EXPECT_EQ("S1R2S3", v1[2]);
    EXPECT_EQ("S2R2S2", v1[3]);
}

TEST(PathwayHandlerTest, multimapGreaterEndsWith) {
    std::multimap<double, std::string, std::greater<double> > p_map;
    p_map.emplace(7.0, "S0");
    p_map.emplace(8.0, "S2R2S2");
    p_map.emplace(9, "S1R2S1");
    p_map.emplace(10.0, "S3R2S1");
    auto v_out = pathway::ends_with(p_map, 4, 4);
    for (std::size_t i = 0; i < v_out.size(); ++i) {
        printf("path %lu: %s\n", i, v_out[i].c_str());
    }
    EXPECT_EQ("S3R2S1", v_out[0]);
    EXPECT_EQ("S1R2S1", v_out[1]);
    EXPECT_EQ("S2R2S2", v_out[2]);
    EXPECT_EQ("S0", v_out[3]);
}

TEST(PathwayHandlerTest, multimapLessEndsWith) {
    std::multimap<double, std::string, std::less<double> > p_map;
    p_map.emplace(7.0, "S0");
    p_map.emplace(8.0, "S2R2S2");
    p_map.emplace(9, "S1R2S1");
    p_map.emplace(10.0, "S3R2S1");
    auto v_out = pathway::ends_with(p_map, 4, 4);
    for (std::size_t i = 0; i < v_out.size(); ++i) {
        printf("path %lu: %s\n", i, v_out[i].c_str());
    }
    EXPECT_EQ("S0", v_out[0]);
    EXPECT_EQ("S2R2S2", v_out[1]);
    EXPECT_EQ("S1R2S1", v_out[2]);
    EXPECT_EQ("S3R2S1", v_out[3]);
}

