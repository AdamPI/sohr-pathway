#include <chrono>
#include <cstdio>
#include <thread>
#include "gtest/gtest.h"
#include "myclock.h"

TEST(TimeTest, unitOfSecond) {
    MyClock clk;
    clk.begin();

    std::this_thread::sleep_for(std::chrono::seconds(1));

    clk.end();
    auto d = clk.GetHowLong();
    printf("Time elapsed: %f seconds\n", d);
    EXPECT_GE(d, 1);
    EXPECT_LE(d, 10);
}
