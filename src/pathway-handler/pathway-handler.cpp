#ifndef __PATHWAYHANDLER_CPP_
#define __PATHWAYHANDLER_CPP_
#include <boost/algorithm/string/predicate.hpp>  //for boost::algorithm::ends_with
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>  //for boost::regex
#include <boost/smart_ptr.hpp>
#include <boost/tokenizer.hpp>
#include <iostream>
#include <iterator>
#include <numeric>
#include <set>
#include <sstream>  //for stringstream

#include "pathway-handler.h"

namespace pathway {
// read from file (ff) "pathway_sim.csv" to get a vector of topN pathways
void read_ff(std::string filename, std::vector<std::string>& pathway_vec,
             size_t topN) {
    std::ifstream fin(filename.c_str());
    pathway_vec.resize(0);

    std::string s;
    size_t icounter = 0;
    // while((icounter++<topN)&&(fin.good())){
    while ((icounter++ < topN) && (fin.good())) {
        try {
            std::getline(fin, s);
        } catch (std::ifstream::failure& e) {
            std::cerr << e.what() << "Exception opening/reading/closing file\n";
        }
        // there may be blank lines
        if (s.size() > 0) {
            boost::char_separator<char> sep(",");
            typedef boost::tokenizer<boost::char_separator<char>> t_tokenizer;
            t_tokenizer tok(s, sep);
            // just push back pathway name
            pathway_vec.push_back(std::string(*(tok.begin())));
            // for(boost::tokenizer<>::iterator beg=tok.begin(); beg!=tok.end();
            // ++beg){ cout << *beg << "\n";
            //}
        }
    }

    fin.clear();
    fin.close();
}

void pathway_to_vector(const std::string& path, std::vector<index_int_t>& spe_v,
                       std::vector<index_int_t>& rxn_v) {
    spe_v.resize(0);
    rxn_v.resize(0);

    const char* pattern1 = "(S\\d+(?:R[-]?\\d+)?)";
    boost::regex re1(pattern1);

    boost::sregex_iterator it1(path.begin(), path.end(), re1);
    boost::sregex_iterator end1;
    std::vector<std::string> reaction_spe;
    for (; it1 != end1; ++it1) {
        reaction_spe.push_back(it1->str());
    }

    const char* pattern2 = "S(\\d+)(?:R(-?\\d+))?";
    boost::regex re2(pattern2);

    for (size_t i = 0; i < reaction_spe.size(); i++) {
        std::vector<std::string> idx_str;

        boost::smatch result2;
        if (boost::regex_search(reaction_spe[i], result2, re2)) {
            for (std::size_t mi = 1; mi < result2.size(); ++mi) {
                std::string s_rxn_idx(result2[mi].first, result2[mi].second);
                idx_str.push_back(s_rxn_idx);
            }
        }

        spe_v.push_back(boost::lexical_cast<index_int_t>(idx_str[0]));

        if (idx_str[1] != std::string(""))
            rxn_v.push_back(boost::lexical_cast<index_int_t>(idx_str[1]));
        else {
            // not the last species
            if (i != reaction_spe.size() - 1) rxn_v.push_back(INT_MAX);
        }
    }
}

void ends_with(std::string spe, const std::vector<std::string>& str_vec_in,
               std::vector<std::string>& str_vec_out, size_t topN) {
    size_t icount = 0;
    for (size_t i = 0; (icount < topN) && (i < str_vec_in.size()); ++i) {
        if (boost::algorithm::ends_with(str_vec_in[i], spe)) {
            ++icount;
            str_vec_out.push_back(str_vec_in[i]);
        }
    }
}

void starts_with(std::string spe, const std::vector<std::string>& str_vec_in,
                 std::vector<std::string>& str_vec_out, size_t topN) {
    size_t icount = 0;
    for (size_t i = 0; (icount < topN) && (i < str_vec_in.size()); ++i) {
        if (boost::algorithm::starts_with(str_vec_in[i], spe)) {
            ++icount;
            str_vec_out.push_back(str_vec_in[i]);
        }
    }
}

// parse the topN pathways for every species from a multimap
// n_spe is the total number of species
std::vector<std::string> ends_with(
    const std::multimap<double, std::string, std::greater<double>>& p_map,
    std::size_t n_spe, std::size_t topN) {
    std::vector<std::string> v;
    std::vector<std::size_t> v_counter(n_spe, 0);
    for (auto x : p_map) {
        // if all species has topN path endwith itself, break
        if (std::accumulate(v_counter.begin(), v_counter.end(), 0) >=
            (int)(n_spe * topN))
            break;
        // iterate over species
        for (std::size_t i = 0; i < n_spe; ++i) {
            if (v_counter[i] >= topN) continue;
            auto spe_name =
                std::string("S") + boost::lexical_cast<std::string>(i);
            if (boost::algorithm::ends_with(x.second, spe_name)) {
                v.push_back(x.second);
                v_counter[i] += 1;
            }
        }
    }

    return v;
}

std::vector<std::string> ends_with(
    const std::multimap<double, std::string, std::less<double>>& p_map,
    std::size_t n_spe, std::size_t topN) {
    std::vector<std::string> v;
    std::vector<std::size_t> v_counter(n_spe, 0);
    for (auto x : p_map) {
        // if all species has topN path endwith itself, break
        if (std::accumulate(v_counter.begin(), v_counter.end(), 0) >=
            (int)(n_spe * topN))
            break;
        // iterate over species
        for (std::size_t i = 0; i < n_spe; ++i) {
            if (v_counter[i] >= topN) continue;
            auto spe_name =
                std::string("S") + boost::lexical_cast<std::string>(i);
            if (boost::algorithm::ends_with(x.second, spe_name)) {
                v.push_back(x.second);
                v_counter[i] += 1;
            }
        }
    }

    return v;
}

void merge(std::vector<std::string>& v1, std::vector<std::string> v2) {
    std::set<std::string> us;
    for (auto s1 : v1) us.insert(s1);
    for (auto s2 : v2) us.insert(s2);

    v1.clear();
    for (auto e : us) v1.push_back(e);
}

};  // namespace pathway
#endif
