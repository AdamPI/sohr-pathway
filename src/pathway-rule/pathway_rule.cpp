#ifndef __PATHWAY_CONSTRAINT_CPP_
#define __PATHWAY_CONSTRAINT_CPP_
#include "pathway_rule.h"
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>  //for json_reader
#include <boost/property_tree/ptree.hpp>        //for property_tree
#include <map>
#include <tuple>
#include <unordered_set>
#include <vector>
#include "cstdio"

namespace pathway {

void Rule::init_from_json(const std::string& fn) {
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(fn, pt, std::locale());
    this->init_from_pt(pt);
}

void Rule::init_from_pt(const boost::property_tree::ptree& pt) {
    // check if "rules.must_react_species" exists
    if (!pt.get_child_optional("rules.must_react_species")) {
        printf("\"rules.must_react_species\" doesn't exist\n");
    } else {
        for (auto k : pt.get_child("rules.must_react_species")) {
            auto v = k.second.get_value<index_int_t>();
            printf("\"rules.must_react_species\" value: %d\n", v);
            _add_must_react_species(v);
        }
    }
    // check if "rules.not_allowed_species" exists
    if (!pt.get_child_optional("rules.not_allowed_out_species")) {
        printf("\"rules.not_allowed_out_species\" doesn't exist\n");
    } else {
        for (auto k : pt.get_child("rules.not_allowed_out_species")) {
            auto v = k.second.get_value<index_int_t>();
            printf("\"rules.not_allowed_out_species\" value: %d\n", v);
            _add_not_allowed_species(v);
        }
    }
    // check if "rules.species_sink_reaction_map" exists
    if (!pt.get_child_optional("rules.species_sink_reaction_map")) {
        printf("\"rules.species_sink_reaction_map\" doesn't exist\n");
    } else {
        for (auto k : pt.get_child("rules.species_sink_reaction_map")) {
            auto s_idx = boost::lexical_cast<index_int_t>(k.first);
            printf("\"rules.species_sink_reaction_map\" species: %d\n", s_idx);
            for (auto v : k.second) {
                auto r_idx = v.second.get_value<index_int_t>();
                printf("    reaction: %d\n", r_idx);
                _add_species_sink_reaction(r_idx, s_idx);
            }
        }
    }
    // check if "rules.reaction_out_species_map" exists
    if (!pt.get_child_optional("rules.reaction_out_species_map")) {
        printf("\"rules.reaction_out_species_map\" doesn't exist\n");
    } else {
        for (auto k : pt.get_child("rules.reaction_out_species_map")) {
            auto r_idx = boost::lexical_cast<index_int_t>(k.first);
            printf("\"rules.reaction_out_species_map\" reaction: %d\n", r_idx);
            for (auto v : k.second) {
                auto s_idx = v.second.get_value<index_int_t>();
                printf("    species: %d\n", s_idx);
                _add_reaction_out_species(r_idx, s_idx);
            }
        }
    }
    // check if "rules.terminal_species" exists
    if (!pt.get_child_optional("rules.terminal_species")) {
        printf("\"rules.terminal_species\" doesn't exist\n");
    } else {
        for (auto k : pt.get_child("rules.terminal_species")) {
            auto v = k.second.get_value<index_int_t>();
            printf("\"rules.terminal_species\" value: %d\n", v);
            terminal_species_set.insert(v);
        }
    }
}
// add r_idx as sink reaction of species s_idx
void Rule::_add_species_sink_reaction(index_int_t r_idx, index_int_t s_idx) {
    if (this->species_sink_reaction_map.count(s_idx) == 0)
        this->species_sink_reaction_map[s_idx] =
            std::unordered_set<index_int_t>();
    this->species_sink_reaction_map[s_idx].insert(r_idx);
}

// add s_idx as out species of reaction r_idx
void Rule::_add_reaction_out_species(index_int_t r_idx, index_int_t s_idx) {
    if (this->reaction_out_species_map.count(r_idx) == 0)
        this->reaction_out_species_map[r_idx] =
            std::unordered_set<index_int_t>();
    this->reaction_out_species_map[r_idx].insert(s_idx);
}

// add s_idx to not allowed species set
void Rule::_add_not_allowed_species(index_int_t s_idx) {
    this->not_allowed_out_species_set.insert(s_idx);
}

// add s_idx to must reaction species set
void Rule::_add_must_react_species(index_int_t s_idx) {
    this->must_react_species_set.insert(s_idx);
}

// is r_idx a valid sink reaction of species s_idx
bool Rule::is_species_sink_reaction(index_int_t r_idx, index_int_t s_idx) {
    // species is not in the map, default to be true
    if (this->species_sink_reaction_map.count(s_idx) == 0) return true;
    if (this->species_sink_reaction_map.at(s_idx).count(r_idx) > 0) return true;
    return false;
}

// is s_idx a valid out species out reaction r_idx
bool Rule::is_reaction_out_species(index_int_t r_idx, index_int_t s_idx) {
    // default to be true if reaction not on the special list
    if (this->reaction_out_species_map.count(r_idx) == 0) return true;
    if (this->reaction_out_species_map.at(r_idx).count(s_idx) > 0) return true;
    return false;
}

bool Rule::is_species_allowed(index_int_t s_idx) {
    // query to see whether species is on the not allowed list or not
    return this->not_allowed_out_species_set.count(s_idx) == 0;
}

bool Rule::is_species_must_react(index_int_t s_idx) {
    //
    return this->must_react_species_set.count(s_idx) != 0;
}

bool Rule::is_terminal_species(index_int_t s_idx) {
    return this->terminal_species_set.count(s_idx) != 0;
}

}  // namespace pathway
#endif
