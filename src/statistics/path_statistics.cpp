#ifndef __STATISTICS_GET_CONCENTRATION_CPP_
#define __STATISTICS_GET_CONCENTRATION_CPP_

#include "path_statistics.h"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include "file_io.h"

namespace pathway {

statistics::statistics(std::string str_in, std::string str_out, int path_len) {
    read_csv_file(str_in);
    sort_print_to_file(str_out, path_len);
}

// Read in csv file
void statistics::read_csv_file(std::string str_in, int N) {
    auto path_num = fileIO::read_topN_line_csv<std::string>(str_in.c_str(), N);
    for (auto p_n : path_num) {
        int index = p_n[0].find_last_of('S');
        unordered_pathway[p_n[0].substr(index)] +=
            boost::lexical_cast<int>(p_n[1]);
    }
}

// Insert pathway
void statistics::insert_pathway(std::string in_pathway) {
    unordered_pathway[in_pathway] += 1;
}

// Sort by counting number and print to file, print only if pathway count >=
// path_len
void statistics::sort_print_to_file(std::string str_out, int path_len) {
    std::ofstream out_file;
    std::copy(unordered_pathway.begin(), unordered_pathway.end(),
              std::back_inserter(ordered_pathway));
    std::sort(ordered_pathway.begin(), ordered_pathway.end(), Compare_pair());
    try {
        // open to use
        out_file.open(str_out.c_str());
        for (str_int_v::iterator iter = ordered_pathway.begin();
             iter != ordered_pathway.end(); ++iter) {
            if ((*iter).second >= path_len) {
                out_file << (*iter).first << " " << (*iter).second << std::endl;
            }
        }
    }  // try
    catch (std::ofstream::failure e) {
        std::cerr << "Exception opening/reading/closing file\n";
    }  // catch

    out_file.close();
}

const str_int_v& statistics::return_sorted_path_count() {
    ordered_pathway.clear();
    std::copy(unordered_pathway.begin(), unordered_pathway.end(),
              std::back_inserter(ordered_pathway));
    std::sort(ordered_pathway.begin(), ordered_pathway.end(), Compare_pair());
    return ordered_pathway;
}
};  // namespace pathway
#endif
