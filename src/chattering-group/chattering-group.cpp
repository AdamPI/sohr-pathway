#ifndef __SPECIES_GROUP_CPP_
#define __SPECIES_GROUP_CPP_

#include "chattering-group.h"

namespace pathway {
bool chattering::is_in_same_chattering_group(index_int_t s1, index_int_t s2) {
    if (this->unique_chattering_species.find(s1) ==
            this->unique_chattering_species.end() ||
        this->unique_chattering_species.find(s2) ==
            this->unique_chattering_species.end())
        return false;

    return this->spe_idx_2_chattering_group_id_idx.at(s1).first ==
           this->spe_idx_2_chattering_group_id_idx.at(s2).first;
}
index_int_t chattering::get_chattering_group_id(index_int_t s_idx) {
    // not in any chattering group
    if (this->unique_chattering_species.find(s_idx) ==
        this->unique_chattering_species.end())
        return -1;

    return index_int_t(spe_idx_2_chattering_group_id_idx.at(s_idx).first);
}
}  // namespace pathway

#endif
