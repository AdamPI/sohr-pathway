#ifndef __NETWORK_SOHR_CPP_
#define __NETWORK_SOHR_CPP_

#include <boost/lexical_cast.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <cstdio>
#include <iostream>
#include <stdexcept>

#include "types.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network.h"

namespace pathway {

// species composition file <spe_fn>, reaction composition file <rxn_fn>, and
// pathway rules file <pr_fn>
Network::Network(const std::string& spe_fn, const std::string& rxn_fn,
                 const std::string& pr_fn, element_name_t e, int sequence_ID,
                 PropPtrType prop) {
    // random number generator
    this->rand = std::make_shared<random_sohr::random>(sequence_ID);
    // pathway rule
    this->rule = std::make_shared<Rule>();
    this->rule->init_from_json(pr_fn);
    // pathway propagator must be initiated outside
    this->prop = prop;

    // read species and reaction composition
    std::vector<species> _species_v;
    spe_name_index_map_t _snim;
    read_species_composition(spe_fn, _species_v, _snim);
    std::vector<reaction> _reaction_v;
    read_reaction_composition(rxn_fn, _reaction_v);
    update_species_reaction(_species_v, _snim, _reaction_v);
    // extract vertex and edges from elements, species and reactions
    std::vector<VertexProperties_graph> vertexVec;
    std::vector<EdgeProperties_graph> edgePro;
    this->init_network_info(vertexVec, edgePro);
    // build the network
    this->init_graph(vertexVec, edgePro);
    // set spe out reaction
    this->set_spe_out_reaction();
    // set reaction out species for followed element
    this->set_reaction_out_spe(e);
    // set reaction out species Gamme factor
    this->set_reaction_out_spe_Gamma(e);

    // init S matrix
    this->init_S_matrix(e);
    // init R matrix
    this->init_SR_matrix(e);
}

Network::Network() {}
Network::~Network() {}

void Network::set_absolute_end_t(const my_time_t t) {
    this->absolute_end_t = t;
}

// update species and reaction information out of vectors/map which can be
// shared across multiple cores
void Network::update_species_reaction(const std::vector<species>& _species_v,
                                      const spe_name_index_map_t& _snim,
                                      std::vector<reaction> _reaction_v) {
    // deep copy species info and species name index map
    this->species_v.assign(_species_v.begin(), _species_v.end());
    this->spe_name_index_map.clear();
    for (auto p : _snim) {
        this->spe_name_index_map.emplace(p.first, p.second);
    }
    update_element(this->species_v, this->element_v);
    update_reaction(this->spe_name_index_map, _reaction_v);
    // construct forward reaction vector needed for network build
    to_forward_reaction(_reaction_v, this->reaction_v);
}

void Network::init_network_info(
    std::vector<VertexProperties_graph>& vertexVec,
    std::vector<EdgeProperties_graph>& edgePro) const {
    // species info
    for (const auto& s : this->species_v) {
        VertexProperties_graph v_tmp;
        v_tmp.species_index = s.index;
        vertexVec.push_back(v_tmp);
    }
    // reaction info
    for (const auto& r : this->reaction_v) {
        // A+B=>C+D leads to 4 edges in total, i.e. A->C, A->D, B->C and B->D.
        // The edge weights are determined at run-time
        for (const auto& s1 : r.net_reactant) {
            for (const auto& s2 : r.net_product) {
                EdgeProperties_graph e_tmp;
                e_tmp.src = s1.index;
                e_tmp.dest = s2.index;
                e_tmp.reaction_key = r.key;
                e_tmp.src_coef = s1.coef;
                e_tmp.dest_coef = s2.coef;
                edgePro.push_back(e_tmp);
                //
            }
        }
    }
    //
}

void Network::init_graph(const std::vector<VertexProperties_graph>& vertexVec,
                         const std::vector<EdgeProperties_graph>& edgePro) {
    for (std::size_t i = 0; i < vertexVec.size(); ++i) {
        AddVertex(vertexVec[i]);
    }
    for (std::size_t i = 0; i < edgePro.size(); ++i) {
        AddEdge(edgePro[i].src, edgePro[i].dest, edgePro[i]);
    }

    // update vertex info
    index_int_t count = 0;
    for (vertex_range_t vp = vertices(graph); vp.first != vp.second;
         ++vp.first) {
        vertex_index_to_descriptor.push_back(*vp.first);
        properties(*vp.first).vertex_index = count++;
        // std::cout << properties(*vp.first).vertexVec << std::endl;
    }

    // update edge index
    count = 0;
    for (edge_range_t er = getEdges(); er.first != er.second; ++er.first) {
        edge_index_to_descriptor.push_back(*er.first);
        properties(*er.first).edge_index = count++;
        // printf("%d\n", properties(*er.first).edge_index);
    }
}

void Network::set_spe_out_reaction() {
    for (auto& s : this->species_v) {
        for (out_edge_range_t itr = getOutEdges(getVertexByIndex(s.index));
             itr.first != itr.second; ++itr.first) {
            bool exist = false;
            auto candidate = std::make_pair(properties(*itr.first).reaction_key,
                                            properties(*itr.first).dest_coef);
            // same reaction can be in different out edge of same species
            for (const auto& kr : s.reaction_k_index_s_coef_v) {
                if (kr == candidate) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                s.reaction_k_index_s_coef_v.push_back(candidate);
            }
        }
    }
}

// we can certainly construct out spe index info from net_product,  here use
// network properties to rebuild that to verify network working as expected
void Network::set_reaction_out_spe(element_name_t ele) {
    for (edge_range_t itr = getEdges(); itr.first != itr.second; ++itr.first) {
        auto spe_idx = properties(*itr.first).dest;
        // make sure product species has component of followed element
        if (species_v[spe_idx].component.count(ele) == 0) continue;
        auto ele_coef = species_v[spe_idx].component.at(ele);
        assert(ele_coef > 0);

        auto rxn_key = properties(*itr.first).reaction_key;
        auto weight = properties(*itr.first).dest_coef * ele_coef;
        assert(weight > 0);

        // same reaction can be mapped to different edges
        bool exist = false;
        auto candidate =
            spe_index_name_weight_t(spe_idx, species_v[spe_idx].name, weight);
        for (const auto& s :
             reaction_v[rxn_key].out_spe_index_weight_v_map[ele]) {
            if (candidate == s) {
                exist = true;
                break;
            }
        }
        if (!exist) {
            reaction_v[rxn_key].out_spe_index_weight_v_map[ele].push_back(
                candidate);
        }
    }
}

// set Prob_max(tau^{j}|t+tau^{j-1};S^{j-1}), with pathway_end_time fixed
value_t Network::set_spe_prob_max(const my_time_t init_t, const my_time_t end_t,
                                  index_int_t s_idx) {
    // pro_max= 1-prob_min= 1-exp[-integrate_{init_t}^{end_t}{propensity
    // function}];
    if (rule->is_terminal_species(s_idx)) {
        this->species_v[s_idx].prob_max = 0.0;
    } else if (init_t <= end_t) {
        this->species_v[s_idx].prob_max =
            1.0 - exp(-(prop->get_c_pseudo1k(s_idx, end_t) -
                        prop->get_c_pseudo1k(s_idx, init_t)));
    } else {
        this->species_v[s_idx].prob_max = 0.0;
    }

    return this->species_v[s_idx].prob_max;
}

// set the prob_min of a species at a given time
value_t Network::set_spe_prob_min(const my_time_t init_t, const my_time_t end_t,
                                  index_int_t s_idx) {
    // prob_min= exp[-integrate_{init_t}^{end_t}{propensity function}];
    if (rule->is_terminal_species(s_idx)) {
        this->species_v[s_idx].prob_min = 1.0;
    } else if (init_t <= end_t) {
        this->species_v[s_idx].prob_min =
            exp(-(prop->get_c_pseudo1k(s_idx, end_t) -
                  prop->get_c_pseudo1k(s_idx, init_t)));
    } else {
        this->species_v[s_idx].prob_min = 1.0;
    }

    return this->species_v[s_idx].prob_min;
}

my_time_t Network::next_reaction_time_from_importance_sampling(
    const my_time_t curr_t, const index_int_t curr_spe, const value_t Y) {
    // if current species is a terminal species, found
    if (rule->is_terminal_species(curr_spe)) {
        return std::numeric_limits<my_time_t>::max();
    }
    // not found
    // the ln of 1.0/(1.0-Y)
    double ln_Y_reciprocal = log(1.0 / (1.0 - Y));
    // use the initial integral value at this time
    double init_spe_drc_int = prop->get_c_pseudo1k(curr_spe, curr_t);
    // exact integral
    double exact_integral = ln_Y_reciprocal + init_spe_drc_int;
    // solve for the first reaction time
    double reaction_time = prop->time_from_c_pseudo1k(curr_spe, exact_integral);
    if (reaction_time < sys_min_time)
        return sys_min_time;
    else
        return reaction_time;
}

// update out reaction rate of species
void Network::update_out_reaction_rate(index_int_t curr_spe, my_time_t t) {
    assert(this->prop != NULL);
    for (const auto& p : species_v[curr_spe].reaction_k_index_s_coef_v) {
        reaction_v[p.first].rate = prop->get_rr(p.first, t);
    }
}

// set out species branching ratio, namely Gamma factor, for specified
// element/followed atom
void Network::set_reaction_out_spe_Gamma(element_name_t ele) {
    for (auto& r : this->reaction_v) {
        for (auto e_sv : r.out_spe_index_weight_v_map) {
            if (e_sv.first != ele) continue;

            stoichiometric_coef_t prob_total = 0.0;
            // calculate total prob
            for (auto s : e_sv.second) {
                if (rule != NULL) {
                    // not allowed species
                    if (!rule->is_species_allowed(s.index)) continue;
                    // not a valid out species of reaction
                    if (!rule->is_reaction_out_species(r.key, s.index))
                        continue;
                }
                if (s.coef > 0) prob_total += s.coef;
            }
            for (auto s : e_sv.second) {
                if (rule != NULL) {
                    // not allowed species
                    if (!rule->is_species_allowed(s.index)) continue;
                    // not a valid out species of reaction
                    if (!rule->is_reaction_out_species(r.key, s.index))
                        continue;
                }
                if (s.coef > 0) {
                    r.out_spe_Gamma_map[s.index] = (s.coef / prob_total);
                    r.out_spe_Gamma_v.push_back(spe_index_name_weight_t(
                        s.index, s.name, s.coef / prob_total));
                }
            }
            //
        }
    }
}

// given a species index, randomly pick a next reaction
index_int_t Network::spe_next_reaction(const index_int_t curr_spe) {
    // probability vector
    std::vector<value_t> prob(
        species_v[curr_spe].reaction_k_index_s_coef_v.size(), 0.0);
    if (prob.size() == 0) return -1;

    // alternative to "each with index"
    std::size_t i = 0;
    for (const auto& p : species_v[curr_spe].reaction_k_index_s_coef_v) {
        prob[i] = p.second * reaction_v[p.first].rate;
        ++i;
    }
    return species_v[curr_spe]
        .reaction_k_index_s_coef_v
            [rand->return_index_randomly_given_probability_vector(prob)]
        .first;
}

// randomly pick next species based on their weight
index_int_t Network::reaction_next_spe(const index_int_t r_idx) {
    std::vector<value_t> prob(reaction_v[r_idx].out_spe_Gamma_v.size(), 0.0);
    if (prob.size() == 0) return -1;

    // alternative to "each with index"
    std::size_t i = 0;
    for (const auto& sin : reaction_v[r_idx].out_spe_Gamma_v) {
        prob[i] = sin.coef;
        ++i;
    }
    return reaction_v[r_idx]
        .out_spe_Gamma_v[rand->return_index_randomly_given_probability_vector(
            prob)]
        .index;
}

// pathway procceed one step forward
when_R_where_t Network::pathway_forward(const my_time_t t,
                                        const index_int_t curr_spe) {
    // Monte-Carlo simulation
    // generate the random number u_1 between 0 and 1.0
    double u_1 = 0.0;
    do {
        u_1 = rand->random01();
    } while (u_1 == 1.0);
    when_R_where_t when_R_where(-1.0, -1, curr_spe);

    // none chattering case
    my_time_t t_tmp =
        next_reaction_time_from_importance_sampling(t, curr_spe, u_1);
    // printf("next reaction time: %f\n", t_tmp);
    if (t_tmp > this->absolute_end_t) {
        // time out of range, abort and return
        // for example curr_vertex is a terminal species
        when_R_where.t = t_tmp;
        return when_R_where;
    }

    // update rate in the reaction network
    update_out_reaction_rate(curr_spe, t_tmp);
    index_int_t next_reaction_index = spe_next_reaction(curr_spe);
    // printf("next reaction index: %d\n", next_reaction_index);
    assert(next_reaction_index >= 0);
    // random pick next spe
    index_int_t next_vertex = reaction_next_spe(next_reaction_index);
    // printf("next vertex index: %d\n", next_vertex);
    assert(next_vertex >= 0);

    when_R_where.t = t_tmp;
    when_R_where.R = next_reaction_index;
    when_R_where.s = next_vertex;

    return when_R_where;
}

// one-time pathway simulation in time range
std::string Network::pathway_sim(const my_time_t init_t, const my_time_t end_t,
                                 const index_int_t init_s) {
    this->set_absolute_end_t(end_t);
    when_R_where_t when_R_where(init_t, -1, init_s);

    std::stringstream path_s;
    path_s << 'S' << init_s;
    while (when_R_where.t < end_t) {
        // printf("current species: %d\n", when_R_where.s);
        when_R_where = pathway_forward(when_R_where.t, when_R_where.s);
        if (when_R_where.t > end_t) break;
        path_s << 'R' << when_R_where.R << 'S' << when_R_where.s;
    }

    return path_s.str();
}

// generate pathway by running Monte Carlo simulation
void Network::gen_pathway(const my_time_t init_t, const my_time_t end_t,
                          const index_int_t init_s, const int Ntraj,
                          statistics& st) {
    for (int i = 0; i < Ntraj; ++i) {
        st.insert_pathway(pathway_sim(init_t, end_t, init_s));
    }
}

// Gamma - branching_ratio
value_t Network::spe_sink_by_a_reaction_Gamma(index_int_t curr_spe,
                                              index_int_t next_reaction) {
    // if current reaction is not a valid sink reaction current species
    if (!rule->is_species_sink_reaction(next_reaction, curr_spe)) return 0.0;

    // calculate this actually
    value_t prob_total = 0.0, prob_target_reaction = 0.0;
    for (std::size_t i = 0;
         i < this->species_v[curr_spe].reaction_k_index_s_coef_v.size(); ++i) {
        auto r_idx =
            this->species_v[curr_spe].reaction_k_index_s_coef_v[i].first;
        // found next reaction
        if (r_idx == next_reaction) {
            prob_target_reaction =
                this->species_v[curr_spe].reaction_k_index_s_coef_v[i].second *
                this->reaction_v[r_idx].rate;
            prob_total += prob_target_reaction;
        }
        // not found next reaction
        else {
            // not a candidate reaction
            if (!rule->is_species_sink_reaction(r_idx, curr_spe)) continue;
            prob_total +=
                this->species_v[curr_spe].reaction_k_index_s_coef_v[i].second *
                this->reaction_v[r_idx].rate;
        }
    }

    value_t reaction_branching_ratio;
    // it prob_total == 0.0, it must be that I set it to be zero
    // artificially it depends
    if (prob_total == 0.0) {
        reaction_branching_ratio = 1.0;
    } else {
        reaction_branching_ratio = prob_target_reaction / prob_total;
    }

    return reaction_branching_ratio;
}

value_t Network::reaction_out_spe_Gamma(index_int_t curr_r, index_int_t out_s) {
    // not a valid out species of given reaction
    if (!rule->is_reaction_out_species(curr_r, out_s)) return 0.0;
    if (this->reaction_v[curr_r].out_spe_Gamma_map.count(out_s) == 0) {
        return 0.0;
    }
    if (!rule->is_species_allowed(out_s)) return 0.0;

    return this->reaction_v[curr_r].out_spe_Gamma_map.at(out_s);
}

// reaction and species branching ratio
r_s_Gamma_t Network::cal_r_s_Gamma(const my_time_t curr_t,
                                   const index_int_t curr_spe,
                                   const index_int_t next_reaction,
                                   const index_int_t next_spe) {
    // update rate in the reaction network
    if (curr_t > 0) this->update_out_reaction_rate(curr_spe, curr_t);

    return r_s_Gamma_t(spe_sink_by_a_reaction_Gamma(curr_spe, next_reaction),
                       this->reaction_out_spe_Gamma(next_reaction, next_spe));
}

// reaction and species branching ratio by simulating species hopping
// through reaction network forward one step, notice the time variable
// is randomly sampled and dynamically updated
r_s_Gamma_t Network::pathway_prob_forward(const index_int_t curr_spe,
                                          const index_int_t next_reaction,
                                          const index_int_t next_spe,
                                          my_time_t& curr_t) {
    if (curr_t >= (absolute_end_t - INFINITESIMAL_DT)) {
        return r_s_Gamma_t(0.0, 0.0);
    }

    this->set_spe_prob_max(curr_t, absolute_end_t, curr_spe);

    double u_1;
    if (species_v[curr_spe].prob_max > 0.0) {
        u_1 = rand->random_min_max(0, species_v[curr_spe].prob_max);
    } else {
        u_1 = 0.0;
        // u_1 = INFINITESIMAL_DT;
    }

    curr_t = next_reaction_time_from_importance_sampling(curr_t, curr_spe, u_1);

    return cal_r_s_Gamma(curr_t, curr_spe, next_reaction, next_spe);
}

// probability species can react in a time range
value_t Network::prob_spe_react_in_a_time_range(const my_time_t init_t,
                                                const my_time_t end_t,
                                                const index_int_t curr_s) {
    if (rule->is_species_must_react(curr_s)) return 1.0;
    if (rule->is_terminal_species(curr_s)) return 0.0;

    this->set_spe_prob_max(init_t, end_t, curr_s);
    return species_v[curr_s].prob_max;
}

// pathway probability by simulating one chain of reaction events
value_t Network::pathway_prob_sim(const my_time_t init_t, const my_time_t end_t,
                                  const std::vector<index_int_t>& s_vec,
                                  const std::vector<index_int_t> r_vec) {
    // set pathway end time
    set_absolute_end_t(end_t);

    // basically, we assume there must be a reaction at the beginning, so
    // should multiply by the 1-P_min(tau=0|t;S^{0})
    double pathway_prob = 1.0;
    double curr_t = init_t;

    // start from the first reaction
    for (size_t i = 0; i < s_vec.size() - 1;) {
        pathway_prob *= prob_spe_react_in_a_time_range(curr_t, end_t, s_vec[i]);
        // curr_t shall be updated here
        auto rs_gamma =
            pathway_prob_forward(s_vec[i], r_vec[i], s_vec[i + 1], curr_t);

        pathway_prob *= rs_gamma.r;
        pathway_prob *= rs_gamma.s;

        // move one step
        ++i;
    }

    // got to multiply by P_min or says (1-P_max)
    this->set_spe_prob_max(curr_t, end_t, s_vec.back());
    pathway_prob *= (1 - species_v[s_vec.back()].prob_max);

    return pathway_prob;
}

// [approx]imate the pathway probability using importance sampling technique
value_t Network::pathway_prob_approx(const my_time_t init_t,
                                     const my_time_t end_t,
                                     const std::vector<index_int_t>& s_vec,
                                     const std::vector<index_int_t> r_vec,
                                     const int Ntraj) {
    assert(Ntraj >= 1);
    value_t p_total = 0.0;
    for (int i = 0; i < Ntraj; ++i) {
        p_total += pathway_prob_sim(init_t, end_t, s_vec, r_vec);
    }
    return p_total / Ntraj;
}

void Network::pathway_prob_approx_ts(const std::vector<my_time_t>& t_vec,
                                     const std::vector<index_int_t>& s_vec,
                                     const std::vector<index_int_t> r_vec,
                                     const int Ntraj,
                                     std::vector<value_t>& pp_vec) {
    assert(t_vec.size() > 1);
    pp_vec.resize(t_vec.size());
    for (int i = 1; i < pp_vec.size(); ++i) {
        pp_vec[i] =
            this->pathway_prob_approx(t_vec[0], t_vec[i], s_vec, r_vec, Ntraj);
    }
}

void Network::init_S_matrix(element_name_t e) {
    // initilization
    this->S_matrix.resize(this->species_v.size());
    for (auto& re : this->S_matrix) re.resize(this->species_v.size());
    for (std::size_t i = 0; i < this->S_matrix.size(); ++i) {
        for (std::size_t j = 0; j < this->S_matrix[i].size(); ++j) {
            this->S_matrix[i][j] = 0;
        }
    }
    // build the matrix
    for (std::size_t i = 0; i < this->S_matrix.size(); ++i) {
        // src species contains element
        if (this->species_v[i].component.count(e) == 0 ||
            this->species_v[i].component.at(e) <= 0)
            continue;
        const auto out_rv = this->species_v[i].reaction_k_index_s_coef_v;
        for (const auto& r : out_rv) {
            // reaction idx
            const auto r_idx = r.first;
            // reaction coef
            const auto r_coef = r.second;
            if (r_coef <= 0) continue;
            for (const auto out_s :
                 this->reaction_v[r_idx].out_spe_index_weight_v_map[e]) {
                // both contains same element/atom
                const auto out_s_idx = out_s.index;
                if (this->species_v[out_s_idx].component.count(e) == 0 ||
                    this->species_v[out_s_idx].component.at(e) <= 0)
                    continue;
                if (out_s.coef <= 0) continue;
                // +1 per reaction
                this->S_matrix[i][out_s_idx] += 1;
            }
        }
    }
}

void Network::init_SR_matrix(element_name_t e) {
    this->SR_matrix.resize(this->species_v.size());
    for (auto& re : this->SR_matrix) re.resize(this->species_v.size());
    for (std::size_t i = 0; i < this->SR_matrix.size(); ++i) {
        for (size_t j = 0; j < this->SR_matrix[i].size(); ++j) {
            this->SR_matrix[i][j] = {};
        }
    }
    // source and target species index
    std::size_t s_i = 0, t_i = 0;
    for (auto itr = this->getEdges(); itr.first != itr.second; ++itr.first) {
        s_i = boost::source(*itr.first, this->graph);
        t_i = boost::target(*itr.first, this->graph);
        if (this->species_v[s_i].component.count(e) == 0 ||
            this->species_v[t_i].component.count(e) == 0 ||
            this->species_v[s_i].component.at(e) == 0 ||
            this->species_v[t_i].component.at(e) == 0)
            continue;
        //
        this->SR_matrix[s_i][t_i].push_back(
            {properties(*itr.first).reaction_key});
    }
}

// Query writes a json of species info, reaction info to terminal/file
void Network::Query(const std::string& fn) const {
    // element root
    boost::property_tree::ptree e_root;
    for (const auto& e : this->element_v) {
        // element child
        boost::property_tree::ptree e_child;
        e_child.put("index", e.index);
        e_child.put("name", e.name);

        e_root.put_child(e.name, e_child);
    }

    // species root
    boost::property_tree::ptree s_root;
    for (const auto& s : this->species_v) {
        // species child
        boost::property_tree::ptree s_child;
        s_child.put("index", s.index);
        s_child.put("name", s.name);

        // component sub child
        boost::property_tree::ptree c_child;
        for (const auto& c : s.component) {
            c_child.put(c.first, c.second);
        }
        s_child.put_child("component", c_child);

        // species sink reactions sub child
        boost::property_tree::ptree kr_child;
        for (const auto& kr : s.reaction_k_index_s_coef_v) {
            kr_child.put(std::to_string(kr.first),
                         this->reaction_v[kr.first].name);
        }
        s_child.put_child("sink_reaction", kr_child);

        s_root.put_child(std::to_string(s.index), s_child);
    }

    // reaction root
    boost::property_tree::ptree r_root;
    for (const auto& r : this->reaction_v) {
        // reaction child
        boost::property_tree::ptree r_child;
        r_child.put("key", r.key);
        // r_child.put("id", r.id);
        r_child.put("name", r.name);
        // r_child.put("index", r.index);

        // reaction out species sub child
        boost::property_tree::ptree os_child;
        // for (const auto& os : r.out_spe_index_weight_v_map) {
        for (const auto& s : r.net_product) {
            // followed atom
            boost::property_tree::ptree fa_sub_child;
            os_child.put(std::to_string(s.index), s.name);
        }
        r_child.put_child("out_species", os_child);

        r_root.put_child(std::to_string(r.key), r_child);
    }

    boost::property_tree::ptree root;
    root.put_child("element", e_root);
    root.put_child("species", s_root);
    root.put_child("reaction", r_root);

    // write to json file
    std::ofstream os;
    try {
        os.open(fn.c_str());
        boost::property_tree::write_json(os, root);
    } catch (std::ofstream::failure e) {
        std::cerr << "Exception opening file\n";
    }
    os.close();
}

NetworkSummary Network::get_network_summary() const {
    auto ns = NetworkSummary();
    ns.n_species = this->species_v.size();
    ns.n_reaction = this->reaction_v.size();
    ns.n_vertex = this->getVertexCount();
    ns.n_edge = this->getEdgeCount();

    return ns;
}

// get edge descriptor by edge index
Network::Edge Network::getEdgeByIndex(index_int_t idx) const {
    if (idx >= 0 && idx < int(edge_index_to_descriptor.size()))
        return edge_index_to_descriptor[idx];

    auto itr = getEdges();
    for (; itr.first != itr.second; ++itr.first) {
        if (properties(*itr.first).edge_index == idx) {
            return *itr.first;
        }
    }
    throw std::invalid_argument("Invalid edge index");
    return Network::Edge();
}

// get vertex descriptor by vertex index
Network::Vertex Network::getVertexByIndex(index_int_t idx) const {
    if (idx >= 0 && idx < int(vertex_index_to_descriptor.size()))
        return vertex_index_to_descriptor[idx];

    auto itr = getVertices();
    for (; itr.first != itr.second; ++itr.first) {
        if (properties(*itr.first).vertex_index == idx) {
            return *itr.first;
        }
    }
    throw std::invalid_argument("Invalid vertex index");
    return Network::Vertex();
}

// for internal test only
const species& Network::_get_species(index_int_t idx) const {
    assert(idx >= 0 && idx < int(this->species_v.size()));
    return this->species_v[idx];
}

const forward_reaction& Network::_get_reaction(index_int_t idx) const {
    assert(idx >= 0 && idx < int(this->reaction_v.size()));
    return this->reaction_v[idx];
}

};  // namespace pathway

#endif  // __NETWORK_SOHR_CPP_
