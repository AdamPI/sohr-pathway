#ifndef __NETWORK_SOHR_CPP_
#define __NETWORK_SOHR_CPP_

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional/optional.hpp>          //for optional
#include <boost/property_tree/json_parser.hpp>  //for json_reader
#include <boost/property_tree/ptree.hpp>        //for property_tree
#include <cassert>
#include <cstdio>
#include <iostream>
#include <unordered_set>
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network-io.h"

namespace pathway {
// notice we convert species name string and element string to lower case
void read_species_composition(const std::string &filename,
                              std::vector<species> &s_v,
                              spe_name_index_map_t &s_n_i_m) {
    s_v.clear();
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(filename, pt, std::locale());
    for (auto p1 : pt) {
        // per species
        species s_tmp;
        // species name
        std::string spe_n = p1.first;
        boost::to_lower(spe_n);
        s_tmp.name = spe_n;
        // printf("Found species: %s\n", spe_n.c_str());
        for (auto p2 : p1.second) {
            // species component
            std::string ele = p2.first;
            boost::to_lower(ele);
            s_tmp.component[ele] =
                boost::lexical_cast<value_t>(p2.second.data());
            // printf("    Element: %s: %s\n", ele.c_str(),
            // p2.second.data().c_str());
        }
        //
        s_v.push_back(s_tmp);
    }
    // update species index
    for (size_t i = 0; i < s_v.size(); ++i) {
        s_v[i].index = i;
    }
    // update species index name mapping
    s_n_i_m.clear();
    for (size_t i = 0; i < s_v.size(); ++i) {
        s_n_i_m[s_v[i].name] = i;
    }
}

// update element registry based on species composition
void update_element(const std::vector<species> &s_v,
                    std::vector<element> &e_v) {
    std::unordered_set<std::string> e_s;
    for (auto s : s_v) {
        for (auto e : s.component) {
            // found
            if (e_s.find(e.first) != e_s.end()) continue;
            // not found
            e_s.insert(e.first);
        }
    }
    // create a backup
    std::vector<element> e_v_b(e_v.begin(), e_v.end());
    e_v.clear();
    // a lookup table
    std::unordered_set<std::string> lookup;
    // iterate over element info vector
    for (auto e : e_v_b) {
        // not found
        if (e_s.find(e.name) == e_s.end()) continue;
        // found
        e_v.push_back(e);
        lookup.insert(e.name);
    }
    for (auto e : e_s) {
        // found
        if (lookup.find(e) != lookup.end()) continue;
        element e_i_tmp;
        e_i_tmp.name = e;
        e_v.push_back(e_i_tmp);
    }
    // update index
    index_int_t count = 0;
    for (auto &e : e_v) {
        e.index = count;
        ++count;
    }
}

// read reaction composition from json file
// notice we convert species name string and element string to lower case
void read_reaction_composition(const std::string &filename,
                               std::vector<reaction> &reaction_v) {
    reaction_v.clear();
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(filename, pt, std::locale());
    for (auto p1 : pt) {
        // per reaction
        reaction r_tmp;
        // reaction name
        std::string rxn_n = p1.first;
        boost::to_lower(rxn_n);
        r_tmp.name = rxn_n;
        // printf("Found reaction: %s\n", rxn_n.c_str());
        // id
        index_int_t id =
            boost::lexical_cast<index_int_t>(p1.second.get<std::string>("id"));
        // printf("    id: %d\n", id);
        r_tmp.id = id;
        // reactant_string
        std::string reactant_string =
            p1.second.get<std::string>("reactant_string");
        boost::to_lower(reactant_string);
        // printf("    reactant_string: %s\n", reactant_string.c_str());
        r_tmp.reactant_string = reactant_string;
        // product_string
        std::string product_string =
            p1.second.get<std::string>("product_string");
        boost::to_lower(product_string);
        // printf("    product_string: %s\n", product_string.c_str());
        r_tmp.product_string = product_string;
        // reversible
        bool reversible =
            boost::lexical_cast<bool>(p1.second.get<bool>("reversible"));
        // printf("    reversible: %s\n", reversible ? "true" : "false");
        r_tmp.reversible = reversible;
        // reactants sub-tree
        auto reactants_tree = p1.second.get_child("reactants");
        for (auto r : reactants_tree) {
            spe_name_t spe_n_tmp = boost::lexical_cast<spe_name_t>(r.first);
            boost::to_lower(spe_n_tmp);
            stoichiometric_coef_t coef_tmp =
                boost::lexical_cast<stoichiometric_coef_t>(r.second.data());
            // printf("    Reactant: %s, coef: %f\n", spe_n_tmp.c_str(),
            // coef_tmp);
            r_tmp.reactant.push_back(
                spe_index_name_weight_t(0, spe_n_tmp, coef_tmp));
        }

        // products sub-tree
        auto products_tree = p1.second.get_child("products");
        for (auto r : products_tree) {
            spe_name_t spe_n_tmp = boost::lexical_cast<spe_name_t>(r.first);
            boost::to_lower(spe_n_tmp);
            stoichiometric_coef_t coef_tmp =
                boost::lexical_cast<stoichiometric_coef_t>(r.second.data());
            // printf("    Product: %s, coef: %f\n", spe_n_tmp.c_str(),
            // coef_tmp);
            r_tmp.product.push_back(
                spe_index_name_weight_t(0, spe_n_tmp, coef_tmp));
        }

        reaction_v.push_back(r_tmp);
    }

    // update reaction index
    for (size_t i = 0; i < reaction_v.size(); ++i) {
        reaction_v[i].index = i;
    }
}

// update reaction -- updates species index in reactions
void update_reaction(const spe_name_index_map_t &s_n_i_m,
                     std::vector<reaction> &r_v) {
    for (auto &r : r_v) {
        for (auto &s : r.reactant) {
            assert(s_n_i_m.count(s.name) == 1);
            s.index = s_n_i_m.at(s.name);
        }
    }
    // update reaction index
    for (auto &r : r_v) {
        for (auto &s : r.product) {
            assert(s_n_i_m.count(s.name) == 1);
            s.index = s_n_i_m.at(s.name);
        }
    }

    // update net reactants
    for (auto &r : r_v) {
        // no need to worry about performance
        std::map<std::string, stoichiometric_coef_t> reactant_m;
        std::map<std::string, stoichiometric_coef_t> product_m;
        for (const auto &s : r.reactant) {
            reactant_m[s.name] = s.coef;
        }
        for (const auto &s : r.product) {
            product_m[s.name] = s.coef;
        }
        // iterate over reactants first
        for (const auto &s : r.reactant) {
            spe_index_name_weight_t s_tmp(s.index, s.name, s.coef);
            // only reactant
            if (product_m.find(s.name) == product_m.end()) {
                r.net_reactant.push_back(s_tmp);
                continue;
            }
            // net reactant
            if (s.coef > product_m[s.name]) {
                s_tmp.coef -= product_m[s.name];
                r.net_reactant.push_back(s_tmp);
                continue;
            }
            // net product will be handled when iterate over product
            // equal case ignored
        }
        // iterate over product
        for (const auto &s : r.product) {
            spe_index_name_weight_t s_tmp(s.index, s.name, s.coef);
            // only product
            if (reactant_m.find(s.name) == reactant_m.end()) {
                r.net_product.push_back(s_tmp);
                continue;
            }
            // net product
            if (s.coef > reactant_m[s.name]) {
                s_tmp.coef -= reactant_m[s.name];
                r.net_product.push_back(s_tmp);
                continue;
            }
            // net reactant was handled when iterate over reactant
            // equal case ignored
        }
    }
}

// reaction to forward_reaction, E.g A<=>B becomes two reactions,
// A=>B and B=>A
void to_forward_reaction(const std::vector<reaction> &r_v,
                         std::vector<forward_reaction> &f_r_v) {
    f_r_v.clear();
    index_int_t counter = 0;
    for (const auto &r : r_v) {
        forward_reaction r_f_tmp;
        r_f_tmp.id = r.id;
        r_f_tmp.index = r.index;
        r_f_tmp.key = counter++;
        r_f_tmp.name = r.reactant_string + " => " + r.product_string;
        r_f_tmp.net_reactant.assign(r.net_reactant.begin(),
                                    r.net_reactant.end());
        r_f_tmp.net_product.assign(r.net_product.begin(), r.net_product.end());
        f_r_v.push_back(r_f_tmp);
        if (!r.reversible) continue;
        // if it is a reversible reaction
        forward_reaction r_b_tmp;
        r_b_tmp.id = r.id;
        r_b_tmp.index = r.index;
        r_b_tmp.key = counter++;
        r_b_tmp.name = r.product_string + " => " + r.reactant_string;
        r_b_tmp.net_reactant.assign(r.net_product.begin(), r.net_product.end());
        r_b_tmp.net_product.assign(r.net_reactant.begin(),
                                   r.net_reactant.end());
        f_r_v.push_back(r_b_tmp);
    }
}

};  // namespace pathway

#endif  // __NETWORK_SOHR_CPP_
