#ifndef __PROPAGATOR_CPP_
#define __PROPAGATOR_CPP_

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional/optional.hpp>          //for optional
#include <boost/property_tree/json_parser.hpp>  //for json_reader
#include <boost/property_tree/ptree.hpp>        //for property_tree
#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

#include "cstdio"
#include "file_io.h"
// file(s) include cubic spline codes has to be put at the end of "include" list
#include "propagator.h"

namespace pathway {

#define PROPLOG() (std::cout << "[PROPAGATOR] ")

Propagator::Propagator() {}
Propagator::Propagator(const std::string &timeFn, const std::string &kFn,
                       const std::string &rrFn, const int _nSpe,
                       const int _nRxn, const int maxN) {
    this->init_from_file(timeFn, kFn, rrFn, _nSpe, _nRxn, maxN);
}
Propagator::~Propagator() {}

// The reaction rate file should not double count the duplicate reaction(s)
void Propagator::init_from_file(const std::string &timeFn,
                                const std::string &kFn, const std::string &rrFn,
                                const int _nSpe, const int _nRxn,
                                const int maxN) {
    // time matrix
    auto t_mat = fileIO::read_topN_line_csv<value_t>(timeFn.c_str(), maxN);
    // convert time matrix to time vector
    int _nPts = t_mat.size();
    this->time_v.resize(_nPts);
    for (size_t i = 0; i < _nPts; ++i) {
        this->time_v[i] = t_mat[i][0];
    }
    t_mat.clear();
    t_mat.shrink_to_fit();
    this->nPts = _nPts;
    assert(this->time_v[0] == 0);

    // pseudo1k
    auto kv_trans = fileIO::read_topN_line_csv<value_t>(kFn.c_str(), maxN);
    this->nSpe = _nSpe;
    assert(_nSpe == kv_trans[0].size());
    assert(_nPts == kv_trans.size());
    PROPLOG() << boost::format(
                     "number of species: %d, number of points: %d\n") %
                     _nSpe % _nPts;

    this->pseudo1k_v.resize(_nSpe);
    for (size_t i = 0; i < _nSpe; ++i) this->pseudo1k_v[i].resize(_nPts);
    // kv_trans and rrv_trans are in form of kv_trans[time][species],
    // rrv_trans[time][reaction] convert them into kv_trans[species][time] and
    // rrv_trans[reaction][time]
    for (size_t i = 0; i < this->pseudo1k_v.size(); ++i) {
        for (size_t j = 0; j < this->pseudo1k_v[i].size(); ++j) {
            this->pseudo1k_v[i][j] = kv_trans[j][i];
        }
    }

    // free the memory
    kv_trans.clear();
    kv_trans.shrink_to_fit();

    // reaction rate
    auto rrv_trans = fileIO::read_topN_line_csv<value_t>(rrFn.c_str(), maxN);
    this->nRxn = _nRxn;
    assert(_nRxn == rrv_trans[0].size());
    assert(_nPts == rrv_trans.size());
    PROPLOG() << boost::format(
                     "number of reactions with forward and backward ones "
                     "distinctly indexed: %d, number of points: %d\n") %
                     _nRxn % _nPts;

    this->rr_v.resize(_nRxn);
    for (size_t i = 0; i < _nRxn; ++i) this->rr_v[i].resize(_nPts);
    for (size_t i = 0; i < this->rr_v.size(); ++i) {
        for (size_t j = 0; j < this->rr_v[i].size(); ++j) {
            this->rr_v[i][j] = rrv_trans[j][i];
        }
    }
    // free the memory
    rrv_trans.clear();
    rrv_trans.shrink_to_fit();

    // pseudo1k and rr pointers
    this->init_pseudo1k_rr_ptr();
    // [c]umulative pseudo1k
    this->calculate_c_pseudo1k();
    this->initiate_c_pseudo1k_ptr();
}

void Propagator::init_time_from_file(const std::string &timeFn, const int _nSpe,
                                     const int _nRxn, const int maxN) {
    if (_nSpe <= 0) {
        throw invalid_argument("#species cannot be less than 0!!!");
    }
    if (_nRxn <= 0) {
        throw invalid_argument("#reaction cannot be less than 0!!!");
    }

    // time matrix
    auto t_mat = fileIO::read_topN_line_csv<value_t>(timeFn.c_str(), maxN);
    // convert time matrix to time vector
    int _nPts = t_mat.size();
    this->time_v.resize(_nPts);
    for (size_t i = 0; i < _nPts; ++i) {
        this->time_v[i] = t_mat[i][0];
    }
    t_mat.clear();
    t_mat.shrink_to_fit();
    this->nPts = _nPts;
    assert(this->time_v[0] == 0);

    // pseudo1k
    this->nSpe = _nSpe;
    PROPLOG() << boost::format(
                     "number of species: %d, number of points: %d\n") %
                     _nSpe % _nPts;
    this->pseudo1k_v.resize(_nSpe);
    for (size_t i = 0; i < _nSpe; ++i) this->pseudo1k_v[i].resize(_nPts);

    // reaction rate
    this->nRxn = _nRxn;
    PROPLOG() << boost::format(
                     "number of reactions with forward and backward ones "
                     "distinctly indexed: %d, number of points: %d\n") %
                     _nRxn % _nPts;
    this->rr_v.resize(_nRxn);
    for (size_t i = 0; i < _nRxn; ++i) this->rr_v[i].resize(_nPts);
}

void Propagator::init_pseudo1k_rr_ptr() {
    // pseudo1k pointers
    this->pseudo1k_ptrs.assign(this->nSpe, NULL);
    for (size_t i = 0; i < this->nSpe; ++i) {
        this->pseudo1k_ptrs[i] =
            std::make_shared<Linear_interp>(this->time_v, this->pseudo1k_v[i]);
    }
    // reaction rate (rr) pointers
    this->rr_ptrs.assign(this->nRxn, NULL);
    for (size_t i = 0; i < this->nRxn; ++i) {
        this->rr_ptrs[i] =
            std::make_shared<Linear_interp>(this->time_v, this->rr_v[i]);
    }
}

void Propagator::calculate_c_pseudo1k() {
    c_pseudo1k_v.resize(pseudo1k_v.size());
    std::copy(pseudo1k_v.begin(), pseudo1k_v.end(), c_pseudo1k_v.begin());

    for (size_t i = 0; i < c_pseudo1k_v.size(); ++i) {
        // The first time interval, at time 0, must be zero value
        c_pseudo1k_v[i][0] = 0.0;
    }
    // The other time interval
    for (size_t i = 0; i < c_pseudo1k_v.size(); ++i) {
        for (size_t j = 1; j < c_pseudo1k_v[0].size(); ++j) {
            ////rectangle rule
            // c_pseudo1k_v[time_j][j] = pseudo1k_v[time_j][j] *
            // (this->time_v[j] - this->time_v[j - 1]) +
            // c_pseudo1k_v[time_j][j - 1];

            // trapezoidal rule
            c_pseudo1k_v[i][j] = 0.5 *
                                     (pseudo1k_v[i][j] + pseudo1k_v[i][j - 1]) *
                                     (this->time_v[j] - this->time_v[j - 1]) +
                                 c_pseudo1k_v[i][j - 1];
        }
    }
}

void Propagator::initiate_c_pseudo1k_ptr() {
    this->c_pseudo1k_ptrs.assign(this->nSpe, NULL);
    for (size_t i = 0; i < this->nSpe; ++i) {
        this->c_pseudo1k_ptrs[i] = std::make_shared<Linear_interp>(
            this->time_v, this->c_pseudo1k_v[i]);
    }
    // reverse pointer, ([c]umulative pseudo1k, time)
    this->reverse_c_pseudo1k_ptrs.assign(this->nSpe, NULL);
    for (size_t i = 0; i < this->nSpe; ++i) {
        this->reverse_c_pseudo1k_ptrs[i] = std::make_shared<Linear_interp>(
            this->c_pseudo1k_v[i], this->time_v);
    }
}

value_t Propagator::get_pseudo1k(const size_t speIdx, const my_time_t t) const {
    assert((speIdx >= 0) && (speIdx < nSpe));
    assert(pseudo1k_ptrs.size() == nSpe);
    assert(pseudo1k_ptrs[speIdx] != NULL);
    if (t > this->time_v.back())
        return pseudo1k_ptrs[speIdx]->interp(this->time_v.back());
    return pseudo1k_ptrs[speIdx]->interp(t);
}

value_t Propagator::get_c_pseudo1k(const size_t speIdx,
                                   const my_time_t t) const {
    assert((speIdx >= 0) && (speIdx < nSpe));
    assert(c_pseudo1k_ptrs.size() == nSpe);
    assert(c_pseudo1k_ptrs[speIdx] != NULL);

    my_time_t diff = t - this->time_v.back();
    if (diff > 0) {
        return c_pseudo1k_v[speIdx].back() + diff * pseudo1k_v[speIdx].back();
    }
    return c_pseudo1k_ptrs[speIdx]->interp(t);
}

my_time_t Propagator::time_from_c_pseudo1k(const size_t speIdx,
                                           const value_t ck) const {
    assert((speIdx >= 0) && (speIdx < nSpe));
    assert(reverse_c_pseudo1k_ptrs.size() == nSpe);
    assert(reverse_c_pseudo1k_ptrs[speIdx] != NULL);

    value_t diff = ck - c_pseudo1k_v[speIdx].back();
    if (diff > 0) {
        return this->time_v.back() + diff / pseudo1k_v[speIdx].back();
    }
    return reverse_c_pseudo1k_ptrs[speIdx]->interp(ck);
}

value_t Propagator::get_rr(const size_t rxnIdx, const my_time_t t) const {
    assert((rxnIdx >= 0) && (rxnIdx < nRxn));
    assert(rr_ptrs.size() == nRxn);
    assert(rr_ptrs[rxnIdx] != NULL);
    if (t > this->time_v.back())
        return rr_ptrs[rxnIdx]->interp(this->time_v.back());
    return rr_ptrs[rxnIdx]->interp(t);
}

// get number of points
int Propagator::get_n_pts() const { return this->nPts; }

// get time
value_t Propagator::get_time(const int i) const { return this->time_v[i]; }

void Propagator::update_pseudo1k_per_t(const int t,
                                       const std::vector<value_t> &k) {
    for (int i = 0; i < this->nSpe; ++i) {
        this->pseudo1k_v[i][t] = k[i];
    }
    return;
}

void Propagator::update_rr_per_t(const int t,
                                 const std::vector<value_t> &rr_fwd,
                                 const std::vector<value_t> &rr_bwd,
                                 const std::map<int, int> &dup_2_non,
                                 const std::map<int, bool> &dup_is_reversible) {
    // no mapping needed case
    if (dup_2_non.size() <= 0) {
        for (int i = 0; i < rr_fwd.size(); ++i) {
            this->rr_v[2 * i][t] = rr_fwd[i];
            this->rr_v[2 * i + 1][t] = rr_bwd[i];
        }
        return;
    }

    // reset reaction rate at this time point
    // presumably nRxn is set correctly which is 2 * non_2_dup.size()
    for (int i = 0; i < this->nRxn; ++i) {
        this->rr_v[i][t] = 0.0;
    }
    int non_dup_idx = -1;
    for (int i = 0; i < rr_fwd.size(); ++i) {
        non_dup_idx = dup_2_non.at(i);
        this->rr_v[non_dup_idx][t] += rr_fwd[i];
        // only the duplicate-reaction is reversible, then
        // non_dup_idx(backward reaction) = non_dup_idx(backward reaction) + 1
        if (dup_is_reversible.at(i)) {
            this->rr_v[non_dup_idx + 1][t] += rr_bwd[i];
        }
    }
    return;
}

void Propagator::save_pseudo1k(const std::string &kFile) const {
    if (kFile == "") return;

    assert(this->pseudo1k_v.size() > 0);
    assert(this->pseudo1k_v[0].size() > 0);

    std::ofstream fout(kFile.c_str());
    for (size_t i = 0; i < this->pseudo1k_v[0].size(); ++i) {
        for (size_t j = 0; j < this->pseudo1k_v.size(); ++j) {
            fout << std::setprecision(
                        std::numeric_limits<double>::max_digits10 + 1)
                 << this->pseudo1k_v[j][i];
            if (j < (this->pseudo1k_v.size() - 1)) fout << ",";
        }
        fout << endl;
    }
    fout.clear();
    fout.close();
}

void Propagator::save_rr(const std::string &rrFile) const {
    if (rrFile == "") return;

    assert(this->rr_v.size() > 0);
    assert(this->rr_v[0].size() > 0);

    std::ofstream fout(rrFile.c_str());
    for (size_t i = 0; i < this->rr_v[0].size(); ++i) {
        for (size_t j = 0; j < this->rr_v.size(); ++j) {
            fout << std::setprecision(
                        std::numeric_limits<double>::max_digits10 + 1)
                 << this->rr_v[j][i];
            if (j < (this->rr_v.size() - 1)) fout << ",";
        }
        fout << endl;
    }
    fout.clear();
    fout.close();
}

// ---------- Utilities ----------

PropPtrType New_prop_ptr() { return std::make_shared<Propagator>(); }

void read_rxn_mapping(const std::string fn, std::map<int, int> &dup_2_non,
                      std::map<int, std::vector<int>> &non_2_dup,
                      std::map<int, bool> &dup_is_reversible) {
    dup_2_non.clear();
    non_2_dup.clear();

    boost::property_tree::ptree pt;
    boost::property_tree::read_json(fn, pt, std::locale());
    auto pc = pt.get_child("mapping");
    /*
     * [PROPAGATOR] p1.first 1243
     * [PROPAGATOR] p2.first backward
     * [PROPAGATOR] p3.second 637
     */
    for (auto p1 : pc) {
        PROPLOG() << "p1.first " << p1.first << std::endl;
        int nop_dup_i1 = boost::lexical_cast<int>(p1.first);
        std::vector<int> dup_i2_v;
        for (auto p2 : p1.second) {
            PROPLOG() << "p2.first " << p2.first << std::endl;
            for (auto p3 : p2.second) {
                PROPLOG() << "p3.second " << p3.second.data() << ", ";
                auto dup_i2_int = boost::lexical_cast<int>(p3.second.data());
                dup_i2_v.push_back(dup_i2_int);
                if (p2.first == "forward") {
                    dup_2_non[dup_i2_int] = nop_dup_i1;
                }
                if (p2.first == "backward") {
                    dup_is_reversible[dup_i2_int] = true;
                }
            }
        }
        std::cout << std::endl;
        non_2_dup[nop_dup_i1] = dup_i2_v;
    }
    for (auto p : dup_2_non) {
        if (dup_is_reversible.count(p.first) == 0) {
            dup_is_reversible[p.first] = false;
        }
    }
    assert(dup_is_reversible.size() == dup_2_non.size());
    return;
}

}  // namespace pathway
#endif
