#ifndef _MYCLOCK_CPP_
#define _MYCLOCK_CPP_

#include "myclock.h"
#include <time.h> /* time_t, struct tm, difftime, time, mktime */
#include <chrono>
#include <iostream>

MyClock::MyClock() {
    dBegin = dEnd = 0;
    Time = 0.0;
    fBegin = false;
}

void MyClock::begin() {
    if (fBegin == false) {
        dBegin = std::chrono::system_clock::to_time_t(
            std::chrono::system_clock::now());
        fBegin = true;
    }
}

void MyClock::end() {
    if (fBegin == true) {
        dEnd = std::chrono::system_clock::to_time_t(
            std::chrono::system_clock::now());
        fBegin = false;
        Time = difftime(dEnd, dBegin);
    }
}

double MyClock::GetHowLong() {
    if (fBegin == false)
        return Time;
    else
        return -1.0;
}

#endif
