#ifndef __MATRIX_SOHR_CPP_
#define __MATRIX_SOHR_CPP_
#include "matrix-sohr.h"
#include <cassert>
#include <cmath>
#include <complex>  // std::complex, std::imag
#include <iostream>
#include <sstream>  // std::stringstream

#include "eigen_unsym.h"
#include "gaussj.h"

namespace pathway {

std::string toString(const path_t &p) {
    std::stringstream ss;
    for (auto e : p) ss << "R" << e;
    return ss.str();
}

std::string toString(const S_matrix_t &mat) {
    std::stringstream ss;
    for (std::size_t i = 0; i < mat.size(); ++i) {
        // ss << "[";
        if (mat[i].size() == 0) {
            // ss << "]";
            if (i != mat.size() - 1) ss << " ";
            continue;
        }
        for (std::size_t j = 0; j < mat[i].size(); ++j) {
            ss << "[";
            if (mat[i][j] > 0) ss << "S" << i << "#" << mat[i][j] << "S" << j;
            ss << "]";
            if (j != mat[i].size() - 1) ss << " ";
        }
        // ss << "]";
        ss << std::endl;
    }
    return ss.str();
}

S_matrix_t matrix_multiplication(S_matrix_t m_k, S_matrix_t k_n) {
    std::size_t m = m_k.size();
    std::size_t m_k_k = m_k[0].size();
    std::size_t k_n_k = k_n.size();
    std::size_t n = k_n[0].size();
    // #columns of first matrix equals #rows of second matrix
    assert(m_k_k == k_n_k);

    S_matrix_t return_matrix(m, std::vector<index_int_t>(n, 0));

    for (std::size_t i = 0; i < m; ++i) {
        for (std::size_t j = 0; j < n; ++j) {
            return_matrix[i][j] = 0;
            for (std::size_t k = 0; k < m_k_k; ++k) {
                return_matrix[i][j] += m_k[i][k] * k_n[k][j];
            }
        }
    }

    return return_matrix;
}

S_matrix_t matrix_power(S_matrix_t n_n, std::size_t k) {
    if (k == 0) return S_matrix_t();
    assert(k >= 1);
    S_matrix_t A = n_n;
    for (std::size_t i = 0; i < k - 1; ++i) {
        A = matrix_multiplication(A, n_n);
    }

    return A;
}

std::string toString(const SR_matrix_t &pRm) {
    std::stringstream ss;
    for (std::size_t i = 0; i < pRm.size(); ++i) {
        for (std::size_t j = 0; j < pRm[i].size(); ++j) {
            ss << "[";
            if (pRm[i][j].size() == 0) {
                ss << "]";
                if (j != pRm[i].size() - 1) ss << " ";
                continue;
            }
            for (std::size_t k = 0; k < pRm[i][j].size(); ++k) {
                ss << "S" << i;
                ss << toString(pRm[i][j][k]);
                ss << "S" << j;
                if (k != pRm[i][j].size() - 1) ss << ",";
            }
            ss << "]";
            if (j != pRm[i].size() - 1) ss << " ";
        }
        ss << std::endl;
    }
    return ss.str();
}

SR_matrix_t matrix_multiplication(SR_matrix_t m_k, SR_matrix_t k_n) {
    std::size_t m = m_k.size();
    std::size_t m_k_k = m_k[0].size();
    std::size_t k_n_k = k_n.size();
    std::size_t n = k_n[0].size();
    // #columns of first matrix equals #rows of second matrix
    assert(m_k_k == k_n_k);
    // each matrix element is a potential matrix
    SR_matrix_t return_matrix(m, std::vector<SR_matrix_element_t>(n));

    for (std::size_t i = 0; i < m; ++i) {
        for (std::size_t j = 0; j < n; ++j) {
            // define multiplication
            // combine two vectors into a single vector
            SR_matrix_element_t p_new;
            for (std::size_t k = 0; k < m_k_k; ++k) {
                for (std::size_t l1 = 0; l1 < m_k[i][k].size(); ++l1) {
                    // for all none-zero path, zero means no path, just a
                    // empty vector
                    if (m_k[i][k][l1].size() != 0) {
                        for (std::size_t l2 = 0; l2 < k_n[k][j].size(); ++l2) {
                            if (k_n[k][j][l2].size() != 0) {
                                std::vector<index_int_t> v_new;
                                // combine two vectors
                                // preallocate memory
                                v_new.reserve(m_k[i][k][l1].size() +
                                              k_n[k][j][l2].size());
                                v_new.insert(v_new.end(), m_k[i][k][l1].begin(),
                                             m_k[i][k][l1].end());
                                v_new.insert(v_new.end(), k_n[k][j][l2].begin(),
                                             k_n[k][j][l2].end());
                                p_new.push_back(v_new);
                            }  // if
                        }      // l2

                    }  // if

                }  // l1
            }      // k

            if (p_new.size() == 0) p_new = {};
            return_matrix[i][j] = p_new;
        }  // j
    }      // i

    return return_matrix;
}

SR_matrix_t matrix_power(SR_matrix_t n_n, std::size_t k) {
    if (k == 0) return SR_matrix_t();
    assert(k >= 1);
    SR_matrix_t A = n_n;
    for (std::size_t i = 0; i < k - 1; ++i) {
        A = matrix_multiplication(A, n_n);
    }

    return A;
}

bool cal_equilibrium_ratio_from_transition_matrix(
    std::vector<std::vector<double>> &transition_mat,
    double &first_real_positive_eigenvalue, std::vector<double> &equil_ratio) {
    std::size_t m = transition_mat.size();
    equil_ratio.assign(m, 0.0);

    MatDoub mat_tmp(m, m, 0.0);
    for (std::size_t i = 0; i < m; ++i) {
        for (std::size_t j = 0; j < m; ++j) {
            mat_tmp[i][j] = transition_mat[i][j];
        }
    }

    Unsymmeig h(mat_tmp, true, true);
    auto eigen_val = h.wri;
    auto eigen_vec = h.zz;

    // find the first eigen value that's is positive
    // if there is more than one steady states, ignore the rest lol
    std::size_t first_real_idx = 0;
    bool ok = false;

    for (std::size_t i = 0; i < m; ++i) {
        if (eigen_val[i].imag() != 0) continue;
        // imaginary is zero, real number
        if (eigen_val[i].real() > 0) {
            first_real_idx = i;
            ok = true;
            break;
        }
    }

    // if there is no real positive eigen-value, return false
    if (ok == false) return false;

    first_real_positive_eigenvalue = eigen_val[first_real_idx].real();
    for (std::size_t i = 0; i < m; ++i) {
        // equil_ratio[i] = eigen_vec[i][first_real_idx];
        equil_ratio[i] =
            eigen_vec[i][first_real_idx] * eigen_vec[i][first_real_idx];
    }

    // normalize
    double sum_tmp = 0.0;
    for (std::size_t i = 0; i < m; ++i) {
        sum_tmp += equil_ratio[i];
    }
    if (sum_tmp == 0.0) return false;

    for (std::size_t i = 0; i < m; ++i) {
        equil_ratio[i] /= sum_tmp;
    }

    return true;
}

bool gaussian_jordan(std::vector<std::vector<double>> &A,
                     std::vector<double> &b) {
    std::size_t m = A.size();
    // b.assign(m, 0.0);

    MatDoub A_mat(m, m, 0.0);
    for (std::size_t i = 0; i < m; ++i) {
        for (std::size_t j = 0; j < m; ++j) {
            A_mat[i][j] = A[i][j];
        }
    }

    MatDoub b_vec(m, 1, 0.0);
    for (std::size_t i = 0; i < m; ++i) b_vec[i][0] = b[i];

    auto ok = gaussj_return(A_mat, b_vec);

    std::size_t n_mat = A_mat.nrows() - 1;

    double sum_b_tmp = 0.0;
    for (std::size_t i = 0; i <= n_mat; ++i) sum_b_tmp += b_vec[i][0];

    while ((ok == false || sum_b_tmp == 0) && (n_mat >= 1)) {
        // set the last variable to be 1.0, move the contribution from the
        // varible to b vector
        MatDoub A_mat_tmp(n_mat, n_mat, 0.0);
        for (std::size_t i = 0; i < n_mat; ++i) {
            for (std::size_t j = 0; j < n_mat; ++j) {
                A_mat_tmp[i][j] = A[i][j];
            }
        }

        MatDoub b_vec_tmp(n_mat, 1, 0.0);
        for (std::size_t i = 0; i < n_mat; ++i) b_vec_tmp[i][0] = b[i];

        // set current the value of current b_vec to be 1.0, move the
        // contribution of it to the b_vec
        b_vec[n_mat][0] = 1.0;
        // for (std::size_t i = 0; i < n_mat; ++i)
        //	b_vec_tmp[i][0] -= (A[i][n_mat] * 1.0);
        for (std::size_t i = 0; i < n_mat; ++i) {
            for (std::size_t j = n_mat; j < m; ++j) {
                b_vec_tmp[i][0] -= (A[i][j] * 1.0);
            }
        }

        ok = gaussj_return(A_mat_tmp, b_vec_tmp);

        if (ok == true) {
            for (std::size_t i = 0; i < n_mat; ++i)
                b_vec[i][0] = b_vec_tmp[i][0];
            break;
        }
        n_mat -= 1;
        sum_b_tmp = 0.0;
        for (std::size_t i = 0; i <= n_mat; ++i) sum_b_tmp += b_vec[i][0];
    }

    double sum_b = 0.0;
    if (ok == true) {
        for (std::size_t i = 0; i < m; ++i) {
            b[i] = b_vec[i][0];
            sum_b += b[i];
        }
    }

    // normalize
    if (ok == false || sum_b == 0) {
        for (std::size_t i = 0; i < m; ++i) {
            b[i] = 1.0 / m;
        }
    } else {
        for (std::size_t i = 0; i < m; ++i) {
            b[i] /= sum_b;
        }
    }

    return true;
}

}  // namespace pathway

#endif  // !__MATRIX_SOHR_CPP_
