"""
cantera related routines
"""
import os
import sys
import argparse
import time
import json
from collections import defaultdict
import itertools
from subprocess import Popen, PIPE
import numpy as np

try:
    import cantera as ct
except ImportError:
    print("CANTERA NOT AVAILABLE")


def ChemKin2Cti(data_dir, chemInpFn='chem.inp', thermFn='therm.dat',
                chemCtiFn='chem.cti'):
    """
    convert ChemKin files chem.inp and therm.dat to .cti
    """
    cmd = ['python', '-m', 'cantera.ck2cti',
           '--input', os.path.join(data_dir, chemInpFn),
           '--thermo', os.path.join(data_dir, thermFn),
           '--output', os.path.join(data_dir, chemCtiFn)]
    process = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)
    return


def Cti2Xml(data_dir, chemCtiFn='chem.cti', chemXmlFn='chem.xml'):
    """
    convert .cti to .xml file
    """
    try:
        import cantera.ctml_writer as cw
        cw.convert(os.path.join(data_dir, chemCtiFn),
                   os.path.join(data_dir, chemXmlFn))
    except ImportError:
        print("CANTERA NOT AVAILABLE")
    return


def normalize_species(data_dir, fn='chem.xml'):
    """
    parse species composition from mechanism file
    Arguments:
        data_dir (str): Directory where mechanism file is located
        fn (str): Mechanism file name
    Returns:
        composition (map): Species composition map keyed by species name
    """
    all_species = ct.Species.listFromFile(
        os.path.join(data_dir, fn))
    spe_composition = defaultdict(dict)

    for spe in all_species:
        spe_composition[spe.name] = spe.composition
    return spe_composition


def normalize_reaction(data_dir, fn='chem.xml'):
    """
    parse reaction composition from mechanism file
    https://cantera.org/documentation/docs-2.4/sphinx/html/cython/kinetics.html
    Arguments:
        data_dir (str): Directory where mechanism file is located
        fn (str): Mechanism file name
    Returns:
        composition (map): Reaction composition map keyed by reaction equation with duplicated
        reaction(s) removed
        mapping (map): reaction index to reaction index mapping, the LHS is in non-duplication
        reaction space, while the RHS is in duplication-reaction space. On LHS
        forward and backward reactions are distinctly indexed.
    Raises:
    """
    all_reactions = ct.Reaction.listFromFile(
        os.path.join(data_dir, fn))
    rxn_composition = defaultdict(dict)

    # name to original index, the one with duplicated reaction(s)
    name_2_idx_1 = {}
    i1 = 0
    for rxn in all_reactions:
        if rxn.equation not in name_2_idx_1:
            name_2_idx_1[rxn.equation] = []
        name_2_idx_1[rxn.equation].append(i1)
        i1 += 1
        rxn_composition[rxn.equation] = {"id": rxn.ID,
                                         "duplicate": rxn.duplicate,
                                         "equation": rxn.equation,
                                         "orders": rxn.orders,
                                         "reactant_string": rxn.reactant_string,
                                         "reactants": rxn.reactants,
                                         "product_string": rxn.product_string,
                                         "products": rxn.products,
                                         "allow_negative_orders": rxn.allow_negative_orders,
                                         "allow_nonreactant_orders": rxn.allow_nonreactant_orders,
                                         "reversible": rxn.reversible}
    # can't rely on order in dictionary hence sorting
    name_and_idx_tuple = [(name, idx) for name, idx in name_2_idx_1.items()]
    name_and_idx_tuple = sorted(name_and_idx_tuple, key=lambda x: x[1][0])
    # index mapping from non-duplicate space to duplicate reaction space
    # with forward and backward reactions distinctly indexed
    non_dup_2_dup = {}
    idx = 0
    for i, x in enumerate(name_and_idx_tuple):
        non_dup_2_dup[idx] = {"forward": x[1]}
        idx += 1
        if rxn_composition[x[0]]["reversible"] or rxn_composition[x[0]]["reversible"] == "yes":
            non_dup_2_dup[idx] = {"backward": x[1]}
            idx += 1

    # 0 -> [0,1,2]
    mapping = {"_comment": \
'''The primary key represents reaction index in non-duplicate-reaction space where forward and backward reactions are distinctly indexed. The secondary key "forward" or "backward" represents if the reaction is a forward or a backward in the duplicate-reaction space.'''}
    mapping["mapping"] = non_dup_2_dup
    return mapping, rxn_composition


def species_info_2json(data_dir, spe_composition, tag=None):
    """
    save species information to json file
    Arguments:
        data_dir (str): Directory where mechanism file is located
        spe_composition (dict): Species composition information
    """
    if tag is not None and tag != "":
        f_n = "spe_composition" + str(tag) + ".json"
    else:
        f_n = "spe_composition.json"
    with open(os.path.join(data_dir, f_n), 'w') as f_h:
        json.dump(spe_composition, f_h, indent=4, sort_keys=False)


def rxn_info_2json(data_dir, non_dup_2_dup, rxn_composition, tag=None):
    """
    save reaction information to json file
    Arguments:
        data_dir (str): Directory where mechanism file is located
        non_dup_2_dup (dict): Reaction index mapping from non-duplicate-reaction
        space to duplicate-reaction space
        rxn_composition (dict): Reaction composition information
    """
    # reaction index mapping
    if tag is not None and tag != "":
        f_n = "rxn_index_mapping" + str(tag) + ".json"
    else:
        f_n = "rxn_index_mapping.json"
    with open(os.path.join(data_dir, f_n), 'w') as f_h:
        json.dump(non_dup_2_dup, f_h, indent=4, sort_keys=False)
    # reaction composition information
    if tag is not None and tag != "":
        f_n = "rxn_composition" + str(tag) + ".json"
    else:
        f_n = "rxn_composition.json"
    with open(os.path.join(data_dir, f_n), 'w') as f_h:
        json.dump(rxn_composition, f_h, indent=4, sort_keys=False)


if __name__ == '__main__':
    INIT_TIME = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', default="default")
    parser.add_argument('-j', '--jobType',
                        default="ChemKin2Cti",
                        choices=['ChemKin2Cti',
                                 'Cti2Xml',
                                 'species_info_2json',
                                 'rxn_info_2json'])

    args = parser.parse_args()
    if args.directory == "default":
        DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
            sys.argv[0]), os.pardir))
    else:
        DATA_DIR = args.directory
    print("Workspace directory {}".format(os.path.realpath(DATA_DIR)))

    if args.jobType == "ChemKin2Cti":
        ChemKin2Cti(DATA_DIR)
    elif args.jobType == "Cti2Xml":
        Cti2Xml(DATA_DIR)
    elif args.jobType == "species_info_2json":
        S_C = normalize_species(DATA_DIR)
        species_info_2json(DATA_DIR, S_C, tag="")
    elif args.jobType == "rxn_info_2json":
        IDX_M, RXN_C = normalize_reaction(DATA_DIR)
        rxn_info_2json(DATA_DIR, IDX_M, RXN_C, tag="")

    END_TIME = time.time()
    print("Time Elapsed:\t{:.5} seconds".format(END_TIME - INIT_TIME))
