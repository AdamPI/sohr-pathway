#!/usr/bin/env python
import numpy as np
import argparse
import sys


def sim_time(ts=3, fn='time_test1.csv'):
   ''' simulate time and save to csv file
   Arguments:
      ts -- number of timestamps
      fn -- file name
   Returns:
   Raises:
   '''
   mat = np.linspace(0, ts-1, ts)
   fn = 't' + str(ts) + '_' + fn
   np.savetxt(fn, mat, delimiter=",")

def sim_species(n, ts=3, fn='cc_test1.csv'):
   ''' simulate species [c]on[c]entration and save to csv file
   Arguments:
      n -- number of species
      ts -- number of timestamps
      fn -- file name
   Returns:
   Raises:
   '''
   mat = np.ones( (ts, n) )
   fn = 's' + str(n) + '_' + 't' + str(ts) + '_' + fn
   np.savetxt(fn, mat, delimiter=",")

def sim_reaction(n, ts=3, fn='rr_test1.csv'):
   ''' simulate reaction rate and save to csv file
   Arguments:
      n -- number of reaction
      ts -- number of timestamps
      fn -- file name
   Returns:
   Raises:
   '''
   mat = np.ones( (ts, n) )
   fn = 'r' + str(n) + '_' + 't' + str(ts) + '_' + fn
   np.savetxt(fn, mat, delimiter=",")


if __name__ == '__main__':
   parser = argparse.ArgumentParser()
   parser.add_argument('--type', help='species or reaction',
                       choices=['t', 's', 'r'])
   parser.add_argument('--num', help='number of species or reaction',
                       required='s' in sys.argv or 'r' in sys.argv)
   parser.add_argument('--time', help='number of timestamps',
                       required=True)
   parser.add_argument('--name', help='file name',
                       required=True)
   args = parser.parse_args()
   if args.type == 't':
      sim_time(int(args.time), args.name)
   elif args.type == 's':
      sim_species(int(args.num), int(args.time), args.name)
   elif args.type == 'r':
      sim_reaction(int(args.num), int(args.time), args.name)
