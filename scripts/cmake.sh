#!/usr/bin/env bash

green() { echo -e "\033[0;32m${1}\033[0m"; }
red() { echo -e "\033[0;31m${1}\033[0m"; }
yellow() { echo -e "\033[0;33m${1}\033[0m"; }

function banner() {
   green "cmake tooling"
   green " use this script to initialize and build project"
}

function usage() {
   red "USAGE:"
   red "   $(basename $0) [-h] (init|build|clean-build|clean|install)"
}

function checker() {
   green "Current directory: $(pwd)"
   if [ ! -d $(pwd)/scripts ]; then
      green "Run script from directory: '$(
         cd $(dirname $0)/..
         pwd -P
      )'"
      exit 2
   fi
   if [ ! -f $(pwd)/scripts/$(basename $0) ]; then
      green "Run script from directory: '$(
         cd $(dirname $0)/..
         pwd -P
      )'"
      exit 2
   fi
}

function init() {
   if [ ! -d build ]; then
      mkdir build
   fi
   cd build
   cmake .. \
      "-DCMAKE_TOOLCHAIN_FILE=$(eval echo ~$USER)/Git/vcpkg/scripts/buildsystems/vcpkg.cmake" \
      -DCMAKE_INSTALL_PREFIX="" \
      -DUSE_MPI=ON
   # -DUSE_MPI=ON \
   # -DCMAKE_RULE_MESSAGES:BOOL=OFF \
   # -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
   cd ..
   yellow "Run 'bash scripts/$(basename $0) build' to build"
}

function build() {
   if [ ! -d build ]; then
      yellow "build directory doesn't exist"
      red "Run 'bash scripts/$(basename $0) init' to initialize"
      exit 2
   fi
   cmake --build build
}

function clean() {
   if [ ! -d build ]; then
      yellow "build directory doesn't exist"
      red "Run 'bash scripts/$(basename $0) init' to initialize"
   fi
   red "Removing build directory '$PWD/build' ..."
   rm -rf build
}

function cleanBuild() {
   clean
   init
   build
}

function install() {
   green "You may need admin authorization to install the package, \
e.g. 'sudo $(basename $0) install \"/usr/local\"'"
   prefix="/usr/local"
   if [ $# -ge 1 ]; then
      prefix=$1
   fi
   green "Please confirm the installation prefix:"
   green "$prefix"
   read -p "Are you sure you wish to continue? (yes|no):"
   if [ "$REPLY" != "yes" ]; then
      red "Please correct the installation location PREFIX, \
e.g. '$(basename $0) install \"/usr/local\"'"
      exit 2
   fi
   green "installing..."
   cd build
   make install DESTDIR=$prefix
}

checker
case $1 in
init)
   init
   ;;
build)
   build
   ;;
clean-build)
   cleanBuild
   ;;
clean)
   clean
   ;;
install)
   if [ $# -lt 2 ]; then
      red "Please specify an installation location PREFIX, \
e.g. '$(basename $0) install \"/usr/local\"'"
      exit 2
   fi
   install $2
   ;;
-h | --help | "")
   banner
   usage
   ;;
*)
   red "Command not found: $1"
   usage
   exit 2
   ;;
esac
