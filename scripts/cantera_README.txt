Q: Install cantera's python interfaces
    - https://cantera.org/install/conda-install.html
Q: Converting Chemkin-format files
    - https://cantera.org/tutorials/ck2cti-tutorial.html
Q: CTI File Syntax
    - https://cantera.org/tutorials/cti/cti-syntax.html
Q: convert input .cti files to CTML using the ctml_writer module
    - https://cantera.org/tutorials/cti/cti-processing.html
Q: parsing a Chemkin file with user-defined isotopes?
    - https://github.com/Cantera/cantera/issues/344
