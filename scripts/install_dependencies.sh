#!/usr/bin/env bash

$(eval echo ~$USER)/Git/vcpkg/vcpkg install boost-program-options
$(eval echo ~$USER)/Git/vcpkg/vcpkg install boost-filesystem
