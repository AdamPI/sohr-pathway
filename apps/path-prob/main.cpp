#include <stdio.h>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <iostream>

#include "colormod.h"
#include "gflags/gflags.h"
#include "pathway-handler.h"

// file(s) include cubic spline codes has to be put at the end of "include" list
#include "network.h"

DEFINE_bool(verbose, false, "display verbose logging messages");
DEFINE_bool(check, false, "verify configuration files are sane");
DEFINE_string(sComp, "", "species composition file");
DEFINE_string(rComp, "", "reaction composition file");
DEFINE_string(pRule, "", "pathway rule file");
DEFINE_string(labelFile, "",
              "elements, species and reaction labelling scheme file");
DEFINE_int64(nMax, int(1e9),
             "maximum number of points to read per time/k/rr files");
DEFINE_string(tFile, "", "time data file");
DEFINE_string(kFile, "", "pseudo-first-order rate constant data file");
DEFINE_string(rrFile, "", "reaction rate data file");
DEFINE_string(atom, "", "tagged atom in lowercase letters");
DEFINE_string(pathway, "", "pathway in format of \"S0R0S1\"");
DEFINE_bool(sMat, false, "display S-matrix");
DEFINE_bool(srMat, false, "display SR-matrix");
DEFINE_bool(mcPath, false, "enumerate pathways using Monte Carlo simulation");
DEFINE_bool(
    pathProb, false,
    "approximate pathway probability using importance sampling technique");
DEFINE_int64(nTraj, -1, "number of trajectory");
DEFINE_int64(initS, -1, "initial species index");
DEFINE_double(startT, -1, "start time");
DEFINE_double(endT, -1, "end time");

static bool IsNonEmptyMessage(const char* flagname, const std::string& value) {
    return value[0] != '\0';
}
DEFINE_validator(sComp, &IsNonEmptyMessage);
DEFINE_validator(rComp, &IsNonEmptyMessage);
DEFINE_validator(pRule, &IsNonEmptyMessage);
DEFINE_validator(atom, &IsNonEmptyMessage);

int main(int argc, char* argv[]) {
    gflags::SetUsageMessage(
        "An utility to appoximate pathway probability based on SOHR - SUM "
        "OVER HISTORY REPRESENTATION of chemical kinetics");
    gflags::SetVersionString("1.0.0");
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    Color::Modifier red(Color::FG_RED);
    Color::Modifier yellow(Color::FG_YELLOW);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier def(Color::FG_DEFAULT);
    if (FLAGS_verbose) {
        // verbose logging info
        std::cout << "[ info ] "
                  << "species composition file: " << yellow << FLAGS_sComp
                  << def << std::endl;
        std::cout << "[ info ] "
                  << "reaction composition file: " << yellow << FLAGS_rComp
                  << def << std::endl;
        std::cout << "[ info ] "
                  << "pathway rule file: " << yellow << FLAGS_pRule << def
                  << std::endl;
        std::cout << "[ info ] "
                  << "time data file: " << yellow << FLAGS_tFile << def
                  << std::endl;
        std::cout << "[ info ] "
                  << "pseudo-first-order rate constant data file: " << yellow
                  << FLAGS_kFile << def << std::endl;
        std::cout << "[ info ] "
                  << "reaction rate data file: " << yellow << FLAGS_rrFile
                  << def << std::endl;
        std::cout << "[ info ] "
                  << "tagged atom: " << yellow << FLAGS_atom << def
                  << std::endl;
    }
    if (FLAGS_check) {
        // configuration file existence check
        bool failed = false;
        if (!boost::filesystem::exists(FLAGS_sComp)) {
            failed = true;
            std::cerr << "[ check #1 ] " << yellow << FLAGS_sComp << red
                      << " doesn't exist!" << def << std::endl;
        } else {
            std::cout << "[ check #1 ] " << green << "OK" << def << std::endl;
        }
        if (!boost::filesystem::exists(FLAGS_rComp)) {
            failed = true;
            std::cerr << "[ check #2 ] " << yellow << FLAGS_rComp << red
                      << " doesn't exist!" << def << std::endl;
        } else {
            std::cout << "[ check #2 ] " << green << "OK" << def << std::endl;
        }
        if (!boost::filesystem::exists(FLAGS_pRule)) {
            failed = true;
            std::cerr << "[ check #3 ] " << yellow << FLAGS_pRule << red
                      << " doesn't exist!" << def << std::endl;
        } else {
            std::cout << "[ check #3 ] " << green << "OK" << def << std::endl;
        }
        if (failed) return 1;

        // run simple sanity check
        auto nPtr = std::make_shared<pathway::Network>(
            FLAGS_sComp, FLAGS_rComp, FLAGS_pRule, FLAGS_atom, 0,
            pathway::New_prop_ptr());
        if (nPtr->get_num_vertices() > 0) {
            std::cout << "[ check #4 ] " << green << "OK" << def << std::endl;
        } else {
            failed = true;
            std::cerr << "[ check #4 ] " << red << " no species found!" << def
                      << std::endl;
        }
        if (nPtr->get_num_edges() > 0) {
            std::cout << "[ check #5 ] " << green << "OK" << def << std::endl;
        } else {
            failed = true;
            std::cerr << "[ check #5 ] " << red << " no edges found!" << def
                      << std::endl;
        }
        if (failed) return 1;
    }

    // path-prob driver
    auto nPtr = std::make_shared<pathway::Network>(FLAGS_sComp, FLAGS_rComp,
                                                   FLAGS_pRule, FLAGS_atom, 0,
                                                   pathway::New_prop_ptr());
    // write elements, species and reactions labelling scheme to file
    std::string labelFileName = FLAGS_labelFile;
    if (labelFileName == "") {
        boost::filesystem::path temp =
            boost::filesystem::temp_directory_path() /
            boost::filesystem::unique_path();
        labelFileName = temp.native() + ".json";  // optional
    }
    std::cout << yellow
              << "Writing elements, species and reactions labelling scheme "
                 "to file: "
              << labelFileName << def << std::endl;
    nPtr->Query(labelFileName);

    if (FLAGS_sMat) {
        std::cout << yellow << pathway::toString(nPtr->S_matrix) << def
                  << std::endl;
    }
    if (FLAGS_srMat) {
        std::cout << yellow << pathway::toString(nPtr->SR_matrix) << def
                  << std::endl;
    }

    bool flag = false;
    if (FLAGS_mcPath || FLAGS_pathProb) {
        if (FLAGS_tFile == "") {
            flag = true;
            std::cout << red << "please set \"-tFile\"" << def << std::endl;
        }
        if (FLAGS_kFile == "") {
            flag = true;
            std::cout << red << "please set \"-kFile\"" << def << std::endl;
        }
        if (FLAGS_rrFile == "") {
            flag = true;
            std::cout << red << "please set \"-rrFile\"" << def << std::endl;
        }
        if (FLAGS_nTraj <= 0) {
            flag = true;
            std::cout << red << "please set \"-nTraj\"" << def << std::endl;
        }
        if (FLAGS_startT == -1) {
            flag = true;
            std::cout << red << "please set \"-initT\"" << def << std::endl;
        }
        if (FLAGS_endT == -1) {
            flag = true;
            std::cout << red << "please set \"-endT\"" << def << std::endl;
        }
    }

    if (FLAGS_mcPath) {
        if (FLAGS_initS == -1) {
            flag = true;
            std::cout << red << "please set \"-initS\"" << def << std::endl;
        }
        if (flag) {
            std::cout << red << "Run \"" << gflags::ProgramInvocationShortName()
                      << " -help\" for more info" << def << std::endl;
            return 1;
        }
        // initiate propagator
        auto ns = nPtr->get_network_summary();
        nPtr->prop->init_from_file(FLAGS_tFile, FLAGS_kFile, FLAGS_rrFile,
                                   ns.n_species, ns.n_reaction, FLAGS_nMax);
        // generate pathway
        pathway::statistics st;
        nPtr->gen_pathway(FLAGS_startT, FLAGS_endT, FLAGS_initS, FLAGS_nTraj,
                          st);
        auto p_count = st.return_sorted_path_count();
        for (size_t i = 0; i < 10 && i < p_count.size(); ++i) {
            std::cout << yellow << "pathway: " << p_count[i].first
                      << ", count: " << p_count[i].second << def << std::endl;
        }
    }

    if (FLAGS_pathProb) {
        if (FLAGS_pathway == "") {
            flag = true;
            std::cout << red << "please set \"-pathway\"" << def << std::endl;
        }
        if (flag) {
            std::cout << red << "Run \"" << gflags::ProgramInvocationShortName()
                      << " -help\" for more info" << def << std::endl;
            return 1;
        }
        // initiate propagator
        auto ns = nPtr->get_network_summary();
        nPtr->prop->init_from_file(FLAGS_tFile, FLAGS_kFile, FLAGS_rrFile,
                                   ns.n_species, ns.n_reaction, FLAGS_nMax);
        // generate pathway
        std::vector<pathway::index_int_t> s_vec, r_vec;
        pathway::pathway_to_vector(FLAGS_pathway, s_vec, r_vec);
        auto prob_approx = nPtr->pathway_prob_approx(FLAGS_startT, FLAGS_endT,
                                                     s_vec, r_vec, FLAGS_nTraj);
        std::cout << "pathway: " << yellow << FLAGS_pathway << def << std::endl;
        std::cout << "pathway probability: " << yellow
                  << boost::format("%e") % prob_approx << def << std::endl;
    }
    // Shut Down Command Line Flags
    gflags::ShutDownCommandLineFlags();
    return 0;
}
